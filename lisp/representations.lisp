




(defclass literal ()
  ((symbolid :accessor lit-symbol 
             :initarg :symbol
             :allocation :instance) ;use gensym to pass in the initarg
   (sign :accessor lit-sign
         :initarg :sign 
         :allocation :instance)
   (arguments :accessor lit-args
              :initarg :args          ;args should be a list of function-expressions
              :allocation :instance)))

(defclass function-expression ()
  ((symbolid :accessor func-symbol
             :initarg :symbol
             :allocation :instance)
   (arguments :accessor func-args
              :initarg :args
              :allocation :instance)
   (variable? :accessor variable?
              :initarg :variable?
              :allocation :instance)))


;lit-args
(defmethod print-object ((p function-expression) stream)
  (format stream "[Function-expression ")
  (format stream (symbol-name (func-symbol p)) stream)
  (format stream " ")
  (if (not (null (func-args p) )) (mapcar  (lambda (x) (print-object x stream)) (func-args p)))
  (format stream "]"))


(defclass problem ()
  ((signature :accessor prob-sig 
              :initarg :sig
              :allocation :instance)
   (arguments :accessor prob-args
              :initarg :args
              :allocation :instance)))

(defclass formula ()
  ((arguments :accessor form-args              ;arguments are a list of function objects
              :initarg :args
              :allocation :instance)))

(defun var-symbol? (x)
  (and (symbolp x)  (equalp "?" (subseq (symbol-name x) 0 1))))

(var-symbol? 'x)
(var-symbol? '?x)

(defun parse-functor (f) 
  (cond ((var-symbol? f) (make-instance 'function-expression :symbol f :args nil :variable? t))
        ((symbolp f) (make-instance 'function-expression :symbol f :args nil :variable? nil) )
        ((listp f) 
         (make-instance 'function-expression :symbol (first f) :args (mapcar 'parse-functor (rest f)) 
                        :variable? nil))))

(defun is-negated? (x)
  (if (equalp "not"  (symbol-name x))))


(defun parse-literal (l)
 ; (make-instance 'literal :symbol l :falsehood nil :args nil)
  (if (equalp (is-negated? l) nil)
      (make-instance 'literal :symbol (first l) :args (mapcar 'parse-functor (rest l)) :sign nil)
      (make-instance 'literal :symbol (first l) :args (mapcar 'parse-functor (rest l)) :sign t)))



