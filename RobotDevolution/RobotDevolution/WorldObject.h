//
//  WorldObject.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/19/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "CCNode.h"
@class  PartialTerm;
@interface WorldObject : CCNode<NSCoding>
{
}
-(BOOL) touchInside:(CGPoint) location;

- (BOOL) acceptedable:(PartialTerm*) other;

@end
