//
//  Robot.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "Robot.h"
#import "Head.h"
#import "Block.h"
#import "GameConfig.h"
#import "Signature.h"
@implementation Robot
@synthesize blocks;
@synthesize width,height,effectiveheight;

+ (Robot *) robotWithBlocks:(CCArray*)blocks;
{
    Robot* robot = [Robot node];
    [Robot condense:blocks];
   // robot.rotation=90;
    [robot initRobotWithBlocks:blocks];
    
	return robot;
}

+(void) condense:(CCArray*)blocks{
    int removed=0;
    for (int i = 0; i<[blocks count]-removed; i++) {
        for (int j=i+1; j<[blocks count]-removed; j++) {
            Block* b1=(Block*)[blocks objectAtIndex:i];
            Block* b2=(Block*)[blocks objectAtIndex:j];
            if([b1 equals:b2])
            {
                [blocks removeObject:b2];
                removed++;
            }
        }
    }

}
-(CGFloat) boundary_x_withNextPos:(CGPoint)next{
    return next.x-[head.headSprite textureRect].size.width*.5;
}
-(CGFloat) boundary_y_withNextPos:(CGPoint)next{
    return next.x-[head.headSprite textureRect].size.width*.5+self.width;
}

// on "init" you need to initialize your instance
-(void) initRobotWithBlocks:(CCArray*)b;
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
        self.blocks=b;
        
        head=[Head head];
        [self addChild:head];
        self.position=ccp(100,100);
        head.position=ccp(0,0);
        
        for (int i=0; i<[blocks count]; i++) {
            Block* b=[blocks objectAtIndex:i];
            [self addChild:b];

            b.position=ccp((i+0.5)*[b.blockSprite textureRect].size.width+[head.headSprite textureRect].size.width*head.scale*.5,0);
        }
        ;
     //   self.rotation=90;
        CCSprite* handsprite = [CCSprite spriteWithFile:@"hand_neg_1.png"];
        CCSprite* blockSprite =  [CCSprite spriteWithFile:@"blackblock.png"];
        CGFloat bheight =[blockSprite textureRect].size.height;
        CGFloat hand_height =[handsprite textureRect].size.height;
        mating_distance_x=MATING_DISTANCE_X_THRESHOLD;
        mating_distance_y=bheight+2*[handsprite textureRect].size.height;
         lower_y = MATING_DISTANCE_Y-MATING_DISTANCE_Y_THRESHOLD;
         upper_y = MATING_DISTANCE_Y+MATING_DISTANCE_Y_THRESHOLD;
        
        blockWidth=[blockSprite textureRect].size.width;
        width= blockWidth*[blocks count]+[head.headSprite textureRect].size.width;
        height=[blockSprite textureRect].size.width+hand_height;
        effectiveheight=height*.42;
     //  [self schedule:@selector(move:) interval:0.5];

	}
}
/*Each element of specs is a CCArray. The first element of this array
 is a signed integer corresponding to the block's id. The second element is 
 another CCArray with a list of CCArrays corresponding to the block's arguments.
 
*/
+ (Robot *) robotFromRawSpecs:(CCArray*)specs{
    Robot* robot = [Robot node];
    CCArray *blocks=[[[CCArray alloc] init]autorelease] ;
    for (int i=0; i<[specs count]; i++) {
        CCArray* bspec=(CCArray*)[specs objectAtIndex:i];
        Signature *s=(Signature*)[bspec objectAtIndex:0];
        
        [blocks addObject:[Block blockWithSign:(s.id_n>0) andID:ABS(s.id_n) ]];        
    }
    
   // robot.rotation=90;

	[robot initRobotWithBlocks:blocks];
   // [blocks release];
    // robot.scale=.5;
	return robot;

}
- (CCArray*) canCombineWith:(Robot* )other{
    
    //This robot is the robot being moved and
    // other is the stationary robot. 
    BOOL cancelled=false;
    CCArray* children = [[[CCArray alloc] init] autorelease];
    
    
    for (int i = 0; i<[blocks count]; i++) {
        for (int j=0; j<[other.blocks count]; j++) {
            Block *b1=(Block*)[blocks objectAtIndex:i];
            Block *b2=(Block*)[other.blocks objectAtIndex:j];
            if ([b1 cancels:b2] && [self inMatingPosition: other block:i otherBlock:j]) {
                cancelled=true;
                CCArray* new_blocks=[[[CCArray alloc] init] autorelease];

                int a = i;int b=j;
                for (int m=0; m<[blocks count]; m++) {
                    if(m==a) continue;
                    [new_blocks addObject:[Block clone:(Block*)[blocks objectAtIndex:m]]];
                }
                for (int m=0; m<[other.blocks count]; m++) {
                    if(m==b) continue;
                    [new_blocks addObject:[Block clone:(Block*)[other.blocks objectAtIndex:m]]];
                }
                CGFloat perfectmating_y_upper=other.position.y+PERFECT_MATING_DISTANCE_Y;
                CGFloat perfectmating_y_lower=other.position.y-PERFECT_MATING_DISTANCE_Y;
                CGFloat self_new_x,self_new_y;
                if(abs(self.position.y-perfectmating_y_upper)<abs(self.position.y-perfectmating_y_lower))
                {
                    self_new_y= perfectmating_y_upper;
                    bouncePointY=perfectmating_y_upper+BouncePointY_DELTA;
                }
                else
                {
                    self_new_y=perfectmating_y_lower;
                    bouncePointY=perfectmating_y_lower-BouncePointY_DELTA;
                }
                if (self.position.x>other.position.x)
                    self_new_x=other.position.x+abs(a-b)*blockWidth;
                else
                    self_new_x=other.position.x-abs(a-b)*blockWidth;
                [Robot elimCDuplicateBlocks:new_blocks];
                self.position=ccp(self_new_x,self_new_y);
                Robot* child= [Robot robotWithBlocks:new_blocks];
                [children addObject:child];
            }
        }
        
    }
    if (cancelled) {
        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(bounceBack) userInfo:nil repeats:NO];
    }
    
    return children;
    
    
}
+(void) elimCDuplicateBlocks:(CCArray* ) blocks{
    
    if ([blocks count]<=1) {
        return;
    }
    NSMutableArray* dups=[NSMutableArray arrayWithCapacity:[blocks count]];
    for (int i =0; i<[blocks count]-1; i++) {
        
        for (int j=i+1; j<[blocks count]; j++) {
            Block *b1=(Block*)[blocks objectAtIndex:i];
            Block *b2=(Block*)[blocks objectAtIndex:j];
            if ([b1 equals:b2] ) {
                [dups addObject:b1];                
            }
            
        }
    }
    for (int i = 0; i<[dups count]; i++) {
        [blocks removeObject:[dups objectAtIndex:i]];
    }
}
+(void) elimCancellableBlocks:(CCArray* ) blocks{
    
    if ([blocks count]<=1) {
        return;
    }
    NSMutableArray* clashes=[NSMutableArray arrayWithCapacity:[blocks count]];
    for (int i =0; i<[blocks count]-1; i++) {
        
        for (int j=i+1; j<[blocks count]; j++) {
            Block *b1=(Block*)[blocks objectAtIndex:i];
            Block *b2=(Block*)[blocks objectAtIndex:j];
            if ([b1 cancels:b2] ) {
                [clashes addObject:b1];
                [clashes addObject:b2];

            }
   
        }
    }
    for (int i = 0; i<[clashes count]; i++) {
        [blocks removeObject:[clashes objectAtIndex:i]];
    }
}
- (void) move:(ccTime)dt {
    int x_sign=rand () % 2?-1:1;
    int y_sign=rand () % 2?-1:1;
    int dx=(int)x_sign*((float) (arc4random()%3));
    int dy=(int)y_sign*((float) (arc4random()%3));
    [self runAction:[CCMoveBy actionWithDuration:1 position:ccp(dx,dy)]];
   // self.position = ccp( self.position.x  +dx, self.position.y +dy);
    if (self.position.x > 480+32) {
//     /       self.position = ccp( -32, self.position.y );
    }
}


-(void) bounceBack{
    CCJumpTo* jumpTo= [CCJumpTo actionWithDuration:0.1 position:ccp(self.position.x,bouncePointY) height:2 jumps:1];
    CCSequence* sequence = [CCSequence actions:jumpTo, nil];
    [self runAction:sequence];
}

- (CCArray*) getTermSpecs:(ccColor3B) c andArgs:(CCArray*) args andVariable:(BOOL) var{
    
    CCArray* term=[[CCArray alloc] init];
    [term addObject:[Signature withArity:[args count] andColor:c andVariable:var]];
    [term addObjectsFromArray:args];
    return term;
}


-(BOOL) touchInside:(CGPoint)location{
    CGPoint loc=[self convertToNodeSpace:location];

    CGRect box =[head.headSprite boundingBox];

    if(CGRectContainsPoint(box, loc))
       return true;
    for (int i=0; i<[blocks count]; i++) {
        Block* b = [blocks objectAtIndex:i];
        if( [b touchInside:location]) return true;
    }
    return false;
}
-(BOOL) bodyLess{
    return [blocks count]==0;
}

-(BOOL) inMatingPosition:(Robot*) other block:(int)i otherBlock:(int)j{
    //Vertical distance between this block and
    //the other block should be block_height+2*arm_height+
    Block *b1=(Block*)[blocks objectAtIndex:i];
    Block *b2=(Block*)[other.blocks objectAtIndex:j];

    CGPoint p = [self convertToWorldSpace:b1.position];
    CGPoint q = [other convertToWorldSpace:b2.position];
    
    return (abs(p.x-q.x)<mating_distance_x) & (abs(p.y-q.y)<upper_y) & (abs(p.y-q.y)>lower_y);
    
}

+ (Robot*) clone:(Robot*) other{
    
    Robot* robot = [Robot node];
    //CCArray* args = [[CCArray alloc] init];
    CCArray* new_blocks=[[[CCArray alloc] init] autorelease];
    for (int i=0; i<[other.blocks count]; i++) {
        [new_blocks addObject:[Block clone:(Block*)[other.blocks objectAtIndex:i]]];
    }
    [robot initRobotWithBlocks:new_blocks];
    
    return robot;
    
}
- (void) encodeWithCoder:(NSCoder *)encoder {
    NSData * encodedData = [NSKeyedArchiver archivedDataWithRootObject:[blocks getNSArray]];
    
    [encoder encodeDataObject:encodedData];
    [encoder encodeCGPoint:self.position forKey:@"position"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    NSData * encodedData= [decoder decodeDataObject];
    NSArray* _blocks= [NSKeyedUnarchiver unarchiveObjectWithData:encodedData];
    [self initRobotWithBlocks:[[CCArray alloc] initWithNSArray:_blocks]];
    self.position=[decoder decodeCGPointForKey:@"position"];
    return  self;
}

-(NSString*) getSpec{
    NSMutableString *spec =[NSMutableString string];
    for (int i=0; i<[blocks count]; i++) {
        Block* tmp=[blocks objectAtIndex:i];
        int curr= (tmp.b_sign?1:-1)*tmp.id_n;
        [spec appendFormat:@"%d |",curr];
    }
    return  [spec substringToIndex:[spec length]-1];
}
// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
    [blocks release];
}


@end
