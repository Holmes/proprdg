//
//  TestScene.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GameScene : CCScene {
    CCLayer* mainLayer;
}
@property (nonatomic,retain) CCLayer *mainLayer;
+(GameScene *) scene;
@end
