//
//  Head.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "WorldObject.h"
@interface Head : WorldObject{
    CCSprite* headSprite;
    CCRepeatForever *blink;
    
}
@property (nonatomic,retain) CCSprite* headSprite;
+(Head *) head;
-(BOOL) touchInside:(CGPoint)location;

@end

