//
//  MainMenuLayer.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

//  MainMenuLayer.m
//  SpaceViking
//
#import "MainMenuLayer.h"
#import <GameKit/GameKit.h>
@interface MainMenuLayer()
-(void)openMotalenSite;
@end

@implementation MainMenuLayer
-(void)openMotalenSite {
    [[GameManager sharedGameManager]
     openSiteWithLinkType:kLinkTypeMotalen];
}

-(void)showOptions {
    CCLOG(@"Show the Options screen");
    [[GameManager sharedGameManager] runSceneWithID:kOptionsScene];
}

-(void)showLevelScene {
    [[SimpleAudioEngine sharedEngine] playEffect:@"button.mp3"];//play a sound

    [[GameManager sharedGameManager] runSceneWithID:kLevelScene];
}
-(void)showHelpScene {
    [[SimpleAudioEngine sharedEngine] playEffect:@"button.mp3"];//play a sound

    [[GameManager sharedGameManager] runSceneWithID:kHelpScene];
}
-(void)showOptionsScene {
    [[SimpleAudioEngine sharedEngine] playEffect:@"button.mp3"];//play a sound

    [[GameManager sharedGameManager] runSceneWithID:kOptionsScene];
}
-(void)displayMainMenu {
    CGSize screenSize = [CCDirector sharedDirector].winSize;
    if (sceneSelectMenu != nil) {
        [sceneSelectMenu removeFromParentAndCleanup:YES];
    }
    GameManager* gm=[GameManager sharedGameManager];
    CCLabelTTF* total_puzzles_label=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"[Total Prisons: %d]",gm.total_puzzles]  fontName:@"Helvetica" fontSize:20];
    CCLabelTTF* solved_puzzles_label=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"[Prisons Broken Out of: %d]",gm.current_puzzle-1]  fontName:@"Helvetica" fontSize:20];
    NSLog(@"Score %f",gm.global_score);
    CCLabelTTF* total_cost_label=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"[Total Rewards: $ %.2f]",gm.global_score]  fontName:@"Helvetica" fontSize:20];

    [self addChild:solved_puzzles_label];
    [self addChild:total_puzzles_label];
    [self addChild:total_cost_label];
    solved_puzzles_label.position=ccp(screenSize.width*.2,screenSize.height-40);
    total_puzzles_label.position=ccp(screenSize.width*.5,screenSize.height-40);
    total_cost_label.position=ccp(screenSize.width*.8,screenSize.height-40);
    [solved_puzzles_label setColor:ccc3(0, 0, 0)];
    [total_puzzles_label setColor:ccc3(0, 0, 0)];
    [total_cost_label setColor:ccc3(0, 0, 0)];


    // Main Menu
    CCSprite *playGameButtonSprite = [CCSprite spriteWithFile:@"playbuttonnormal.png"];
    CCSprite *playGameButtonSpriteSel = [CCSprite spriteWithFile:@"playbuttonselected.png"];
    [playGameButtonSprite.texture setAntiAliasTexParameters];
    
    [playGameButtonSpriteSel.texture setAntiAliasTexParameters];

    CCMenuItemSprite *playGameButton = [CCMenuItemSprite itemFromNormalSprite:playGameButtonSprite selectedSprite:playGameButtonSpriteSel target:self selector:@selector(showLevelScene)];
    
    CCSprite *helpButtonSprite = [CCSprite spriteWithFile:@"helpbutton.png"];
    CCSprite *helpButtonSpriteSel = [CCSprite spriteWithFile:@"helpbuttonselected.png"];
    [helpButtonSprite.texture setAntiAliasTexParameters];
    
    [helpButtonSprite.texture setAntiAliasTexParameters];
    
    CCMenuItemSprite *helpButton = [CCMenuItemSprite itemFromNormalSprite:helpButtonSprite selectedSprite:helpButtonSpriteSel target:self selector:@selector(showHelpScene)];

    CCSprite *optionsButtonSprite = [CCSprite spriteWithFile:@"optionsnormal.png"];
    CCSprite *optionsButtonSpriteSel = [CCSprite spriteWithFile:@"optionsselected.png"];
    [optionsButtonSprite.texture setAntiAliasTexParameters];
    
    [optionsButtonSpriteSel.texture setAntiAliasTexParameters];
    
    CCMenuItemSprite *optionsButton = [CCMenuItemSprite itemFromNormalSprite:optionsButtonSprite selectedSprite:optionsButtonSpriteSel target:self selector:@selector(showOptionsScene)];
    
    playGameButton.scale=.4;
    helpButton.scale=.4;
    optionsButton.scale=.4;
    mainMenu.scale=0.5;
    mainMenu = [CCMenu
                menuWithItems:playGameButton,helpButton,optionsButton,nil];
    [mainMenu alignItemsHorizontallyWithPadding:
     screenSize.width * 0.059f];
    [mainMenu setPosition:
     ccp(screenSize.width * 2,
         screenSize.height / 2)];
    id moveAction =
    [CCMoveTo actionWithDuration:0.5f
                        position:ccp(screenSize.width * 0.5,
                                     screenSize.height/2)];
    id moveEffect = [CCEaseIn actionWithAction:moveAction rate:1.0f];
    [mainMenu runAction:moveEffect];
    [self addChild:mainMenu z:0 tag:kMainMenuTagValue];
    
    
    
    CCMenuItemImage *leaderboardicon=[CCMenuItemImage itemFromNormalImage:@"leaderboardicon.png" selectedImage:@"leaderboardiconselected.png" target:self selector:@selector(showLeaderBoard)];
    CCMenu *leaderboardiconmenu=[CCMenu menuWithItems:leaderboardicon, nil];
    [leaderboardiconmenu setPosition:
     ccp(-screenSize.width * 2,
         screenSize.height *.30)];
    id moveActionLeaderBoard =
    [CCMoveTo actionWithDuration:0.5f
                        position:ccp(screenSize.width * 0.48,
                                     screenSize.height*0.30)];
    id moveEffectLeaderBoard = [CCEaseIn actionWithAction:moveActionLeaderBoard rate:.5];
    [leaderboardiconmenu runAction:moveEffectLeaderBoard];
    
    [self addChild:leaderboardiconmenu];

    CCMenuItemImage *motalen=[CCMenuItemImage itemFromNormalImage:@"learnmorenormal.png" selectedImage:@"learnmoreselected.png" target:self selector:@selector(openMotalenSite)];
        motalen.scale=0.70;
    CCMenu *bottommenu=[CCMenu menuWithItems:motalen, nil];
    [bottommenu setPosition:
     ccp(screenSize.width * 2,
         screenSize.height *.22)];
    id moveActionBottom =
    [CCMoveTo actionWithDuration:0.5f
                        position:ccp(screenSize.width * 0.9,
                                     screenSize.height*.1)];
    
    
    id moveEffectBottom = [CCEaseIn actionWithAction:moveActionBottom rate:.5];
    [bottommenu runAction:moveEffectBottom];

    [self addChild:bottommenu];
   
    

}
- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)leaderboardController
{
    
	[viewController dismissModalViewControllerAnimated:YES];
}
- (void) showLeaderBoard
{
    GKLeaderboardViewController *leaderboardController =
    [[GKLeaderboardViewController alloc] init];
    
    viewController = [[UIViewController alloc] init];
    [[[CCDirector sharedDirector] openGLView] addSubview:viewController.view];

    if (leaderboardController != nil)
    {
        leaderboardController.leaderboardDelegate = self ;
        leaderboardController.timeScope = GKLeaderboardTimeScopeWeek;
        leaderboardController.category = @"com.motalen.proprdg.leaderboard";
        [viewController presentViewController: leaderboardController animated: YES
                         completion:nil];
    } }

- (void)removeFirstSplash:(CCSprite *)oldSprite {
    CCDirector *director = [CCDirector sharedDirector];
    
    [oldSprite removeFromParentAndCleanup:true];
    CGSize size = [director winSize];
    
	CCSprite *sprite = [CCSprite spriteWithFile:@"catabot-splash.png"];
	sprite.position = ccp(size.width/2, size.height/2);
	//sprite.rotation = -90;
   [self addChild:sprite];
    [self performSelector:@selector(removeSecondSplash:) withObject:sprite afterDelay:2];
    
    
}

- (void) displaySplash{

        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *sprite = [CCSprite spriteWithFile:@"motalen_big.png"];
        sprite.position = ccp(size.width/2, size.height/2);
        //	sprite.rotation = -90;
        [self addChild:sprite];
        [self performSelector:@selector(removeFirstSplash:) withObject:sprite afterDelay:1];

}

- (void)removeSecondSplash:(CCSprite *)oldSprite {
    [oldSprite removeFromParentAndCleanup:true];
}

-(id)init {
    self = [super initWithColor:ccc4(255, 255, 255, 255)] ;
    if (self != nil) {
        if([GameManager sharedGameManager].splash){
        [self displaySplash];
            [self performSelector:@selector(displayMainMenu) withObject:nil  afterDelay:3];

        }
        else
        {[self displayMainMenu];
        }}
    return self;
}
@end





