//
//  HelpScene.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "HelpScene.h"

@implementation HelpScene
-(id)init {
    self = [super init];
    if (self != nil) {
        helpLayer = [HelpLayer node];
        [self addChild:helpLayer];
    }
    return self;
}
@end
