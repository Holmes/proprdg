//
//  Head.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "Head.h"


@implementation Head
@synthesize headSprite;
+(Head *) head
{
    Head* head = [Head node];
	// 'layer' is an autorelease object.
    
	// add layer as a child to scene
	//[head init];
	// return the scene
	return head;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode
                                          batchNodeWithFile:@"rdg_head_anims.png"];
        [self addChild:spriteSheet];
        NSMutableArray *walkAnimFrames = [NSMutableArray array];
        for(int i = 1; i <= 6; ++i) {
            [walkAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"rdg_head_%d.png", i]]];
        }
        CCAnimation *walkAnim = [CCAnimation
                                 animationWithFrames:walkAnimFrames delay:0.5+ 0.1f*(1+(int) arc4random() % 4)];
      
        self.headSprite = [CCSprite spriteWithSpriteFrameName:@"rdg_head_1.png"];
        blink = [CCRepeatForever actionWithAction:
                           [CCAnimate actionWithAnimation:walkAnim restoreOriginalFrame:NO]];
        [headSprite runAction:blink];
        [spriteSheet addChild:headSprite];

  //      headSprite=[CCSprite spriteWithFile:@"rdg_head_1.png"];
     //   headSprite.scaleX=0.5;
      //  headSprite.scaleY=0.5;
        
//        CCRotateBy* rotateBy1 = [CCRotateBy actionWithDuration:1 angle:10];
//        CCRotateBy* rotateBy2 = [CCRotateBy actionWithDuration:1 angle:-10];
//        CCRotateBy* rotateBy3 = [CCRotateBy actionWithDuration:1 angle:-10];
//        CCRotateBy* rotateBy4 = [CCRotateBy actionWithDuration:1 angle:10];
//        CCSequence* sequence = [CCSequence actions:rotateBy1, rotateBy2,rotateBy3,rotateBy4, nil];
//        CCRepeatForever* repeat = [CCRepeatForever actionWithAction:sequence];
//        [self runAction:repeat];
       // self.scale=0.50;
	//	[headSprite.texture setAntiAliasTexParameters];
    //    self.scale=0.25;
  //      [self addChild:headSprite];
        [headSprite.texture setAliasTexParameters];

      //  self.rotation=-90;
	}
	return self;
}
-(BOOL) touchInside:(CGPoint)location{
    CGRect box=[headSprite textureRect];
    
    box.origin.x=self.position.x;
    box.origin.y=self.position.y;
    
    if(CGRectContainsPoint(box, location))
        return true;
    return false;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}


@end
