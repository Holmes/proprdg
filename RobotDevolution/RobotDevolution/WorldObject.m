//
//  WorldObject.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/19/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "WorldObject.h"

@implementation WorldObject
-(BOOL) touchInside:(CGPoint) location;
{
    [self doesNotRecognizeSelector:_cmd];
    return false;
}

-(void) update
{
    
    [super update];
}

@end
