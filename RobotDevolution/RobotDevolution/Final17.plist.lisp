
(((:PUZZLE (OR (NOT P3) P2) (OR (NOT P4) (NOT P2)) (OR (NOT P4) P3)
   (OR P4 (NOT P2)) (OR (NOT P3) (NOT P1) P4 P2) (OR P3 P2 P4)
   (OR (NOT P1) P4 (NOT P3) (NOT P2)) (OR (NOT P1) (NOT P4) (NOT P3)))
  (:MIN-PROOF-LEVEL . 4) (:MIN-PROOF-LENGTH . 5)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 6431 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:12 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 9.
% Level of proof is 4.
% Maximum clause weight is 3.000.
% Given clauses 7.


2 -P3 | P2.  [assumption].
3 -P4 | -P2.  [assumption].
4 -P4 | P3.  [assumption].
5 P4 | -P2.  [assumption].
6 P3 | P2 | P4.  [assumption].
8A P3 | P2 | P3.  [resolve(6,c,4,a)].
8 P3 | P2.  [copy(8A),merge(c)].
9A P2 | P2.  [resolve(8,a,2,a)].
9 P2.  [copy(9A),merge(b)].
10 P4.  [resolve(9,a,5,b)].
11A -P2.  [resolve(10,a,3,a)].
11 $F.  [resolve(9,a,11A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P1) (OR P4 (NOT P1) (NOT P2)) (OR P2 (NOT P1) (NOT P4))
   (OR P2 P3) (OR (NOT P2) (NOT P4) P3 P1) (OR (NOT P2) (NOT P3))
   (OR P2 (NOT P1)) (OR P3 (NOT P2)))
  (:MIN-PROOF-LEVEL . 4) (:MIN-PROOF-LENGTH . 5)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 1143 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:07:42 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 9.
% Level of proof is 4.
% Maximum clause weight is 2.000.
% Given clauses 7.


2 -P3 | P1.  [assumption].
5 P2 | P3.  [assumption].
7 -P2 | -P3.  [assumption].
8 P2 | -P1.  [assumption].
9 P3 | -P2.  [assumption].
10 P2 | P1.  [resolve(5,b,2,a)].
11A P2 | P2.  [resolve(10,b,8,b)].
11 P2.  [copy(11A),merge(b)].
12 P3.  [resolve(11,a,9,b)].
13A -P3.  [resolve(11,a,7,a)].
13 $F.  [resolve(12,a,13A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P4)) (OR P4 P3) (OR P2 P1 P3 P4) (OR (NOT P4) P1)
   (OR P1 P2 (NOT P3)) (OR P3 (NOT P2) P4 (NOT P1)) (OR (NOT P2) P4)
   (OR (NOT P1) P4))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 12583 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:46 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 8.


2 -P1 | -P4.  [assumption].
3 P4 | P3.  [assumption].
4 -P4 | P1.  [assumption].
5 P1 | P2 | -P3.  [assumption].
6 -P2 | P4.  [assumption].
7 -P1 | P4.  [assumption].
8 P1 | P2 | P4.  [resolve(5,c,3,b)].
9A P1 | P4 | P4.  [resolve(8,b,6,a)].
9 P1 | P4.  [copy(9A),merge(c)].
10A P4 | P4.  [resolve(9,a,7,a)].
10 P4.  [copy(10A),merge(b)].
11 P1.  [resolve(10,a,4,a)].
12A -P4.  [resolve(11,a,2,a)].
12 $F.  [resolve(10,a,12A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) (NOT P2)) (OR (NOT P1) P4) (OR (NOT P3) (NOT P1))
   (OR P1 (NOT P3)) (OR P1 P4) (OR P2 (NOT P3) (NOT P1) P4) (OR (NOT P4) P2)
   (OR (NOT P4) (NOT P1)) (OR (NOT P2) (NOT P4)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 30113 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:24 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 5.
% Maximum clause weight is 2.000.
% Given clauses 9.


2 -P4 | -P2.  [assumption].
3 -P1 | P4.  [assumption].
6 P1 | P4.  [assumption].
7 -P4 | P2.  [assumption].
8 -P4 | -P1.  [assumption].
9 P2 | P1.  [resolve(7,a,6,b)].
10 P1 | -P4.  [resolve(9,a,2,b)].
11A P1 | P1.  [resolve(10,b,6,b)].
11 P1.  [copy(11A),merge(b)].
12 -P4.  [resolve(11,a,8,b)].
14A P4.  [resolve(11,a,3,a)].
14 $F.  [resolve(12,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 P1 P2 P4) (OR P2 P3 (NOT P4)) (OR (NOT P4) (NOT P2))
   (OR (NOT P4) P2 P3) (OR (NOT P1) (NOT P3)) (OR P2 P3)
   (OR (NOT P1) (NOT P3) (NOT P2) P4) (OR (NOT P2) P3 P4) (OR (NOT P3) P1))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 28153 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:13 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 7.


4 -P4 | -P2.  [assumption].
5 -P1 | -P3.  [assumption].
6 P2 | P3.  [assumption].
7 -P2 | P3 | P4.  [assumption].
8 -P3 | P1.  [assumption].
9A P3 | P4 | P3.  [resolve(7,a,6,a)].
9 P3 | P4.  [copy(9A),merge(c)].
10 P3 | -P2.  [resolve(9,b,4,a)].
11A P3 | P3.  [resolve(10,b,6,a)].
11 P3.  [copy(11A),merge(b)].
12 P1.  [resolve(11,a,8,a)].
13A -P3.  [resolve(12,a,5,a)].
13 $F.  [resolve(11,a,13A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P4)) (OR P3 (NOT P4) P2 P1) (OR (NOT P4) (NOT P3))
   (OR (NOT P2) (NOT P4)) (OR (NOT P4) (NOT P1) (NOT P2)) (OR (NOT P2) P4)
   (OR P2 P4) (OR (NOT P2) P1) (OR (NOT P3) (NOT P4) (NOT P1)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 21595 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:36 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 5.
% Maximum clause weight is 2.000.
% Given clauses 8.


2 P3 | -P4.  [assumption].
3 -P4 | -P3.  [assumption].
4 -P2 | -P4.  [assumption].
5 -P2 | P4.  [assumption].
6 P2 | P4.  [assumption].
8 P2 | P3.  [resolve(6,b,2,b)].
9 P2 | -P4.  [resolve(8,b,3,b)].
10A P2 | P2.  [resolve(9,b,6,b)].
10 P2.  [copy(10A),merge(b)].
12 P4.  [resolve(10,a,5,a)].
13A -P4.  [resolve(10,a,4,a)].
13 $F.  [resolve(12,a,13A,a)].

============================== end of proof ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 2 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 6.
% Maximum clause weight is 2.000.
% Given clauses 8.


2 P3 | -P4.  [assumption].
3 -P4 | -P3.  [assumption].
5 -P2 | P4.  [assumption].
6 P2 | P4.  [assumption].
8 P2 | P3.  [resolve(6,b,2,b)].
9 P2 | -P4.  [resolve(8,b,3,b)].
10A P2 | P2.  [resolve(9,b,6,b)].
10 P2.  [copy(10A),merge(b)].
12 P4.  [resolve(10,a,5,a)].
14 -P3.  [resolve(12,a,3,a)].
15A -P4.  [resolve(14,a,2,a)].
15 $F.  [resolve(12,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P2 P4 P3) (OR (NOT P1) (NOT P3))
   (OR P1 (NOT P2) (NOT P3)) (OR P3 (NOT P2)) (OR P3 (NOT P1)) (OR P4 P2)
   (OR P1 (NOT P4)) (OR P1 P4 (NOT P3)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 16043 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:05 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 10.


3 -P1 | -P3.  [assumption].
4 P1 | -P2 | -P3.  [assumption].
5 P3 | -P2.  [assumption].
6 P3 | -P1.  [assumption].
7 P4 | P2.  [assumption].
8 P1 | -P4.  [assumption].
10 P1 | P2.  [resolve(8,b,7,a)].
11 P1 | P3.  [resolve(10,b,5,b)].
12A P1 | P1 | -P3.  [resolve(10,b,4,b)].
12 P1 | -P3.  [copy(12A),merge(b)].
13A P1 | P1.  [resolve(12,b,11,b)].
13 P1.  [copy(13A),merge(b)].
14 P3.  [resolve(13,a,6,b)].
15A -P3.  [resolve(13,a,3,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P3 P1) (OR (NOT P3) P1) (OR P2 (NOT P1) P4) (OR P3 P2)
   (OR (NOT P3) P4 P2) (OR (NOT P1) (NOT P4)) (OR (NOT P1) P4)
   (OR P3 (NOT P4)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 15759 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:04 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 -P2 | P3 | P1.  [assumption].
3 -P3 | P1.  [assumption].
5 P3 | P2.  [assumption].
7 -P1 | -P4.  [assumption].
8 -P1 | P4.  [assumption].
9 P3 | -P4.  [assumption].
10A P3 | P3 | P1.  [resolve(5,b,2,a)].
10 P3 | P1.  [copy(10A),merge(b)].
11 P3 | P4.  [resolve(10,b,8,a)].
12A P3 | P3.  [resolve(11,b,9,b)].
12 P3.  [copy(12A),merge(b)].
14 P1.  [resolve(12,a,3,a)].
15 P4.  [resolve(14,a,8,a)].
16A -P4.  [resolve(14,a,7,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P1)) (OR (NOT P3) P4) (OR (NOT P4) (NOT P3)) (OR P2 P3)
   (OR P4 (NOT P3)) (OR P4 P1) (OR (NOT P1) (NOT P3) (NOT P2) P4)
   (OR P1 (NOT P2) (NOT P4) P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 13679 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:52 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 9.


2 P3 | -P1.  [assumption].
3 -P3 | P4.  [assumption].
4 -P4 | -P3.  [assumption].
5 P2 | P3.  [assumption].
6 P4 | P1.  [assumption].
7 P1 | -P2 | -P4 | P3.  [assumption].
8 P4 | P3.  [resolve(6,b,2,b)].
9A P1 | -P4 | P3 | P3.  [resolve(7,b,5,a)].
9 P1 | -P4 | P3.  [copy(9A),merge(d)].
10A P1 | P3 | P3.  [resolve(9,b,8,a)].
10 P1 | P3.  [copy(10A),merge(c)].
11A P3 | P3.  [resolve(10,a,2,b)].
11 P3.  [copy(11A),merge(b)].
12 -P4.  [resolve(11,a,4,b)].
13A P4.  [resolve(11,a,3,a)].
13 $F.  [resolve(12,a,13A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P1)) (OR P2 (NOT P4) (NOT P3)) (OR (NOT P4) P2)
   (OR (NOT P1) (NOT P4)) (OR (NOT P1) P2 P3 (NOT P4)) (OR (NOT P3) P4)
   (OR P1 (NOT P2) (NOT P4)) (OR P3 P4 P1))
  (:MIN-PROOF-LEVEL . 4) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 12755 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:47 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 4.
% Maximum clause weight is 3.000.
% Given clauses 7.


2 P3 | -P1.  [assumption].
4 -P4 | P2.  [assumption].
5 -P1 | -P4.  [assumption].
6 -P3 | P4.  [assumption].
7 P1 | -P2 | -P4.  [assumption].
8 P3 | P4 | P1.  [assumption].
9A P3 | P4 | P3.  [resolve(8,c,2,b)].
9 P3 | P4.  [copy(9A),merge(c)].
10A P4 | P4.  [resolve(9,a,6,a)].
10 P4.  [copy(10A),merge(b)].
11 P1 | -P2.  [resolve(10,a,7,c)].
12 -P1.  [resolve(10,a,5,b)].
13 P2.  [resolve(10,a,4,a)].
14A -P2.  [resolve(12,a,11,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P4 (NOT P1) (NOT P3)) (OR P4 P3 P2 (NOT P1))
   (OR (NOT P1) (NOT P4) P2) (OR P3 P2 P4) (OR P2 P3 P1 (NOT P4))
   (OR (NOT P3) P2) (OR P3 (NOT P2)) (OR (NOT P2) (NOT P3)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 7851 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:20 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 9.


4 -P1 | -P4 | P2.  [assumption].
5 P3 | P2 | P4.  [assumption].
6 P2 | P3 | P1 | -P4.  [assumption].
7 -P3 | P2.  [assumption].
8 P3 | -P2.  [assumption].
9 -P2 | -P3.  [assumption].
10A P2 | P3 | P1 | P3 | P2.  [resolve(6,d,5,c)].
10B P2 | P3 | P1 | P2.  [copy(10A),merge(d)].
10 P2 | P3 | P1.  [copy(10B),merge(d)].
11A P2 | P3 | -P4 | P2.  [resolve(10,c,4,a)].
11 P2 | P3 | -P4.  [copy(11A),merge(d)].
12A P2 | P3 | P3 | P2.  [resolve(11,c,5,c)].
12B P2 | P3 | P2.  [copy(12A),merge(c)].
12 P2 | P3.  [copy(12B),merge(c)].
13A P2 | P2.  [resolve(12,b,7,a)].
13 P2.  [copy(13A),merge(b)].
14 -P3.  [resolve(13,a,9,a)].
15A -P2.  [resolve(14,a,8,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 P3) (OR (NOT P2) P1 P4) (OR (NOT P3) (NOT P1) (NOT P4))
   (OR P2 P1 (NOT P4)) (OR P3 (NOT P1)) (OR P1 (NOT P2)) (OR P4 (NOT P3))
   (OR P4 P1 P2))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 6443 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:12 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 P4 | P3.  [assumption].
4 -P3 | -P1 | -P4.  [assumption].
5 P2 | P1 | -P4.  [assumption].
6 P3 | -P1.  [assumption].
7 P1 | -P2.  [assumption].
8 P4 | -P3.  [assumption].
10A P4 | P4.  [resolve(8,b,2,b)].
10 P4.  [copy(10A),merge(b)].
11 P2 | P1.  [resolve(10,a,5,c)].
12 -P3 | -P1.  [resolve(10,a,4,c)].
13A P1 | P1.  [resolve(11,a,7,b)].
13 P1.  [copy(13A),merge(b)].
14 -P3.  [resolve(13,a,12,b)].
15A -P1.  [resolve(14,a,6,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) (NOT P4)) (OR P3 P4) (OR P4 P1 (NOT P3))
   (OR (NOT P3) (NOT P1) P4) (OR P4 P1 P3 (NOT P2)) (OR P1 P4 P2 P3) (OR P1 P3)
   (OR (NOT P1) P3))
  (:MIN-PROOF-LEVEL . 4) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 787 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:07:40 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 4.
% Maximum clause weight is 3.000.
% Given clauses 6.


2 -P3 | -P4.  [assumption].
4 P4 | P1 | -P3.  [assumption].
5 -P3 | -P1 | P4.  [assumption].
6 P1 | P3.  [assumption].
7 -P1 | P3.  [assumption].
8A P3 | P3.  [resolve(7,a,6,a)].
8 P3.  [copy(8A),merge(b)].
9 -P1 | P4.  [resolve(8,a,5,a)].
10 P4 | P1.  [resolve(8,a,4,c)].
11 -P4.  [resolve(8,a,2,a)].
12 P1.  [resolve(11,a,10,a)].
13A P4.  [resolve(12,a,9,a)].
13 $F.  [resolve(11,a,13A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P1) (OR P1 (NOT P4) (NOT P3)) (OR P4 P1) (OR P3 P1 P4)
   (OR (NOT P2) P1 P4 (NOT P3)) (OR P2 (NOT P4) P3) (OR P2 P4)
   (OR (NOT P1) (NOT P3)) (OR (NOT P1) P3))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 31489 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:32 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 10.


2 -P2 | P1.  [assumption].
3 P1 | -P4 | -P3.  [assumption].
4 P4 | P1.  [assumption].
5 P2 | -P4 | P3.  [assumption].
7 -P1 | -P3.  [assumption].
8 -P1 | P3.  [assumption].
9 P2 | P3 | P1.  [resolve(5,b,4,a)].
10A P3 | P1 | P1.  [resolve(9,a,2,a)].
10 P3 | P1.  [copy(10A),merge(c)].
11A P1 | P1 | -P4.  [resolve(10,a,3,c)].
11 P1 | -P4.  [copy(11A),merge(b)].
12A P1 | P1.  [resolve(11,b,4,a)].
12 P1.  [copy(12A),merge(b)].
13 P3.  [resolve(12,a,8,a)].
14A -P3.  [resolve(12,a,7,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P2) (OR P3 (NOT P1)) (OR P4 P1 (NOT P3))
   (OR (NOT P2) P3 (NOT P1)) (OR P3 P1) (OR (NOT P3) (NOT P2))
   (OR P3 P2 P4 (NOT P1)) (OR P4 P2 (NOT P1) P3) (OR (NOT P1) P2))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 26157 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:02 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 2.000.
% Given clauses 9.


2 -P3 | P2.  [assumption].
3 P3 | -P1.  [assumption].
5 P3 | P1.  [assumption].
6 -P3 | -P2.  [assumption].
7 -P1 | P2.  [assumption].
9 P1 | P2.  [resolve(5,a,2,a)].
10 P1 | -P3.  [resolve(9,b,6,b)].
11A P1 | P1.  [resolve(10,b,5,a)].
11 P1.  [copy(11A),merge(b)].
12 P2.  [resolve(11,a,7,a)].
13 P3.  [resolve(11,a,3,b)].
14A -P2.  [resolve(13,a,6,a)].
14 $F.  [resolve(12,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P2)) (OR P4 P3) (OR (NOT P3) P2) (OR P1 P4)
   (OR P1 P2 (NOT P4)) (OR (NOT P1) P3 (NOT P4)) (OR (NOT P3) (NOT P2))
   (OR (NOT P1) (NOT P3) (NOT P2)) (OR (NOT P3) P2 P1 P4))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 24449 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:53 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 10.


2 P3 | -P2.  [assumption].
3 P4 | P3.  [assumption].
4 -P3 | P2.  [assumption].
6 P1 | P2 | -P4.  [assumption].
7 -P1 | P3 | -P4.  [assumption].
8 -P3 | -P2.  [assumption].
9 P1 | P2 | P3.  [resolve(6,c,3,a)].
10A P1 | P3 | P3.  [resolve(9,b,2,b)].
10 P1 | P3.  [copy(10A),merge(c)].
11A P3 | P3 | -P4.  [resolve(10,a,7,a)].
11 P3 | -P4.  [copy(11A),merge(b)].
12A P3 | P3.  [resolve(11,b,3,a)].
12 P3.  [copy(12A),merge(b)].
13 -P2.  [resolve(12,a,8,a)].
14A P2.  [resolve(12,a,4,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P4) (OR (NOT P4) P2 (NOT P3)) (OR (NOT P1) P2)
   (OR P3 P1 (NOT P4) P2) (OR (NOT P2) (NOT P3)) (OR (NOT P1) (NOT P2) P3)
   (OR P1 P3) (OR P2 P1) (OR P2 P1))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 20679 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:31 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 8.


4 -P1 | P2.  [assumption].
6 -P2 | -P3.  [assumption].
7 -P1 | -P2 | P3.  [assumption].
8 P1 | P3.  [assumption].
9 P2 | P1.  [assumption].
10 P1 | -P2.  [resolve(8,b,6,b)].
11A P1 | P1.  [resolve(10,b,9,a)].
11 P1.  [copy(11A),merge(b)].
12 -P2 | P3.  [resolve(11,a,7,a)].
13 P2.  [resolve(11,a,4,a)].
15 P3.  [resolve(13,a,12,a)].
16A -P3.  [resolve(13,a,6,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 (NOT P1)) (OR (NOT P3) (NOT P4) (NOT P2)) (OR P2 P1 (NOT P4))
   (OR P2 P4) (OR P1 (NOT P3)) (OR P3 P1) (OR (NOT P4) (NOT P3) P2)
   (OR P3 (NOT P4)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 10207 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:33 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 6.


2 P4 | -P1.  [assumption].
3 -P3 | -P4 | -P2.  [assumption].
6 P1 | -P3.  [assumption].
7 P3 | P1.  [assumption].
8 -P4 | -P3 | P2.  [assumption].
9 P3 | -P4.  [assumption].
10A P1 | P1.  [resolve(7,a,6,b)].
10 P1.  [copy(10A),merge(b)].
11 P4.  [resolve(10,a,2,b)].
12 P3.  [resolve(11,a,9,b)].
13A -P3 | P2.  [resolve(11,a,8,a)].
13 P2.  [resolve(12,a,13A,a)].
14A -P4 | -P2.  [resolve(12,a,3,a)].
14B -P2.  [resolve(11,a,14A,a)].
14 $F.  [resolve(13,a,14B,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P4)) (OR (NOT P4) (NOT P3)) (OR (NOT P2) P3)
   (OR P2 P3) (OR (NOT P3) (NOT P2) P1 P4) (OR P4 (NOT P1)) (OR (NOT P3) P2)
   (OR (NOT P4) P2))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5603 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:07 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 7.


3 -P4 | -P3.  [assumption].
4 -P2 | P3.  [assumption].
5 P2 | P3.  [assumption].
6 -P3 | -P2 | P1 | P4.  [assumption].
7 P4 | -P1.  [assumption].
8 -P3 | P2.  [assumption].
10A P2 | P2.  [resolve(8,a,5,b)].
10 P2.  [copy(10A),merge(b)].
11 -P3 | P1 | P4.  [resolve(10,a,6,b)].
12 P3.  [resolve(10,a,4,a)].
13 P1 | P4.  [resolve(12,a,11,a)].
14 -P4.  [resolve(12,a,3,b)].
15 P1.  [resolve(14,a,13,b)].
16A -P1.  [resolve(14,a,7,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P4 (NOT P1)) (OR P2 P3 P4 (NOT P1)) (OR P1 P2 P4)
   (OR P3 (NOT P2) (NOT P1)) (OR (NOT P1) (NOT P4)) (OR (NOT P4) P1)
   (OR (NOT P1) (NOT P4) (NOT P3)) (OR P4 (NOT P2)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5195 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:05 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 8.


2 P2 | P4 | -P1.  [assumption].
3 P1 | P2 | P4.  [assumption].
5 -P1 | -P4.  [assumption].
6 -P4 | P1.  [assumption].
7 P4 | -P2.  [assumption].
8A P1 | P1 | P2.  [resolve(6,a,3,c)].
8 P1 | P2.  [copy(8A),merge(b)].
9 P1 | P4.  [resolve(8,b,7,b)].
10A P1 | P1.  [resolve(9,b,6,a)].
10 P1.  [copy(10A),merge(b)].
11 -P4.  [resolve(10,a,5,a)].
13A P2 | -P1.  [resolve(11,a,2,b)].
13 P2.  [resolve(10,a,13A,b)].
14A -P2.  [resolve(11,a,7,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P3)) (OR (NOT P3) P1 (NOT P2)) (OR (NOT P4) P1)
   (OR P2 P1 (NOT P4)) (OR (NOT P4) (NOT P1) P3) (OR P1 P3 P4 P2) (OR P3 P4)
   (OR (NOT P3) P2) (OR (NOT P4) P3 (NOT P1) P2))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 29901 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:23 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 10.


2 -P1 | -P3.  [assumption].
3 -P3 | P1 | -P2.  [assumption].
4 -P4 | P1.  [assumption].
5 -P4 | -P1 | P3.  [assumption].
7 P3 | P4.  [assumption].
8 -P3 | P2.  [assumption].
9A P3 | -P1 | P3.  [resolve(7,b,5,a)].
9 P3 | -P1.  [copy(9A),merge(c)].
10 P3 | P1.  [resolve(7,b,4,a)].
11 P1 | P2.  [resolve(10,a,8,a)].
12A P1 | -P3 | P1.  [resolve(11,b,3,c)].
12 P1 | -P3.  [copy(12A),merge(c)].
13A P1 | P1.  [resolve(12,b,10,a)].
13 P1.  [copy(13A),merge(b)].
14 P3.  [resolve(13,a,9,b)].
15A -P3.  [resolve(13,a,2,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) (NOT P3) P2 P1) (OR (NOT P2) P4) (OR (NOT P1) P2)
   (OR P1 P2 P3) (OR P3 (NOT P1) (NOT P2) P4) (OR (NOT P3) P1)
   (OR (NOT P4) P1 P3 (NOT P2)) (OR (NOT P4) (NOT P2) (NOT P1) P3)
   (OR (NOT P1) (NOT P2)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 26077 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:02 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 10.


3 -P2 | P4.  [assumption].
4 -P1 | P2.  [assumption].
5 P1 | P2 | P3.  [assumption].
6 -P3 | P1.  [assumption].
7 -P4 | P1 | P3 | -P2.  [assumption].
9 -P1 | -P2.  [assumption].
10A P1 | P1 | P2.  [resolve(6,a,5,c)].
10 P1 | P2.  [copy(10A),merge(b)].
11 P1 | P4.  [resolve(10,b,3,a)].
12A P1 | P1 | P3 | -P2.  [resolve(11,b,7,a)].
12 P1 | P3 | -P2.  [copy(12A),merge(b)].
13A P1 | P3 | P1.  [resolve(12,c,10,b)].
13 P1 | P3.  [copy(13A),merge(c)].
14A P1 | P1.  [resolve(13,b,6,a)].
14 P1.  [copy(14A),merge(b)].
15 -P2.  [resolve(14,a,9,a)].
16A P2.  [resolve(14,a,4,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P3) (NOT P4)) (OR P1 P2 (NOT P3) (NOT P4))
   (OR (NOT P4) P1 P2) (OR (NOT P2) P3) (OR (NOT P3) (NOT P1) (NOT P2))
   (OR (NOT P4) (NOT P1) (NOT P3) P2) (OR (NOT P2) (NOT P3) P1) (OR P1 P2)
   (OR (NOT P1) P2))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 25813 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:00 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 8.


4 -P2 | P3.  [assumption].
5 -P3 | -P1 | -P2.  [assumption].
7 -P2 | -P3 | P1.  [assumption].
8 P1 | P2.  [assumption].
9 -P1 | P2.  [assumption].
10 P1 | P3.  [resolve(8,b,4,a)].
11A P1 | -P2 | P1.  [resolve(10,b,7,b)].
11 P1 | -P2.  [copy(11A),merge(c)].
12A P1 | P1.  [resolve(11,b,8,b)].
12 P1.  [copy(12A),merge(b)].
13 P2.  [resolve(12,a,9,a)].
14A -P3 | -P2.  [resolve(12,a,5,b)].
14 -P3.  [resolve(13,a,14A,b)].
15A P3.  [resolve(13,a,4,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P3) (OR (NOT P3) (NOT P4)) (OR P3 (NOT P4) (NOT P1))
   (OR P3 (NOT P4) P1 (NOT P2)) (OR P1 P4 P3) (OR (NOT P1) P2 P3 P4)
   (OR (NOT P4) P1) (OR (NOT P2) P3) (OR (NOT P3) P4))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 22885 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:44 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 -P2 | P3.  [assumption].
3 -P3 | -P4.  [assumption].
4 P3 | -P4 | -P1.  [assumption].
5 P1 | P4 | P3.  [assumption].
6 -P1 | P2 | P3 | P4.  [assumption].
7 -P4 | P1.  [assumption].
8 -P3 | P4.  [assumption].
9A P2 | P3 | P4 | P4 | P3.  [resolve(6,a,5,a)].
9B P2 | P3 | P4 | P3.  [copy(9A),merge(d)].
9 P2 | P3 | P4.  [copy(9B),merge(d)].
10A P3 | P4 | P3.  [resolve(9,a,2,a)].
10 P3 | P4.  [copy(10A),merge(c)].
11 P3 | P1.  [resolve(10,b,7,a)].
12A P3 | P3 | -P4.  [resolve(11,b,4,c)].
12 P3 | -P4.  [copy(12A),merge(b)].
13A P3 | P3.  [resolve(12,b,10,b)].
13 P3.  [copy(13A),merge(b)].
14 P4.  [resolve(13,a,8,a)].
15A -P4.  [resolve(13,a,3,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P3 (NOT P4) P1) (OR (NOT P2) P3) (OR P1 (NOT P2))
   (OR P3 (NOT P4) (NOT P1)) (OR (NOT P4) (NOT P3)) (OR P1 P2) (OR (NOT P1) P2)
   (OR P4 (NOT P3) (NOT P1) (NOT P2)) (OR (NOT P4) P2 (NOT P3) (NOT P1)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 21279 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:35 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 5.


3 -P2 | P3.  [assumption].
4 P1 | -P2.  [assumption].
6 -P4 | -P3.  [assumption].
7 P1 | P2.  [assumption].
8 -P1 | P2.  [assumption].
9 P4 | -P3 | -P1 | -P2.  [assumption].
10A P1 | P1.  [resolve(7,b,4,b)].
10 P1.  [copy(10A),merge(b)].
11 P4 | -P3 | -P2.  [resolve(10,a,9,c)].
12 P2.  [resolve(10,a,8,a)].
14 P4 | -P3.  [resolve(12,a,11,c)].
15 P3.  [resolve(12,a,3,a)].
16 P4.  [resolve(15,a,14,b)].
17A -P3.  [resolve(16,a,6,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 P1 (NOT P2) (NOT P4)) (OR P4 P3) (OR P4 P2 (NOT P1))
   (OR (NOT P2) (NOT P4)) (OR P1 (NOT P3) (NOT P4) P2) (OR (NOT P3) P1)
   (OR (NOT P1) P3 P4) (OR (NOT P3) (NOT P1) (NOT P2) P4) (OR P2 (NOT P4)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 20943 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:33 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 10.


3 P4 | P3.  [assumption].
4 P4 | P2 | -P1.  [assumption].
5 -P2 | -P4.  [assumption].
7 -P3 | P1.  [assumption].
8 -P3 | -P1 | -P2 | P4.  [assumption].
9 P2 | -P4.  [assumption].
10 P1 | P4.  [resolve(7,a,3,b)].
11A P4 | P4 | P2.  [resolve(10,a,4,c)].
11 P4 | P2.  [copy(11A),merge(b)].
12A P4 | -P3 | -P1 | P4.  [resolve(11,b,8,c)].
12 P4 | -P3 | -P1.  [copy(12A),merge(d)].
13A P4 | -P1 | P4.  [resolve(12,b,3,b)].
13 P4 | -P1.  [copy(13A),merge(c)].
14A P4 | P4.  [resolve(13,b,10,a)].
14 P4.  [copy(14A),merge(b)].
15 P2.  [resolve(14,a,9,b)].
16A -P4.  [resolve(15,a,5,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P3) (OR P1 (NOT P3) (NOT P2)) (OR P4 (NOT P1))
   (OR P2 P1) (OR P1 P3 (NOT P4)) (OR (NOT P3) (NOT P4) (NOT P2) (NOT P1))
   (OR (NOT P1) P2) (OR (NOT P2) (NOT P1) (NOT P3)) (OR P1 P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 20039 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:28 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 8.


2 -P4 | P3.  [assumption].
3 P1 | -P3 | -P2.  [assumption].
4 P4 | -P1.  [assumption].
5 P2 | P1.  [assumption].
7 -P1 | P2.  [assumption].
8 -P2 | -P1 | -P3.  [assumption].
9 P1 | P3.  [assumption].
10A P1 | P1 | -P3.  [resolve(5,a,3,c)].
10 P1 | -P3.  [copy(10A),merge(b)].
11A P1 | P1.  [resolve(10,b,9,b)].
11 P1.  [copy(11A),merge(b)].
12 -P2 | -P3.  [resolve(11,a,8,b)].
13 P2.  [resolve(11,a,7,a)].
14 P4.  [resolve(11,a,4,b)].
15 -P3.  [resolve(13,a,12,a)].
16A P3.  [resolve(14,a,2,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 (NOT P1)) (OR P4 (NOT P3) (NOT P2))
   (OR (NOT P3) (NOT P4) (NOT P2) P1) (OR (NOT P3) (NOT P2) P1)
   (OR (NOT P2) (NOT P4) (NOT P3)) (OR (NOT P2) P3) (OR P4 P2)
   (OR (NOT P4) P1 P2) (OR (NOT P1) P4 (NOT P3) P2))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 19003 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:22 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 8.


2 P2 | -P1.  [assumption].
3 P4 | -P3 | -P2.  [assumption].
6 -P2 | -P4 | -P3.  [assumption].
7 -P2 | P3.  [assumption].
8 P4 | P2.  [assumption].
9 -P4 | P1 | P2.  [assumption].
10A P1 | P2 | P2.  [resolve(9,a,8,a)].
10 P1 | P2.  [copy(10A),merge(c)].
11A P2 | P2.  [resolve(10,a,2,b)].
11 P2.  [copy(11A),merge(b)].
12 P3.  [resolve(11,a,7,a)].
13A -P4 | -P3.  [resolve(11,a,6,a)].
13 -P4.  [resolve(12,a,13A,b)].
15A -P3 | -P2.  [resolve(13,a,3,a)].
15B -P2.  [resolve(12,a,15A,a)].
15 $F.  [resolve(11,a,15B,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P3 (NOT P4)) (OR P3 P1) (OR P4 (NOT P1))
   (OR P2 (NOT P4) (NOT P3) P1) (OR (NOT P4) (NOT P2)) (OR (NOT P3) (NOT P4))
   (OR (NOT P3) P4) (OR (NOT P3) P1 (NOT P2)) (OR P1 (NOT P2) (NOT P4)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 18179 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:17 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 -P1 | P3 | -P4.  [assumption].
3 P3 | P1.  [assumption].
4 P4 | -P1.  [assumption].
7 -P3 | -P4.  [assumption].
8 -P3 | P4.  [assumption].
10 P4 | P1.  [resolve(8,a,3,a)].
11 P1 | -P3.  [resolve(10,a,7,b)].
12A P1 | P1.  [resolve(11,b,3,a)].
12 P1.  [copy(12A),merge(b)].
13 P4.  [resolve(12,a,4,b)].
14A P3 | -P4.  [resolve(12,a,2,a)].
14 P3.  [resolve(13,a,14A,b)].
15A -P4.  [resolve(14,a,7,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 (NOT P1)) (OR (NOT P4) P3) (OR (NOT P3) P1) (OR (NOT P2) P3)
   (OR (NOT P1) (NOT P2) P3 (NOT P4)) (OR (NOT P4) (NOT P3) (NOT P2) P1)
   (OR (NOT P2) (NOT P3) (NOT P1)) (OR P3 P1 (NOT P4)) (OR P2 P4 P1))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 16655 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:09 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 8.


2 P2 | -P1.  [assumption].
3 -P4 | P3.  [assumption].
4 -P3 | P1.  [assumption].
5 -P2 | P3.  [assumption].
6 -P2 | -P3 | -P1.  [assumption].
7 P2 | P4 | P1.  [assumption].
8 P2 | P1 | P3.  [resolve(7,b,3,a)].
9A P1 | P3 | P3.  [resolve(8,a,5,a)].
9 P1 | P3.  [copy(9A),merge(c)].
10A P1 | P1.  [resolve(9,b,4,a)].
10 P1.  [copy(10A),merge(b)].
11 -P2 | -P3.  [resolve(10,a,6,c)].
12 P2.  [resolve(10,a,2,b)].
13 -P3.  [resolve(12,a,11,a)].
14A P3.  [resolve(12,a,5,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) (NOT P4)) (OR (NOT P2) P1 P4) (OR P4 (NOT P1) (NOT P3))
   (OR P2 (NOT P3) P1 (NOT P4)) (OR P2 (NOT P3) P4 P1) (OR P4 P3)
   (OR (NOT P4) P1) (OR (NOT P1) P2 (NOT P4)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 15883 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:04 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 -P2 | -P4.  [assumption].
3 -P2 | P1 | P4.  [assumption].
4 P4 | -P1 | -P3.  [assumption].
6 P2 | -P3 | P4 | P1.  [assumption].
7 P4 | P3.  [assumption].
8 -P4 | P1.  [assumption].
9 -P1 | P2 | -P4.  [assumption].
10A P4 | P2 | P4 | P1.  [resolve(7,b,6,b)].
10 P4 | P2 | P1.  [copy(10A),merge(c)].
11A P4 | P4 | -P1.  [resolve(7,b,4,c)].
11 P4 | -P1.  [copy(11A),merge(b)].
12A P4 | P1 | P1 | P4.  [resolve(10,b,3,a)].
12B P4 | P1 | P4.  [copy(12A),merge(c)].
12 P4 | P1.  [copy(12B),merge(c)].
13A P4 | P4.  [resolve(12,b,11,b)].
13 P4.  [copy(13A),merge(b)].
14 -P1 | P2.  [resolve(13,a,9,c)].
15 P1.  [resolve(13,a,8,a)].
16 -P2.  [resolve(13,a,2,b)].
17A P2.  [resolve(15,a,14,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P4)) (OR P2 (NOT P3)) (OR (NOT P1) P3)
   (OR P1 P4 P3 (NOT P2)) (OR P4 (NOT P1)) (OR (NOT P2) (NOT P3) P4)
   (OR P1 P4 P3) (OR (NOT P1) (NOT P2)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 13451 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:51 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 7.
% Maximum clause weight is 3.000.
% Given clauses 11.


2 P1 | -P4.  [assumption].
3 P2 | -P3.  [assumption].
4 -P1 | P3.  [assumption].
7 -P2 | -P3 | P4.  [assumption].
8 P1 | P4 | P3.  [assumption].
9 -P1 | -P2.  [assumption].
10A P1 | P3 | P1.  [resolve(8,b,2,b)].
10 P1 | P3.  [copy(10A),merge(c)].
11 P1 | P2.  [resolve(10,b,3,b)].
12 P1 | -P3 | P4.  [resolve(11,b,7,a)].
13A P1 | P4 | P1.  [resolve(12,b,10,b)].
13 P1 | P4.  [copy(13A),merge(c)].
14A P1 | P1.  [resolve(13,b,2,b)].
14 P1.  [copy(14A),merge(b)].
15 -P2.  [resolve(14,a,9,a)].
17 P3.  [resolve(14,a,4,a)].
18A -P3.  [resolve(15,a,3,a)].
18 $F.  [resolve(17,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P2 P3) (OR P3 P1 (NOT P4)) (OR (NOT P2) (NOT P3))
   (OR P3 P1 P4) (OR (NOT P3) P2) (OR (NOT P2) (NOT P4) P3 (NOT P1))
   (OR P4 (NOT P1)) (OR (NOT P1) P2 P4 P3))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 12587 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:46 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 12.


2 -P4 | P2 | P3.  [assumption].
3 P3 | P1 | -P4.  [assumption].
4 -P2 | -P3.  [assumption].
5 P3 | P1 | P4.  [assumption].
6 -P3 | P2.  [assumption].
7 -P2 | -P4 | P3 | -P1.  [assumption].
8 P4 | -P1.  [assumption].
9A P4 | P3 | P4.  [resolve(8,b,5,b)].
9 P4 | P3.  [copy(9A),merge(c)].
10A P3 | P3 | P1.  [resolve(9,a,3,c)].
10 P3 | P1.  [copy(10A),merge(b)].
11A P3 | P2 | P3.  [resolve(9,a,2,a)].
11 P3 | P2.  [copy(11A),merge(c)].
12A P3 | -P4 | P3 | -P1.  [resolve(11,b,7,a)].
12 P3 | -P4 | -P1.  [copy(12A),merge(c)].
13A P3 | -P4 | P3.  [resolve(12,c,10,b)].
13 P3 | -P4.  [copy(13A),merge(c)].
14A P3 | P3.  [resolve(13,b,9,a)].
14 P3.  [copy(14A),merge(b)].
15 P2.  [resolve(14,a,6,a)].
16A -P3.  [resolve(15,a,4,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P4) P2) (OR (NOT P2) (NOT P3)) (OR (NOT P3) (NOT P4) P1)
   (OR (NOT P1) P2 (NOT P3)) (OR (NOT P4) P3) (OR (NOT P2) P3) (OR P4 P2)
   (OR (NOT P2) (NOT P4) P1 (NOT P3)) (OR (NOT P4) P2 P3 P1))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 28289 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:14 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 7.


3 -P2 | -P3.  [assumption].
4 -P3 | -P4 | P1.  [assumption].
5 -P1 | P2 | -P3.  [assumption].
6 -P4 | P3.  [assumption].
7 -P2 | P3.  [assumption].
8 P4 | P2.  [assumption].
9 P2 | P3.  [resolve(8,a,6,a)].
10 P2 | -P3 | P1.  [resolve(8,a,4,b)].
11A P3 | P3.  [resolve(9,a,7,a)].
11 P3.  [copy(11A),merge(b)].
12 P2 | P1.  [resolve(11,a,10,b)].
13 -P1 | P2.  [resolve(11,a,5,c)].
15 -P2.  [resolve(11,a,3,b)].
16 -P1.  [resolve(15,a,13,b)].
17A P1.  [resolve(15,a,12,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 2 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 7.


3 -P2 | -P3.  [assumption].
4 -P3 | -P4 | P1.  [assumption].
5 -P1 | P2 | -P3.  [assumption].
6 -P4 | P3.  [assumption].
7 -P2 | P3.  [assumption].
8 P4 | P2.  [assumption].
9 P2 | P3.  [resolve(8,a,6,a)].
11A P3 | P3.  [resolve(9,a,7,a)].
11 P3.  [copy(11A),merge(b)].
13 -P1 | P2.  [resolve(11,a,5,c)].
14 -P4 | P1.  [resolve(11,a,4,a)].
15 -P2.  [resolve(11,a,3,b)].
16 -P1.  [resolve(15,a,13,b)].
18 P4.  [resolve(15,a,8,b)].
19A P1.  [resolve(18,a,14,a)].
19 $F.  [resolve(16,a,19A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 P4) (OR P3 (NOT P4)) (OR P1 P4 P2)
   (OR (NOT P1) (NOT P4) P3 (NOT P2)) (OR P2 P1 (NOT P4))
   (OR (NOT P2) (NOT P1) (NOT P3) P4) (OR P2 (NOT P3)) (OR (NOT P4) (NOT P2))
   (OR P3 P4 (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 26109 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:02 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 P1 | P4.  [assumption].
3 P3 | -P4.  [assumption].
5 -P2 | -P1 | -P3 | P4.  [assumption].
6 P2 | -P3.  [assumption].
7 -P4 | -P2.  [assumption].
8 P3 | P4 | -P1.  [assumption].
9A P3 | P4 | P4.  [resolve(8,c,2,a)].
9 P3 | P4.  [copy(9A),merge(c)].
10 P4 | P2.  [resolve(9,a,6,b)].
11A P4 | -P2 | -P1 | P4.  [resolve(9,a,5,c)].
11 P4 | -P2 | -P1.  [copy(11A),merge(d)].
12A P4 | -P1 | P4.  [resolve(11,b,10,b)].
12 P4 | -P1.  [copy(12A),merge(c)].
13A P4 | P4.  [resolve(12,b,2,a)].
13 P4.  [copy(13A),merge(b)].
14 -P2.  [resolve(13,a,7,a)].
16 P3.  [resolve(13,a,3,b)].
17A -P3.  [resolve(14,a,6,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P1) (OR (NOT P1) P3) (OR (NOT P3) (NOT P2) (NOT P1))
   (OR P3 P2 (NOT P4) P1) (OR P4 P3 P1) (OR (NOT P3) P2) (OR (NOT P4) P1 P2)
   (OR (NOT P4) (NOT P1) (NOT P3) P2) (OR (NOT P2) (NOT P4)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 23793 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:49 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 10.


2 -P3 | P1.  [assumption].
3 -P1 | P3.  [assumption].
4 -P3 | -P2 | -P1.  [assumption].
6 P4 | P3 | P1.  [assumption].
7 -P3 | P2.  [assumption].
8 -P4 | P1 | P2.  [assumption].
9 -P2 | -P4.  [assumption].
10A P1 | P2 | P3 | P1.  [resolve(8,a,6,a)].
10 P1 | P2 | P3.  [copy(10A),merge(d)].
11 -P2 | P3 | P1.  [resolve(9,b,6,a)].
12A P3 | P1 | P1 | P3.  [resolve(11,a,10,b)].
12B P3 | P1 | P3.  [copy(12A),merge(c)].
12 P3 | P1.  [copy(12B),merge(c)].
14A P1 | P1.  [resolve(12,a,2,a)].
14 P1.  [copy(14A),merge(b)].
15 -P3 | -P2.  [resolve(14,a,4,c)].
16 P3.  [resolve(14,a,3,a)].
17 -P2.  [resolve(16,a,15,a)].
18A P2.  [resolve(16,a,7,a)].
18 $F.  [resolve(17,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P4 (NOT P3)) (OR (NOT P1) (NOT P3) P4) (OR (NOT P1) P3)
   (OR P1 P4 P3) (OR (NOT P2) (NOT P4)) (OR (NOT P2) (NOT P1))
   (OR P3 (NOT P4) P2 P1) (OR (NOT P3) (NOT P2) P1 P4) (OR P2 (NOT P3)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 22011 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:39 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 -P2 | P4 | -P3.  [assumption].
4 -P1 | P3.  [assumption].
5 P1 | P4 | P3.  [assumption].
6 -P2 | -P4.  [assumption].
8 P3 | -P4 | P2 | P1.  [assumption].
9 P2 | -P3.  [assumption].
10A P3 | P2 | P1 | P1 | P3.  [resolve(8,b,5,b)].
10B P3 | P2 | P1 | P3.  [copy(10A),merge(d)].
10 P3 | P2 | P1.  [copy(10B),merge(d)].
11 P3 | P1 | -P4.  [resolve(10,b,6,a)].
12A P3 | P1 | P1 | P3.  [resolve(11,c,5,b)].
12B P3 | P1 | P3.  [copy(12A),merge(c)].
12 P3 | P1.  [copy(12B),merge(c)].
13A P3 | P3.  [resolve(12,b,4,a)].
13 P3.  [copy(13A),merge(b)].
14 P2.  [resolve(13,a,9,b)].
16A P4 | -P3.  [resolve(14,a,2,a)].
16 P4.  [resolve(13,a,16A,b)].
18A -P4.  [resolve(14,a,6,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) (NOT P1) P4) (OR (NOT P4) (NOT P1)) (OR P4 (NOT P2))
   (OR P4 (NOT P1) P3) (OR (NOT P3) P1 P2) (OR P3 P4) (OR P3 P1)
   (OR (NOT P2) (NOT P4)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 11567 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:40 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 7.
% Maximum clause weight is 3.000.
% Given clauses 13.


2 -P3 | -P1 | P4.  [assumption].
3 -P4 | -P1.  [assumption].
4 P4 | -P2.  [assumption].
6 -P3 | P1 | P2.  [assumption].
7 P3 | P4.  [assumption].
8 P3 | P1.  [assumption].
9 -P2 | -P4.  [assumption].
10 P3 | -P1.  [resolve(7,b,3,a)].
11A P3 | P3.  [resolve(10,b,8,b)].
11 P3.  [copy(11A),merge(b)].
12 P1 | P2.  [resolve(11,a,6,a)].
13 -P1 | P4.  [resolve(11,a,2,a)].
14 P1 | -P4.  [resolve(12,b,9,a)].
15 P1 | P4.  [resolve(12,b,4,b)].
16A P1 | P1.  [resolve(15,b,14,b)].
16 P1.  [copy(16A),merge(b)].
17 P4.  [resolve(16,a,13,a)].
18A -P1.  [resolve(17,a,3,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 (NOT P1) (NOT P2)) (OR P2 (NOT P1)) (OR P3 (NOT P4) P2)
   (OR P1 P4 P2) (OR P1 (NOT P3)) (OR (NOT P4) (NOT P2) (NOT P1))
   (OR P3 (NOT P2)) (OR (NOT P1) P2 P4))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5123 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:04 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 7.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 P4 | -P1 | -P2.  [assumption].
3 P2 | -P1.  [assumption].
4 P3 | -P4 | P2.  [assumption].
5 P1 | P4 | P2.  [assumption].
6 P1 | -P3.  [assumption].
7 -P4 | -P2 | -P1.  [assumption].
8 P3 | -P2.  [assumption].
9A P1 | P2 | P3 | P2.  [resolve(5,b,4,b)].
9 P1 | P2 | P3.  [copy(9A),merge(d)].
10A P1 | P2 | P1.  [resolve(9,c,6,b)].
10 P1 | P2.  [copy(10A),merge(c)].
11A P2 | P2.  [resolve(10,a,3,b)].
11 P2.  [copy(11A),merge(b)].
12 P3.  [resolve(11,a,8,b)].
13 -P4 | -P1.  [resolve(11,a,7,b)].
14 P4 | -P1.  [resolve(11,a,2,c)].
15 P1.  [resolve(12,a,6,b)].
16 P4.  [resolve(15,a,14,b)].
17A -P1.  [resolve(16,a,13,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 P3) (OR (NOT P3) (NOT P2) (NOT P4)) (OR (NOT P4) P2)
   (OR (NOT P3) P1) (OR (NOT P3) (NOT P4) P1) (OR (NOT P3) (NOT P2) (NOT P1))
   (OR (NOT P2) (NOT P4)) (OR (NOT P1) P4 (NOT P3)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 2335 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:07:49 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 8.


2 P4 | P3.  [assumption].
4 -P4 | P2.  [assumption].
5 -P3 | P1.  [assumption].
6 -P3 | -P2 | -P1.  [assumption].
7 -P2 | -P4.  [assumption].
8 -P1 | P4 | -P3.  [assumption].
9 P2 | P3.  [resolve(4,a,2,a)].
10 P3 | -P4.  [resolve(9,a,7,a)].
11A P3 | P3.  [resolve(10,b,2,a)].
11 P3.  [copy(11A),merge(b)].
12 -P1 | P4.  [resolve(11,a,8,c)].
13 -P2 | -P1.  [resolve(11,a,6,a)].
14 P1.  [resolve(11,a,5,a)].
15 -P2.  [resolve(14,a,13,b)].
16 P4.  [resolve(14,a,12,a)].
17A P2.  [resolve(16,a,4,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) (NOT P3)) (OR (NOT P1) P3) (OR P4 (NOT P2) (NOT P3) P1)
   (OR (NOT P3) (NOT P1)) (OR (NOT P1) (NOT P4) (NOT P2) (NOT P3)) (OR P3 P1)
   (OR (NOT P3) P1 P4 P2) (OR P1 P2 P3 P4))
  (:MIN-PROOF-LEVEL . 9) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 1415 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:07:44 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 9.
% Maximum clause weight is 4.000.
% Given clauses 12.


2 -P4 | -P3.  [assumption].
3 -P1 | P3.  [assumption].
4 P4 | -P2 | -P3 | P1.  [assumption].
5 -P3 | -P1.  [assumption].
6 P3 | P1.  [assumption].
7 -P3 | P1 | P4 | P2.  [assumption].
8A P1 | P4 | P2 | P1.  [resolve(7,a,6,a)].
8 P1 | P4 | P2.  [copy(8A),merge(d)].
9 P1 | P2 | -P3.  [resolve(8,b,2,a)].
10A P1 | P2 | P1.  [resolve(9,c,6,a)].
10 P1 | P2.  [copy(10A),merge(c)].
11A P1 | P4 | -P3 | P1.  [resolve(10,b,4,b)].
11 P1 | P4 | -P3.  [copy(11A),merge(d)].
12A P1 | P4 | P1.  [resolve(11,c,6,a)].
12 P1 | P4.  [copy(12A),merge(c)].
13 P1 | -P3.  [resolve(12,b,2,a)].
14A P1 | P1.  [resolve(13,b,6,a)].
14 P1.  [copy(14A),merge(b)].
15 -P3.  [resolve(14,a,5,b)].
16A P3.  [resolve(14,a,3,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 (NOT P1) (NOT P2)) (OR P1 (NOT P4) (NOT P3) (NOT P2))
   (OR (NOT P4) (NOT P1)) (OR P4 (NOT P2) (NOT P3) P1) (OR P2 (NOT P1))
   (OR (NOT P2) P3 P1) (OR P1 P3 P4 (NOT P2)) (OR (NOT P3) (NOT P4))
   (OR P2 P1))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 24797 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:55 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 P4 | -P1 | -P2.  [assumption].
4 -P4 | -P1.  [assumption].
5 P4 | -P2 | -P3 | P1.  [assumption].
6 P2 | -P1.  [assumption].
7 -P2 | P3 | P1.  [assumption].
8 -P3 | -P4.  [assumption].
9 P2 | P1.  [assumption].
10A P1 | P3 | P1.  [resolve(9,a,7,a)].
10 P1 | P3.  [copy(10A),merge(c)].
11A P1 | P4 | -P2 | P1.  [resolve(10,b,5,c)].
11 P1 | P4 | -P2.  [copy(11A),merge(d)].
12A P1 | P4 | P1.  [resolve(11,c,9,a)].
12 P1 | P4.  [copy(12A),merge(c)].
13 P1 | -P3.  [resolve(12,b,8,b)].
14A P1 | P1.  [resolve(13,b,10,b)].
14 P1.  [copy(14A),merge(b)].
15 P2.  [resolve(14,a,6,b)].
16 -P4.  [resolve(14,a,4,b)].
17A -P1 | -P2.  [resolve(16,a,2,a)].
17B -P2.  [resolve(14,a,17A,a)].
17 $F.  [resolve(15,a,17B,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 (NOT P4) P1 P3) (OR (NOT P4) P3)
   (OR (NOT P1) P4 (NOT P2) (NOT P3)) (OR P1 (NOT P3))
   (OR P2 (NOT P3) (NOT P1)) (OR P3 (NOT P1))
   (OR P2 (NOT P1) (NOT P3) (NOT P4)) (OR (NOT P3) (NOT P4)) (OR P1 P4))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 24609 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:54 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 8.


3 -P4 | P3.  [assumption].
4 -P1 | P4 | -P2 | -P3.  [assumption].
5 P1 | -P3.  [assumption].
6 P2 | -P3 | -P1.  [assumption].
7 P3 | -P1.  [assumption].
8 -P3 | -P4.  [assumption].
9 P1 | P4.  [assumption].
10 P1 | P3.  [resolve(9,b,3,a)].
11A P1 | P1.  [resolve(10,b,5,b)].
11 P1.  [copy(11A),merge(b)].
12 P3.  [resolve(11,a,7,b)].
13A P2 | -P1.  [resolve(12,a,6,b)].
13 P2.  [resolve(11,a,13A,b)].
14A P4 | -P2 | -P3.  [resolve(11,a,4,a)].
14B P4 | -P3.  [resolve(13,a,14A,b)].
14 P4.  [resolve(12,a,14B,b)].
15A -P4.  [resolve(12,a,8,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P3 (NOT P4) P1) (OR (NOT P2) (NOT P3) P1)
   (OR P4 (NOT P3) P2 P1) (OR (NOT P2) (NOT P1) (NOT P3)) (OR P4 (NOT P3))
   (OR P3 (NOT P1)) (OR (NOT P1) P2) (OR P1 P3) (OR P1 (NOT P3) P2 (NOT P4)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 23841 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:49 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 11.


3 -P2 | -P3 | P1.  [assumption].
5 -P2 | -P1 | -P3.  [assumption].
6 P4 | -P3.  [assumption].
7 P3 | -P1.  [assumption].
8 -P1 | P2.  [assumption].
9 P1 | P3.  [assumption].
10 P1 | -P3 | P2 | -P4.  [assumption].
11 P1 | P4.  [resolve(9,b,6,b)].
12A P1 | P1 | -P3 | P2.  [resolve(11,b,10,d)].
12 P1 | -P3 | P2.  [copy(12A),merge(b)].
13A P1 | P2 | P1.  [resolve(12,b,9,b)].
13 P1 | P2.  [copy(13A),merge(c)].
14A P1 | -P3 | P1.  [resolve(13,b,3,a)].
14 P1 | -P3.  [copy(14A),merge(c)].
15A P1 | P1.  [resolve(14,b,9,b)].
15 P1.  [copy(15A),merge(b)].
16 P2.  [resolve(15,a,8,a)].
17 P3.  [resolve(15,a,7,b)].
18A -P1 | -P3.  [resolve(16,a,5,a)].
18B -P3.  [resolve(15,a,18A,a)].
18 $F.  [resolve(17,a,18B,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P4 (NOT P2)) (OR P2 P3 (NOT P4)) (OR P4 (NOT P1))
   (OR P3 (NOT P1)) (OR (NOT P4) (NOT P3)) (OR (NOT P2) P1 P3)
   (OR (NOT P4) P3 (NOT P2) (NOT P1)) (OR P4 P1) (OR (NOT P4) (NOT P1) P2))
  (:MIN-PROOF-LEVEL . 8) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 22415 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:41 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 8.
% Maximum clause weight is 3.000.
% Given clauses 12.


3 P2 | P3 | -P4.  [assumption].
4 P4 | -P1.  [assumption].
5 P3 | -P1.  [assumption].
6 -P4 | -P3.  [assumption].
7 -P2 | P1 | P3.  [assumption].
8 P4 | P1.  [assumption].
10 P1 | P2 | P3.  [resolve(8,a,3,c)].
11 P1 | P2 | -P4.  [resolve(10,c,6,b)].
12A P1 | P2 | P1.  [resolve(11,c,8,a)].
12 P1 | P2.  [copy(12A),merge(c)].
13A P1 | P1 | P3.  [resolve(12,b,7,a)].
13 P1 | P3.  [copy(13A),merge(b)].
14 P1 | -P4.  [resolve(13,b,6,b)].
15A P1 | P1.  [resolve(14,b,8,a)].
15 P1.  [copy(15A),merge(b)].
17 P3.  [resolve(15,a,5,b)].
18 P4.  [resolve(15,a,4,b)].
19A -P3.  [resolve(18,a,6,a)].
19 $F.  [resolve(17,a,19A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P1 (NOT P3) P2) (OR (NOT P1) (NOT P4)) (OR (NOT P4) P1)
   (OR (NOT P2) P3 (NOT P1) P4) (OR P1 (NOT P4) (NOT P2) P3)
   (OR (NOT P3) (NOT P2) P4) (OR P2 P4) (OR P1 (NOT P2)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 16011 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:05 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 8.


3 -P1 | -P4.  [assumption].
4 -P4 | P1.  [assumption].
5 -P2 | P3 | -P1 | P4.  [assumption].
6 -P3 | -P2 | P4.  [assumption].
7 P2 | P4.  [assumption].
8 P1 | -P2.  [assumption].
9 P2 | P1.  [resolve(7,b,4,a)].
10 P2 | -P4.  [resolve(9,b,3,a)].
11A P2 | P2.  [resolve(10,b,7,b)].
11 P2.  [copy(11A),merge(b)].
12 P1.  [resolve(11,a,8,b)].
13 -P3 | P4.  [resolve(11,a,6,b)].
14A P3 | -P1 | P4.  [resolve(11,a,5,a)].
14 P3 | P4.  [resolve(12,a,14A,b)].
15 -P4.  [resolve(12,a,3,a)].
16 P3.  [resolve(15,a,14,b)].
17A P4.  [resolve(16,a,13,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P4 (NOT P2) P1) (OR (NOT P1) (NOT P4) (NOT P2))
   (OR (NOT P4) P3 P2) (OR (NOT P3) (NOT P4)) (OR P3 (NOT P4) P1 (NOT P2))
   (OR (NOT P1) P4 P3 (NOT P2)) (OR P4 (NOT P2)) (OR P2 P4))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 14495 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:57 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 8.


3 -P1 | -P4 | -P2.  [assumption].
4 -P4 | P3 | P2.  [assumption].
5 -P3 | -P4.  [assumption].
6 P3 | -P4 | P1 | -P2.  [assumption].
8 P4 | -P2.  [assumption].
9 P2 | P4.  [assumption].
10A P2 | P3 | P2.  [resolve(9,b,4,a)].
10 P2 | P3.  [copy(10A),merge(c)].
11 P2 | -P4.  [resolve(10,b,5,a)].
12A P2 | P2.  [resolve(11,b,9,b)].
12 P2.  [copy(12A),merge(b)].
13 P4.  [resolve(12,a,8,b)].
14A P3 | P1 | -P2.  [resolve(13,a,6,b)].
14 P3 | P1.  [resolve(12,a,14A,c)].
15A -P1 | -P2.  [resolve(13,a,3,b)].
15 -P1.  [resolve(12,a,15A,b)].
16 -P3.  [resolve(13,a,5,b)].
17A P1.  [resolve(16,a,14,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) (NOT P3)) (OR P4 (NOT P1)) (OR P3 (NOT P2) (NOT P4))
   (OR P2 (NOT P1)) (OR P1 (NOT P3) (NOT P2)) (OR (NOT P3) (NOT P1) P2 P4)
   (OR P4 P1 (NOT P2) P3) (OR P1 P2))
  (:MIN-PROOF-LEVEL . 8) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 7979 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:08:20 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 8.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 -P4 | -P3.  [assumption].
3 P4 | -P1.  [assumption].
4 P3 | -P2 | -P4.  [assumption].
5 P2 | -P1.  [assumption].
6 P1 | -P3 | -P2.  [assumption].
7 P4 | P1 | -P2 | P3.  [assumption].
8 P1 | P2.  [assumption].
9A P1 | P4 | P1 | P3.  [resolve(8,b,7,c)].
9 P1 | P4 | P3.  [copy(9A),merge(c)].
10A P1 | P3 | P3 | -P2.  [resolve(9,b,4,c)].
10 P1 | P3 | -P2.  [copy(10A),merge(c)].
11A P1 | P3 | P1.  [resolve(10,c,8,b)].
11 P1 | P3.  [copy(11A),merge(c)].
12A P1 | P1 | -P2.  [resolve(11,b,6,b)].
12 P1 | -P2.  [copy(12A),merge(b)].
13A P1 | P1.  [resolve(12,b,8,b)].
13 P1.  [copy(13A),merge(b)].
14 P2.  [resolve(13,a,5,b)].
15 P4.  [resolve(13,a,3,b)].
16A P3 | -P4.  [resolve(14,a,4,b)].
16 P3.  [resolve(15,a,16A,b)].
17A -P3.  [resolve(15,a,2,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) (NOT P4) (NOT P1)) (OR (NOT P4) P3) (OR P2 P1)
   (OR P1 P2) (OR (NOT P1) P4) (OR (NOT P3) P4)
   (OR (NOT P1) (NOT P4) (NOT P3) P2) (OR P4 P1) (OR (NOT P2) (NOT P3)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 32117 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:36 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 -P2 | -P4 | -P1.  [assumption].
3 -P4 | P3.  [assumption].
4 P2 | P1.  [assumption].
5 -P1 | P4.  [assumption].
7 -P1 | -P4 | -P3 | P2.  [assumption].
8 P4 | P1.  [assumption].
9 -P2 | -P3.  [assumption].
10 P1 | P3.  [resolve(8,a,3,a)].
11 P1 | -P2.  [resolve(10,b,9,b)].
12A P1 | P1.  [resolve(11,b,4,a)].
12 P1.  [copy(12A),merge(b)].
13 -P4 | -P3 | P2.  [resolve(12,a,7,a)].
14 P4.  [resolve(12,a,5,a)].
15A -P2 | -P1.  [resolve(14,a,2,b)].
15 -P2.  [resolve(12,a,15A,b)].
16A -P3 | P2.  [resolve(14,a,13,a)].
16 -P3.  [resolve(15,a,16A,b)].
17A P3.  [resolve(14,a,3,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 P3 (NOT P1) (NOT P2)) (OR P2 P1) (OR (NOT P1) P2)
   (OR P2 (NOT P1) (NOT P3) P4) (OR P1 P4) (OR (NOT P1) (NOT P3) P4 (NOT P2))
   (OR P1 (NOT P4)) (OR P2 (NOT P1) (NOT P4)) (OR (NOT P1) (NOT P2) (NOT P4)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 31993 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:35 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 6.


2 P4 | P3 | -P1 | -P2.  [assumption].
4 -P1 | P2.  [assumption].
5 P1 | P4.  [assumption].
6 -P1 | -P3 | P4 | -P2.  [assumption].
7 P1 | -P4.  [assumption].
8 -P1 | -P2 | -P4.  [assumption].
9A P1 | P1.  [resolve(7,b,5,b)].
9 P1.  [copy(9A),merge(b)].
10 -P2 | -P4.  [resolve(9,a,8,a)].
11 -P3 | P4 | -P2.  [resolve(9,a,6,a)].
12 P2.  [resolve(9,a,4,a)].
13A P4 | P3 | -P2.  [resolve(9,a,2,c)].
13 P4 | P3.  [resolve(12,a,13A,c)].
14 -P3 | P4.  [resolve(12,a,11,c)].
15 -P4.  [resolve(12,a,10,a)].
16 -P3.  [resolve(15,a,14,b)].
17A P3.  [resolve(15,a,13,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P4)) (OR P2 P1) (OR (NOT P3) (NOT P1) (NOT P4) (NOT P2))
   (OR P4 (NOT P3)) (OR (NOT P4) P1 P3 (NOT P2)) (OR (NOT P4) P3)
   (OR (NOT P4) P2 (NOT P1)) (OR (NOT P2) (NOT P3) P1 (NOT P4)) (OR P4 P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 23565 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:48 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 17.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 7.


2 P3 | -P4.  [assumption].
3 P2 | P1.  [assumption].
4 -P3 | -P1 | -P4 | -P2.  [assumption].
5 P4 | -P3.  [assumption].
6 -P4 | P2 | -P1.  [assumption].
7 -P2 | -P3 | P1 | -P4.  [assumption].
8 P4 | P3.  [assumption].
9A -P3 | P1 | -P4 | P1.  [resolve(7,a,3,a)].
9 -P3 | P1 | -P4.  [copy(9A),merge(d)].
10A P4 | P4.  [resolve(8,b,5,b)].
10 P4.  [copy(10A),merge(b)].
11 -P3 | P1.  [resolve(10,a,9,c)].
12 P2 | -P1.  [resolve(10,a,6,a)].
13 -P3 | -P1 | -P2.  [resolve(10,a,4,c)].
14 P3.  [resolve(10,a,2,b)].
15 -P1 | -P2.  [resolve(14,a,13,a)].
16 P1.  [resolve(14,a,11,a)].
17 -P2.  [resolve(16,a,15,a)].
18A -P1.  [resolve(17,a,12,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 P3 (NOT P1)) (OR P1 (NOT P2) P3 (NOT P4)) (OR P2 P4 (NOT P3))
   (OR (NOT P1) (NOT P2)) (OR P4 (NOT P2) P1)
   (OR (NOT P3) P1 (NOT P2) (NOT P4)) (OR (NOT P2) P4 P3)
   (OR (NOT P4) (NOT P3)) (OR P3 P2))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 22307 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:41 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 10.


3 P1 | -P2 | P3 | -P4.  [assumption].
4 P2 | P4 | -P3.  [assumption].
5 -P1 | -P2.  [assumption].
6 P4 | -P2 | P1.  [assumption].
9 -P4 | -P3.  [assumption].
10 P3 | P2.  [assumption].
11A P2 | P2 | P4.  [resolve(10,a,4,c)].
11 P2 | P4.  [copy(11A),merge(b)].
12 P2 | -P3.  [resolve(11,b,9,a)].
13A P2 | P2.  [resolve(12,b,10,a)].
13 P2.  [copy(13A),merge(b)].
15 P4 | P1.  [resolve(13,a,6,b)].
16 -P1.  [resolve(13,a,5,b)].
17A -P2 | P3 | -P4.  [resolve(16,a,3,a)].
17 P3 | -P4.  [resolve(13,a,17A,a)].
18 P4.  [resolve(16,a,15,b)].
19 P3.  [resolve(18,a,17,b)].
20A -P3.  [resolve(18,a,9,a)].
20 $F.  [resolve(19,a,20A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P2 (NOT P4) P3) (OR (NOT P3) (NOT P4)) (OR P4 P2)
   (OR (NOT P2) (NOT P1) P3 (NOT P4)) (OR P3 P1 P2) (OR P4 (NOT P1))
   (OR P1 (NOT P2)) (OR (NOT P2) P1) (OR (NOT P3) (NOT P4) P1 (NOT P2)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 12)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 24741 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:54 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 17.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 -P1 | P2 | -P4 | P3.  [assumption].
3 -P3 | -P4.  [assumption].
4 P4 | P2.  [assumption].
5 -P2 | -P1 | P3 | -P4.  [assumption].
6 P3 | P1 | P2.  [assumption].
7 P4 | -P1.  [assumption].
8 P1 | -P2.  [assumption].
9A P2 | -P1 | P2 | P3.  [resolve(4,a,2,c)].
9 P2 | -P1 | P3.  [copy(9A),merge(c)].
10 P1 | P2 | -P4.  [resolve(6,a,3,a)].
11A P1 | P2 | P2.  [resolve(10,c,4,a)].
11 P1 | P2.  [copy(11A),merge(c)].
12A P1 | P1.  [resolve(11,b,8,b)].
12 P1.  [copy(12A),merge(b)].
13 P2 | P3.  [resolve(12,a,9,b)].
14 P4.  [resolve(12,a,7,b)].
15A -P2 | P3 | -P4.  [resolve(12,a,5,b)].
15 -P2 | P3.  [resolve(14,a,15A,c)].
16 -P3.  [resolve(14,a,3,b)].
17 -P2.  [resolve(16,a,15,b)].
18A P3.  [resolve(17,a,13,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P4) (OR P1 (NOT P4) (NOT P3) (NOT P2))
   (OR (NOT P1) P4 (NOT P2) (NOT P3)) (OR (NOT P3) (NOT P1) (NOT P4) (NOT P2))
   (OR (NOT P4) (NOT P1) P3) (OR P2 P4) (OR (NOT P4) P2) (OR (NOT P2) P1 P3)
   (OR (NOT P1) P3 (NOT P2) P4))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 12)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 21187 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:34 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 18.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 -P2 | P4.  [assumption].
3 P1 | -P4 | -P3 | -P2.  [assumption].
4 -P3 | -P1 | -P4 | -P2.  [assumption].
5 -P4 | -P1 | P3.  [assumption].
6 P2 | P4.  [assumption].
7 -P4 | P2.  [assumption].
8 -P2 | P1 | P3.  [assumption].
9A P2 | P2.  [resolve(7,a,6,b)].
9 P2.  [copy(9A),merge(b)].
10 P1 | P3.  [resolve(9,a,8,a)].
11 -P3 | -P1 | -P4.  [resolve(9,a,4,d)].
12 P1 | -P4 | -P3.  [resolve(9,a,3,d)].
13 P4.  [resolve(9,a,2,a)].
14 P1 | -P3.  [resolve(13,a,12,b)].
15 -P3 | -P1.  [resolve(13,a,11,c)].
16 -P1 | P3.  [resolve(13,a,5,a)].
17A P1 | P1.  [resolve(14,b,10,b)].
17 P1.  [copy(17A),merge(b)].
18 P3.  [resolve(17,a,16,a)].
19A -P1.  [resolve(18,a,15,a)].
19 $F.  [resolve(17,a,19A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) (NOT P1) (NOT P3) P4) (OR P3 (NOT P2) P4) (OR P2 P3)
   (OR (NOT P4) (NOT P3)) (OR P3 (NOT P4)) (OR (NOT P4) (NOT P2) P1)
   (OR P2 (NOT P1)) (OR P1 (NOT P3) P4 (NOT P2)) (OR (NOT P3) P4 P2))
  (:MIN-PROOF-LEVEL . 8) (:MIN-PROOF-LENGTH . 13)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 26665 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:10:05 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 19.
% Level of proof is 8.
% Maximum clause weight is 4.000.
% Given clauses 14.


2 -P2 | -P1 | -P3 | P4.  [assumption].
3 P3 | -P2 | P4.  [assumption].
4 P2 | P3.  [assumption].
5 -P4 | -P3.  [assumption].
6 P3 | -P4.  [assumption].
9 P1 | -P3 | P4 | -P2.  [assumption].
10 -P3 | P4 | P2.  [assumption].
11A P4 | P2 | P2.  [resolve(10,a,4,b)].
11 P4 | P2.  [copy(11A),merge(c)].
12 P2 | -P3.  [resolve(11,a,5,a)].
13A P2 | P2.  [resolve(12,b,4,b)].
13 P2.  [copy(13A),merge(b)].
14 P1 | -P3 | P4.  [resolve(13,a,9,d)].
16 P3 | P4.  [resolve(13,a,3,b)].
17 -P1 | -P3 | P4.  [resolve(13,a,2,a)].
19A P3 | P3.  [resolve(16,b,6,b)].
19 P3.  [copy(19A),merge(b)].
20 -P1 | P4.  [resolve(19,a,17,b)].
21 P1 | P4.  [resolve(19,a,14,b)].
22 -P4.  [resolve(19,a,5,b)].
23 P1.  [resolve(22,a,21,b)].
24A P4.  [resolve(23,a,20,a)].
24 $F.  [resolve(22,a,24A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) (NOT P1) P2) (OR P2 (NOT P4) (NOT P3) P1)
   (OR (NOT P2) P1 P4 (NOT P3)) (OR (NOT P4) P3) (OR P3 (NOT P1))
   (OR (NOT P2) (NOT P4) (NOT P3)) (OR (NOT P2) P3) (OR P1 P2)
   (OR P4 (NOT P1)))
  (:MIN-PROOF-LEVEL . 8) (:MIN-PROOF-LENGTH . 13)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 24561 was started by Naveen on carn-walk-wl-23.dynamic2.rpi.edu,
Sun Jun 23 13:09:53 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 17.
% Level of proof is 8.
% Maximum clause weight is 4.000.
% Given clauses 13.


2 -P3 | -P1 | P2.  [assumption].
4 -P2 | P1 | P4 | -P3.  [assumption].
6 P3 | -P1.  [assumption].
7 -P2 | -P4 | -P3.  [assumption].
8 -P2 | P3.  [assumption].
9 P1 | P2.  [assumption].
10 P4 | -P1.  [assumption].
11 P1 | P3.  [resolve(9,b,8,a)].
12A P1 | P1 | P4 | -P3.  [resolve(9,b,4,a)].
12 P1 | P4 | -P3.  [copy(12A),merge(b)].
13A P1 | P4 | P1.  [resolve(12,c,11,b)].
13 P1 | P4.  [copy(13A),merge(c)].
14 P1 | -P2 | -P3.  [resolve(13,b,7,b)].
15A P1 | -P3 | P1.  [resolve(14,b,9,b)].
15 P1 | -P3.  [copy(15A),merge(c)].
16A P1 | P1.  [resolve(15,b,11,b)].
16 P1.  [copy(16A),merge(b)].
17 P4.  [resolve(16,a,10,b)].
18 P3.  [resolve(16,a,6,b)].
19A -P1 | P2.  [resolve(18,a,2,a)].
19 P2.  [resolve(16,a,19A,a)].
20A -P4 | -P3.  [resolve(19,a,7,a)].
20B -P3.  [resolve(17,a,20A,a)].
20 $F.  [resolve(18,a,20B,a)].

============================== end of proof ==========================
"))) 