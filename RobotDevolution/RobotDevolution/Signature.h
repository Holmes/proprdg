//
//  Signature.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/23/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
@interface Signature : NSObject
{    NSInteger arity;
     ccColor3B color;
    
    NSInteger id_n;
    BOOL variable;
}   
@property (atomic,assign) NSInteger arity;
@property (atomic,assign) NSInteger id_n;
@property (atomic,assign) BOOL variable;

@property (nonatomic,assign) ccColor3B color;

+(id)withArity:(NSInteger) arity andColor:(ccColor3B) color andVariable:(BOOL) var;
+(id)withId:(NSInteger) id_n ;

@end
