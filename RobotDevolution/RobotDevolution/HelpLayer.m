//
//  HelpLayer.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "HelpLayer.h"
#import "GameConfig.h"
#define TOTAL_HELP_SLIDES 4
@interface HelpLayer()
{
    int current_screen;
    CCSprite* help1;
    CCMenuItemImage *backbutton;
    CCMenuItemImage *forwardbutton;
    int dir;
}
@end

@implementation HelpLayer
-(void)openMotalenSite {
    [[GameManager sharedGameManager]
     openSiteWithLinkType:kLinkTypeMotalen];
}

-(void) mainMenu{
    [[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:swipeForward];
    [[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:swipeBackward];
    [swipeForward release];
    [swipeBackward release];
    [[GameManager sharedGameManager] runSceneWithID:kMainMenuScene];
}
-(void) displayHelpSlide{
    CGFloat offset=0;
    if (dir==1) {
        offset=-[help1 boundingBox].size.width*.5;
    }
    else
        offset=[help1 boundingBox].size.width;;
    
    CCSequence *seq = [CCSequence actions:
                       [CCMoveTo actionWithDuration:0.5 position:ccp(offset, help1.position.y)],
                       [CCCallFunc actionWithTarget:self selector:@selector(disappeard)], nil];
    [help1 runAction:seq];
    



}
-(void)disappeard{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CGFloat offset=0;
    if (dir==1) {
        offset=[help1 boundingBox].size.width;
    }
    else
        offset=-[help1 boundingBox].size.width*.5;
    

    NSString* curr_screen_name= [NSString stringWithFormat:@"help_screens_%d.png", current_screen];
    [help1 setTexture:[[CCSprite spriteWithFile:curr_screen_name]texture]];
    help1.position=ccp(offset,winSize.height/2);
    CCAction *seq =  [CCMoveTo actionWithDuration:0.5 position:ccp(winSize.width/2,winSize.height/2)];

                       [help1 runAction:seq];

}
-(void) setUpUI{
    CGSize winSize = [[CCDirector sharedDirector] winSize];

    CGFloat padding=900;
    backbutton=[CCMenuItemImage itemFromNormalImage:@"back_unsel.png" selectedImage:@"back_sel.png" disabledImage:@"back_dis.png" target:self selector:@selector(backward)];
    forwardbutton=[CCMenuItemImage itemFromNormalImage:@"front_unsel.png" selectedImage:@"front_sel.png" disabledImage:@"front_dis.png" target:self selector:@selector(forward)];
    [backbutton setIsEnabled:false];
    [forwardbutton setIsEnabled:true];
    
    CCMenu *bottomMenu=[CCMenu menuWithItems:backbutton,forwardbutton, nil];
    bottomMenu.position=ccp(0,0);
    [bottomMenu setScale:0.5];

    backbutton.position=ccp(BOTTOM_MENU_X,-winSize.height*.5+[backbutton boundingBox].size.height);
    forwardbutton.position=ccp(winSize.width-BOTTOM_MENU_X,-winSize.height*.5+[forwardbutton boundingBox].size.height);
    [self addChild:bottomMenu];
    


}

-(void) displayHelp{
    NSString* curr_screen_name= [NSString stringWithFormat:@"help_screens_%d.png", current_screen];
    help1=[CCSprite spriteWithFile:curr_screen_name];

    CGSize winSize = [[CCDirector sharedDirector] winSize];
    [self addChild:help1];
    
    CGFloat xscale=winSize.width/[help1 textureRect].size.width;
    CGFloat yscale=winSize.height/[help1 textureRect].size.height;
    
    help1.scale=MIN(xscale, yscale);
    help1.position=ccp(winSize.width/2,winSize.height/2);

 //   help1=[CCSprite spriteWithFile:curr_screen_name];
    CCMenuItemImage *back=[CCMenuItemImage itemFromNormalImage:@"arrow-left.png" selectedImage:@"arrow-left-sel.png" target:self selector:@selector(mainMenu)];
    back.scale=0.5;
    CCMenu *menu=[CCMenu menuWithItems:back, nil];
    menu.position=ccp(BACK_BUTTON_X,winSize.height-BACK_BUTTON_Y);
    
    [self addChild:menu];
    
    swipeForward =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(forward)] ;
    [swipeForward setDirection:UISwipeGestureRecognizerDirectionUp];
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:swipeForward];

    [swipeForward setNumberOfTouchesRequired:1];
    
     swipeBackward =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backward)] ;
    [swipeBackward setDirection:UISwipeGestureRecognizerDirectionDown];
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:swipeBackward];
    
    [swipeBackward setNumberOfTouchesRequired:1];
    
    [self setUpUI];


    
}

-(void)forward{
     NSLog(@"forward");
    if (current_screen<TOTAL_HELP_SLIDES) {
        current_screen++;
        dir=1;

        [self displayHelpSlide];
        [backbutton setIsEnabled:true];

    }
    if(current_screen==TOTAL_HELP_SLIDES){
        [forwardbutton setIsEnabled:false];

    }

}
-(void)backward{
    NSLog(@"backward");
    if (current_screen>1) {
        current_screen--;
        dir=-1;

        [self displayHelpSlide];
        [forwardbutton setIsEnabled:true];
    }
    if(current_screen==1){
        [backbutton setIsEnabled:false];

    }

}
-(id)init {
    self = [super initWithColor:ccc4(255, 255, 255, 255)] ;
    if (self != nil) {
        current_screen=1;
        dir=1;
        [self displayHelp];
    }
    return self;
}

@end
