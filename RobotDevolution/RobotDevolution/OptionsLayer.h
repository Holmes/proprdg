//
//  OptionsLayer.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 1/6/13.
//  Copyright 2013 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

@interface OptionsLayer : CCLayerColor {
    CCMenuItemImage *resetButton;
}

@end
