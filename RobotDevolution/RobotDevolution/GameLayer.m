//
//  HelloWorldLayer.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright Rensselaer Polytechnic Institute 2012. All rights reserved.
//


// Import the interfaces
#import "GameLayer.h"
#import "Head.h"
#import "Robot.h"
#import "Block.h"
#import "Signature.h"
#import "stdlib.h"
#import "GameConfig.h"
#import "GameManager.h"
#import "GameState.h"
// HelloWorldLayer implementation
@interface GameLayer ()
{
    UILongPressGestureRecognizer *longPress;
    bool reloading;
    CCMenu *topMenu, *bottomMenu, *trashMenu;
    NSInteger selected_index;
    CGPoint last_touch;
    CCArray* fountain;
    NSInteger puzzle_id;
    
    CGFloat swidth;
    CGPoint trash_pos;
    CGFloat prison_y;
    CGFloat prison_door_left;
    CGFloat prison_door_right;
    CGFloat trash_area_x;;
    
    CCMenuItemImage *undoButton;
    CCMenuItemImage *redoButton;
    CCMenuItemImage *trashButton;
    CCLabelTTF* scoreLabel;
    
    CGFloat     lastScale;
    CGPoint     lastPoint;
    
    CGFloat screen_x_bound;
    CGFloat screen_y_bound;
    
}
@end

@implementation GameLayer

+(GameLayer *) layerWithPuzzleID:(NSInteger) puzzleID
{
	GameLayer *layer = [GameLayer layerWithColor:ccc4(255, 255, 255, 255)];
    [layer initLayerWithPuzzleID:puzzleID];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"THEME1.mp3" loop:YES];
    
    
	return layer;
}

-(void) initLayerWithPuzzleID:(NSInteger) _puzzleID{
    UISwipeGestureRecognizer *pinch = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(flick:)];
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:pinch];
    screen_x_bound=[[CCDirector sharedDirector] winSize].width;
    screen_y_bound=[[CCDirector sharedDirector] winSize].height;
    longPress = [[UILongPressGestureRecognizer alloc]
                 initWithTarget:self action:@selector(handleLongPress:)];
    
    // [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:longPress];
    longPress.minimumPressDuration=2;
    
    //UIPinchGestureRecognizer *zoom = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoom:)];
    //[[[CCDirector sharedDirector] openGLView] addGestureRecognizer:zoom];
    
    puzzle_id=_puzzleID;
    [self setUpHUD];
    
    GameState* gameState= [GameState sharedGameState];
    GameManager* gm=[GameManager sharedGameManager];
    
    // Game is not reloading. We have some persistent state.
    if (puzzle_id==gameState.level && !gm.reloading) {
        reloading=true;
        worldObjects = [[gameState current] retain];
        for (int i = 0; i< [worldObjects count]; i++) {
            WorldObject* curr=(WorldObject*)[worldObjects objectAtIndex:i];
            if ([self isNearTrash:curr.position]) {
                [curr runAction:[self removeFromTrashEffect]];
            }
            [curr resumeSchedulerAndActions];
            curr.position=curr.position;
            [self addChild:curr];
            [self refreshHUD];
        }
        
        
    }
    // Starting fresh
    else {
        worldObjects=[[CCArray alloc] init];
        
        [self initializePuzzle];
        [self setUpGameState];
        gameState.level=puzzle_id;
        gm.reloading=false;
        
    }
    
    //[self schedule:@selector(move:) interval:1];
    
}
-(void) reset{
    GameManager* gm=[GameManager sharedGameManager];
    
    gm.reloading=true;
    [gm reload];
    
}
-(void) clearCurrentState{
    for (int i = 0; i< [worldObjects count]; i++) {
        [self removeChild:[worldObjects objectAtIndex:i] cleanup:false];
        [(CCNode*)[worldObjects objectAtIndex:i] removeFromParentAndCleanup:false];
    }
}
-(void) refreshCurrenState{
    for (int i = 0; i< [worldObjects count]; i++) {
        WorldObject* curr=(WorldObject*)[worldObjects objectAtIndex:i];
        if ([self isNearTrash:curr.position]) {
            [curr runAction:[self removeFromTrashEffect]];
        }
        curr.position=curr.position;
        [self addChild:curr];
    }
}
- (void) undo{
    if([[GameState sharedGameState] canUndo]){
        [[SimpleAudioEngine sharedEngine] playEffect:@"button.mp3"];//play a sound
        
        [self clearCurrentState];
        [worldObjects release];
        worldObjects=[[[GameState sharedGameState] undo] retain];
        [self refreshCurrenState];
        [redoButton setIsEnabled:true];
    }
    
    [self refreshHUD];
}

- (void) redo{
    if([[GameState sharedGameState] canRedo]){
        [[SimpleAudioEngine sharedEngine] playEffect:@"button.mp3"];//play a sound
        
        [self clearCurrentState];
        [worldObjects release];
        worldObjects=[[[GameState sharedGameState] redo] retain];
        [self refreshCurrenState];
        [undoButton setIsEnabled:true];
        
    }
    [self refreshHUD];
    
}

- (void)refreshHUD{
    
    [redoButton setIsEnabled:[[GameState sharedGameState] canRedo]];
    [undoButton setIsEnabled:[[GameState sharedGameState] canUndo]];
    [self updateScore];
    
}
- (void) updateState{
    [[GameState sharedGameState] updateWithState:worldObjects];
    [self refreshHUD];
    
}
-(void) updateScore{
    [scoreLabel setString:[NSString stringWithFormat:@" Used up: $%.2f",[[GameState sharedGameState] cost]]];
}
- (void) setUpGameState{
    GameState* gameState= [GameState sharedGameState];
    //  [gameState readFromSaved];
    [gameState initializeGameRobots:worldObjects];
    [self updateState];
    
    
}
- (void) setUpHUD{
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    prison_y= round([CCDirector sharedDirector].winSize.height*0.1);
    
    self.isTouchEnabled=YES;
    CCLabelTTF* label=[CCLabelTTF labelWithString:[NSString stringWithFormat:@" Prison Number: %d",puzzle_id]  fontName:@"Helvetica" fontSize:20];
    
    [label setColor:ccc3(0, 0, 0)];
    [self addChild: label];
    label.position=ccp(size.width*.5,size.height-40);
    
    scoreLabel=[CCLabelTTF labelWithString:[NSString stringWithFormat:@" Score: $ 0"]  fontName:@"Helvetica" fontSize:20];
    
    [scoreLabel setColor:ccc3(0, 0, 0)];
    [self addChild: scoreLabel];
    scoreLabel.position=ccp(size.width*.8,size.height-40);
    
    CCMenuItemImage *back=[CCMenuItemImage itemFromNormalImage:@"arrow-left.png" selectedImage:@"arrow-left-sel.png" target:self selector:@selector(mainMenu)];
    back.scale=0.5;
    topMenu=[CCMenu menuWithItems:back, nil];
    topMenu.position=ccp(BACK_BUTTON_X,size.height-BACK_BUTTON_Y);
    
    [self addChild:topMenu];
    
    undoButton=[CCMenuItemImage itemFromNormalImage:@"undo_n.png" selectedImage:@"undo_s.png" disabledImage:@"undo_d.png" target:self selector:@selector(undo)];
    redoButton=[CCMenuItemImage itemFromNormalImage:@"redo_n.png" selectedImage:@"redo_s.png" disabledImage:@"redo_d.png" target:self selector:@selector(redo)];
    CCMenuItemImage *resetButton=[CCMenuItemImage itemFromNormalImage:@"reset_g_n.png" selectedImage:@"reset_g_s.png" disabledImage:@"reset_g_d.png" block:^(id sender)
                                  {                UIAlertView* dialog = [[UIAlertView alloc] init];
                                      
                                      [dialog setDelegate:self];
                                      [dialog setTitle:@"Reset"];
                                      [dialog setMessage:@"Do you want to rest the puzzle?"];
                                      [dialog addButtonWithTitle:@"Reset!"];
                                      
                                      [dialog addButtonWithTitle:@"Cancel!"];
                                      [dialog show];
                                      [dialog release];
                                  }];
    //resetButton.scale=0.50;
    [undoButton setIsEnabled:false];
    [redoButton setIsEnabled:false];
    bottomMenu=[CCMenu menuWithItems:undoButton,redoButton,resetButton, nil];
    CGFloat totalButtonSize=[undoButton boundingBox].size.width+[redoButton boundingBox].size.width+[resetButton boundingBox].size.width;
    bottomMenu.position=ccp(BOTTOM_MENU_X+totalButtonSize,BOTTOM_MENU_Y);
    [bottomMenu alignItemsHorizontallyWithPadding:20.0];
    [self addChild:bottomMenu];
    
    trashButton=[CCMenuItemImage itemFromNormalImage:@"trash_n.png" selectedImage:@"trash_n.png" disabledImage:@"trash_d.png" target:self selector:@selector(trash)];
    
    trashMenu=[CCMenu menuWithItems:trashButton, nil];
    
    trash_pos=ccp(size.width-[trashButton boundingBox].size.width,BOTTOM_MENU_Y*1.2);
    trashMenu.position=trash_pos;
    [self addChild:trashMenu];
    
    
    prison_door_left=size.width*.5-PRISON_WALL_WIDTH*.5;
    prison_door_right=size.width*.5+PRISON_WALL_WIDTH*.5;
    trash_area_x=size.width-TRASH_SQUARE_DIM;
    swidth=size.width;
    
}
-(void) alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex{
	NSLog(@"%d", (int) buttonIndex);
    if (buttonIndex==0) {
        [self reset];
    }
}
- (void) initializePuzzle {
    CGSize size = [[CCDirector sharedDirector] winSize];
    //
    //    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3.amazonaws.com/PropRDG/%d", puzzle_id]];
    //    NSData *data = [NSData dataWithContentsOfURL:URL];
    //
    //    // Assuming data is in UTF8.
    //    NSString *puzzle_spec = [NSString stringWithUTF8String:[data bytes]];
    NSArray* robots_spec_text=[self getLevelSpec:puzzle_id];
    //NSArray *robots_spec_text=[puzzle_spec componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (int i=0; i<[robots_spec_text count]; i++) {
        CCArray *robot_spec=[[[CCArray alloc] init]autorelease];
        
        NSString *current_robot_spec=(NSString*)[robots_spec_text objectAtIndex:i];
        NSArray *current_blocks= [current_robot_spec componentsSeparatedByString:@"|"];
        for (int j =0; j<[current_blocks count]; j++) {
            CCArray *block_spec=[[[CCArray alloc] init] autorelease];
            Signature *bs=[Signature withId:[(NSString*)[current_blocks objectAtIndex:j] intValue]];
            [block_spec addObject:bs];
            [bs release];
            [robot_spec addObject:block_spec];
        }
        
        Robot* r = [Robot robotFromRawSpecs:robot_spec];
        [self addWorldObject:r];
        r.position= ccp(size.width*.5 + arc4random()% (int) 250 , size.height*.5+arc4random()% 350);
        // [r runAction:[CCMoveTo actionWithDuration:20 position:ccp(size.width*.5 + arc4random()% (int) 250 , size.height*.5+arc4random()% 350)]];
        
    }
    
}

- (void)addWorldObject:(CCNode*)obj{
    [worldObjects addObject:obj];
    [self addChild:obj z:0];
    
}

- (NSArray*) getLevelSpec:(int) level{
    
    NSString* levelString= [NSString stringWithFormat:@"Level_%d", level];
    NSString *path = [[NSBundle mainBundle] pathForResource:LEVEL_DATA_FILE ofType:@"plist"];
    NSDictionary *rootDict = [[[NSDictionary alloc] initWithContentsOfFile:path] autorelease];
    NSArray *robots = [rootDict objectForKey:levelString];
    return robots;
    
}
-(int) getTotalInstances{
    return TOTAL_LEVELS;
}

-(void) registerWithTouchDispatcher{
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    selected_index=[self findIndexTouched:touch in:worldObjects];
    longPress.enabled=YES;
    last_touch=[self convertTouchToNodeSpace: touch];
    CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
    
    if(selected_index>=0){
        
        //   CCNode* temp=(CCNode*) [worldObjects objectAtIndex:selected_index];
        [self bringToTop:[worldObjects objectAtIndex:selected_index]];
        // [temp runAction:[self bubbleEffect]];
        
    }
    return YES;
    
}

-(void) bringToTop:(WorldObject*) obj{
    int index=-1;
    int maxZ=-1;
    for (int i=0; i<[worldObjects count]; i++) {
        WorldObject* temp=(WorldObject*) [worldObjects objectAtIndex:i];
        if (([temp zOrder]>maxZ)) {
            maxZ=[temp zOrder];
            index=i;
        }
    }
    [self reorderChild:obj z:maxZ+1];
    
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
    if(selected_index>=0){
        CGFloat xdiff= touchLocation.x -last_touch.x;
        CGFloat ydiff= touchLocation.y -last_touch.y;
        last_touch=touchLocation;
        
        Robot* temp=(Robot*) [worldObjects objectAtIndex:selected_index];
        //  if(touchLocation.y<=prison_y+PRISON_WALL_BUFFER) return;
        CGPoint newPos=ccp(temp.position.x+xdiff, temp.position.y+ydiff);
       
//         if ([self overlapsWithPrisonDoor:temp withNewPos:newPos]|[self goesout:temp withNewPos:newPos])
        if ([self overlapsWithPrisonDoor:temp withNewPos:newPos]) {
            
            return;
        }
         temp.position=ccp(round(newPos.x),round(newPos.y));
        
    }
    
    
}
-(BOOL) overlapsWithPrisonDoor:(Robot*) robot withNewPos:(CGPoint) newPos{

    return (newPos.y-robot.effectiveheight<=prison_y) &
    (([robot boundary_x_withNextPos:newPos]<prison_door_right+2 & [robot boundary_x_withNextPos:newPos]>prison_door_left-2)|
     ([robot boundary_y_withNextPos:newPos]<prison_door_right+2 & [robot boundary_y_withNextPos:newPos]>prison_door_left-2)|
     ([robot boundary_x_withNextPos:newPos]<prison_door_right+2 & [robot boundary_y_withNextPos:newPos]>prison_door_left-2));

}

-(BOOL) goesout:(Robot*) robot withNewPos:(CGPoint) newPos{
    
    return (([robot boundary_x_withNextPos:newPos]<0 )|
          (  newPos.y-robot.effectiveheight<0 )| (newPos.y+robot.effectiveheight>screen_y_bound));
             
    //|  ([robot boundary_y_withNextPos:newPos]>screen_x_bound & newPos.y>prison_y)
}

-(BOOL) isNearTrash:(CGPoint) touch{
    //TODO: Take into account the length of the selected robot
   // CGSize size = [[CCDirector sharedDirector] winSize];
    return ccpDistance(trash_pos,touch)<TRASH_THRESHOLD;
//  /  return CGRectContainsPoint(CGRectMake(size.width-TRASH_SQUARE_DIM, 0, TRASH_SQUARE_DIM, TRASH_SQUARE_DIM),touch);
}
-(void) trashRobot:(id) timer{
    Robot* robot=(Robot*) [worldObjects objectAtIndex:selected_index];
    [self removeChild:robot cleanup:true];
    [worldObjects removeObject:robot];
    [self updateState];
    
    selected_index=-1;
    
}
-(CCSequence*) bounceEffect:(CGFloat) x{
    CCJumpTo* jumpTo= [CCJumpTo actionWithDuration:0.1 position:ccp(x,prison_y+PRISON_WALL_BUFFER*1.2) height:2 jumps:1];
    CCSequence* sequence = [CCSequence actions:jumpTo, nil];
    return  sequence;
}
-(CCSequence*) trashEffect{
    CCScaleTo* shrink= [CCScaleTo actionWithDuration:BUBBLE_DURATION scale: 0.1];
    
    CCSequence* sequence = [CCSequence actions:shrink, nil];
    return  sequence;
}

-(CCSequence*) birthEffect{
    CCScaleTo* grow= [CCScaleTo actionWithDuration:BUBBLE_DURATION scale: 1];
    
    CCSequence* sequence = [CCSequence actions:grow, nil];
    return  sequence;
}
-(CCSequence*) removeFromTrashEffect{
    CCScaleTo* grow= [CCScaleTo actionWithDuration:BUBBLE_DURATION scale: 1];
    CCJumpTo* jumpTo= [CCJumpTo actionWithDuration:0.3 position:ccp(swidth/2,TRASH_SQUARE_DIM+200) height:2 jumps:1];
    
    CCSequence* sequence = [CCSequence actions:grow,jumpTo, nil];
    return  sequence;
}


-(CCSequence*) bubbleEffect{
    CCScaleTo* grow= [CCScaleTo actionWithDuration:BUBBLE_DURATION scale: BUBBLE_SCALE];
    CCScaleTo* shrink= [CCScaleTo actionWithDuration:BUBBLE_DURATION scale: 1];
    
    CCSequence* sequence = [CCSequence actions:grow,shrink, nil];
    return  sequence;
}
- (CCSequence*) escape:(ccTime) dur{
    CGSize size = [[CCDirector sharedDirector] winSize];

    CCJumpTo* jumpTo1= [CCJumpTo actionWithDuration:dur position:ccp(round(size.width*.5),prison_y) height:2 jumps:2];
    CCJumpTo* jumpTo2= [CCJumpTo actionWithDuration:0.25 position:ccp(round(size.width*.5),prison_y+40)height:2 jumps:2];
    CCJumpTo* jumpTo3= [CCJumpTo actionWithDuration:0.25 position:ccp(round(size.width*.5),prison_y) height:2 jumps:2];
    CCJumpTo* jumpTo4= [CCJumpTo actionWithDuration:0.25 position:ccp(round(size.width*.5),prison_y+40) height:2 jumps:2];
    CCJumpTo* jumpTo5= [CCJumpTo actionWithDuration:0.25 position:ccp(round(size.width*.5),prison_y) height:2 jumps:2];
    CCJumpTo* jumpTo6= [CCJumpTo actionWithDuration:0.5 position:ccp(round(size.width*.5),-30) height:2 jumps:2];
    
    CCSequence* sequence = [CCSequence actions:jumpTo1,jumpTo2,jumpTo3,jumpTo4,jumpTo5,jumpTo6, nil];
    return  sequence;
}
- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    if(selected_index>=0){
        CCNode* temp=(CCNode*) [worldObjects objectAtIndex:selected_index];
        
        if([self isNearTrash:temp.position]){
            [self trashRobot:temp];
            //[temp runAction:[self trashEffect]];
            //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(trashRobot:) userInfo:nil repeats:NO];
            
        }
        
        if ([temp isMemberOfClass:[Robot class]]) {
            for (int i=0; i<[worldObjects count]; i++) {
                WorldObject* base=(WorldObject*) [worldObjects objectAtIndex:i];
                if(base==temp || ![base isMemberOfClass:[Robot class]])
                    continue;
                Robot * r1=(Robot*) temp;
                Robot * r2=(Robot*) base;
                CGPoint p1= r1.position;
                CGPoint p2= r2.position;
                CGFloat dist = ccpDistance(p1, p2);
                
                
                
                if(dist < MATING_DISTANCE){
                    
                    CCArray* children=[r1 canCombineWith:r2] ;
                    for (Robot* child in children)
                    {
                        [self moveHeartFrom:r1.position to:r2.position];
                        
                        [self addWorldObject:child];
                        [self bringToTop:child];
                        
                        // ans.scale=0.1;
                        // [ans runAction:[self birthEffect]];
                        [undoButton setIsEnabled:true];
                        
                        
                        child.position=[self positionChildParent1:r1 andParent2:r2 andChild:child ];
                        
                        if ([self overlapsWithPrisonDoor:child withNewPos:child.position]) {
                            [child runAction:[self bounceEffect:child.position.x]];
                        }
                        
                        if([child bodyLess])
                        {
                            //[ans runAction:[self bubbleEffect]];
                            [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
                            
                            [[SimpleAudioEngine sharedEngine] playEffect:@"levelUPPP.mp3"];
                            [[SimpleAudioEngine sharedEngine] playEffect:@"Hurrah4-cheering.wav"];
                           
                            CGFloat dist=ccpDistance(ccp(prison_door_left,prison_y), child.position);
                            
                            ccTime dur=MIN(3, dist/FINAL_VELOCITY);

                            [self finalEffects:child andDuration:dur];
                            GameManager* gm=[GameManager sharedGameManager];
                            
                            [NSTimer scheduledTimerWithTimeInterval:dur+2 target:gm selector:@selector(nextPuzzle) userInfo:nil repeats:NO];
                            
                            break;
                        }
                        
                    }
                    if ([children count]>0) {
                        [[SimpleAudioEngine sharedEngine] playEffect:@"mating.mp3"];
                        [self updateState];
                        
                    }
                
                }
            }
        }
        
    }
}

-(CGPoint) positionChildParent1:(Robot*)r1 andParent2:(Robot*)r2 andChild:(Robot*)ans {
    CGFloat possible_x= MIN(r1.position.x,r2.position.x)-ans.width;
    CGFloat right_offset= r1.position.x > r2.position.x? r1.position.x+r1.width: r2.position.x+r2.width;
    possible_x= possible_x<0? right_offset:possible_x;
    int x_sign=rand () % 2?-1:1;
    int y_sign=rand () % 2?-1:1;

    int dx=(int)x_sign*((float) (arc4random()%20));
    int dy=(int)y_sign*((float) (arc4random()%40));
    
return ccp(round(possible_x+dx),round(round((r1.position.y+r2.position.y)*.5)+dy));
}

- (CGPoint)  roundedPoint:(CGPoint)p {
    return ccp(p.x,p.y);
}
-(void) moveHeartFrom:(CGPoint) p1 to:(CGPoint)p2{
    CCSprite *heart = [CCSprite spriteWithFile:@"heart-full-red.png"];
    heart.scale=0.4;
    heart.position=[self roundedPoint:p1];
    [self addChild:heart];
    [self reorderChild:heart z:50];
    [heart setOpacity:175];
    
    CCJumpTo* jumpTo1= [CCJumpTo actionWithDuration:1.3 position:[self roundedPoint:p2] height:2 jumps:1];
    CCSequence* sequence = [CCSequence actions:jumpTo1, [CCCallFuncN actionWithTarget:self selector:@selector(spriteMoveFinished:)], nil];
    
    [heart runAction:sequence];
    
    
}
-(void)spriteMoveFinished:(id)sender {
    CCSprite *b = (CCSprite *)sender;
    [self removeChild:b cleanup:YES];
}

-(void) finalEffects:(Robot*) final andDuration:(ccTime)dur{
    self.isTouchEnabled=NO;
    [topMenu setIsTouchEnabled:false];
    [bottomMenu setIsTouchEnabled:false];
    [trashMenu setIsTouchEnabled:false];
    for (int i=0; i<[worldObjects count]; i++)
    {
        WorldObject* rest=(WorldObject*) [worldObjects objectAtIndex:i];
        if(rest==final || ![rest isMemberOfClass:[Robot class]])
            continue;
        //  [rest runAction:[CCFadeTo actionWithDuration:FADE_TO_FINAL_REST opacity:0.2]];
    }
    [final runAction:[self escape:dur]];
    
    
}
-(void)draw{
    [super draw];
    
    glColor4f(0.5, 0.5, 0.5, 1.0f);
    glLineWidth(2);

    ccDrawLine(ccp( 0, prison_y), ccp(prison_door_left+1,prison_y));

    glColor4f(0.3, 0.3, 0.3, 1.0f);
    glLineWidth(4);
    ccDrawLine(ccp(prison_door_left, 0), ccp(prison_door_left,prison_y+1));
    ccDrawLine(ccp( prison_door_right, 0), ccp(prison_door_right,prison_y));

    //Trash area
    //glLineWidth(2);
    
   // ccDrawLine(ccp(trash_area_x, 0), ccp(trash_area_x,TRASH_SQUARE_DIM));
    //ccDrawLine(ccp( trash_area_x, TRASH_SQUARE_DIM), ccp(swidth,TRASH_SQUARE_DIM));
    
}

-(void) kill:(UILongPressGestureRecognizer*) kill{
    
}
- (void)handlePinchGesture:(UIPinchGestureRecognizer *)sender {
    if ([sender numberOfTouches] < 2)
        return;
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        lastScale = 1.0;
        //     lastPoint = [sender locationInView:[self parent]];
    }
    
    // Scale
    //   CGFloat scale = 1.0 - (lastScale - sender.scale);
    self.scale=sender.scale;
    lastScale = sender.scale;
    
    // Translate
    //   CGPoint point = [sender locationInView:[self parent]];
    // self.position=point;    lastPoint = [sender locationInView:[self parent]];
}

-(void)zoom:(UIPinchGestureRecognizer*)pinch{
    if(pinch.state == UIGestureRecognizerStateEnded)
    {
        
        if(selected_index>=0){
            
            CCNode* temp=(CCNode*) [worldObjects objectAtIndex:selected_index];
            temp.scale=3.5*pinch.scale;
        }
        
    }
    else if(pinch.state == UIGestureRecognizerStateBegan )
    {
        
        if(selected_index>=0){
            
            CCNode* temp=(CCNode*) [worldObjects objectAtIndex:selected_index];
            pinch.scale=temp.scale;
        }
        
        
    }
    if(pinch.scale != NAN && pinch.scale != 0.0)
    {
        // pinch.view.transform = CGAffineTransformMakeScale(pinch.scale, pinch.scale);
        if(selected_index>=0){
            
            CCNode* temp=(CCNode*) [worldObjects objectAtIndex:selected_index];
            temp.scale=pinch.scale;
        }
        
    }
}
-(void)flick:(UISwipeGestureRecognizer*)flick{
    
    if(selected_index>=0){
        
        CCNode* temp=(CCNode*) [worldObjects objectAtIndex:selected_index];
        switch (flick.direction) {
            case UISwipeGestureRecognizerDirectionDown:
                temp.position=ccpAdd(temp.position, ccp(0,-2));
                break;
            case UISwipeGestureRecognizerDirectionUp:
                temp.position=ccpAdd(temp.position, ccp(0,2));
                break;
            case UISwipeGestureRecognizerDirectionLeft:
                temp.position=ccpAdd(temp.position, ccp(-2,0));
                break;
            case UISwipeGestureRecognizerDirectionRight:
                temp.position=ccpAdd(temp.position, ccp(2,0));
                break;
            default:
                break;
        }
        
    }
    
    
}
- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer {
    
    //   selected_index=[self findIndexTouchedWithLoc:[recognizer locationInView:[[CCDirector sharedDirector] openGLView]] in:worldObjects];
    if (longPress.enabled) {
        
        longPress.enabled=NO;
        if(selected_index>=0){
            
            CCNode* temp=(CCNode*) [worldObjects objectAtIndex:selected_index];
            if ([temp isMemberOfClass:[Robot class]]) {
                Robot* new=[Robot clone:(Robot*)temp];
                [self addWorldObject:new];
                new.position=temp.position;
                [self cloneEffects:new];
                [self updateState];
                [undoButton setIsEnabled:true];
            }
            
        }}
}
-(void) cloneEffects:(Robot*) clone{
    
    CCJumpTo* jumpTo1= [CCJumpTo actionWithDuration:0.4 position:ccp(clone.position.x,clone.position.y+CLONE_BIRTH_JUMP_DIST_Y) height:2 jumps:2];
    
    CCSequence* sequence = [CCSequence actions:jumpTo1, nil];
    
    [clone runAction:sequence];
    
    
}

-(NSInteger) findIndexTouched:(UITouch *)touch in: (CCArray*) objs{
    CGPoint location = [self convertTouchToNodeSpace: touch];
    int index=-1;
    int maxZ=-1;
    for (int i=0; i<[objs count]; i++) {
        WorldObject* temp=(WorldObject*) [objs objectAtIndex:i];
        if ([temp touchInside:location] & ([temp zOrder]>maxZ)) {
            maxZ=[temp zOrder];
            index=i;
        }
    }
    return index;
    
}
-(NSInteger) findIndexTouchedWithLoc:(CGPoint )location in: (CCArray*) objs{
    
    int index=-1;
    int maxZ=-1;
    for (int i=0; i<[objs count]; i++) {
        WorldObject* temp=(WorldObject*) [objs objectAtIndex:i];
        if ([temp touchInside:location] & ([temp zOrder]>maxZ)) {
            maxZ=[temp zOrder];
            index=i;
        }
    }
    return index;
    
}

- (void) move:(ccTime)dt {
    
    if ((arc4random()%5)==0) {
        int curr=arc4random()%[worldObjects count];
        WorldObject* temp=(WorldObject*) [worldObjects objectAtIndex:curr];
        if ([temp isMemberOfClass:[Robot class]]) {
            ccBezierConfig bezier;
            
            
            
            int x_sign=rand () % 2?-1:1;
            int y_sign=rand () % 2?-1:1;
            int dx=(int)x_sign*((float) (arc4random()%20));
            int dy=(int)y_sign*((float) (arc4random()%20));
            
            bezier.controlPoint_1 = temp.position;
            bezier.controlPoint_2 = temp.position;
            bezier.endPosition = ccpAdd(temp.position, ccp(dx,dy));
            //   [temp runAction:[CCMoveBy actionWithDuration:0.25 position:ccp(dx,dy)]];
            [temp runAction:[CCBezierTo actionWithDuration:0.25 bezier:bezier]];
            // temp.position=ccpAdd(temp.position, ccp(dx,dy));
        }
    }
}





-(void) mainMenu{
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    // [self updateState];
    for (int i = 0; i< [worldObjects count]; i++) {
        WorldObject* curr=(WorldObject*)[worldObjects objectAtIndex:i];
        [curr pauseSchedulerAndActions];
        [self removeChild:curr cleanup:false];
    }
    
    //  [[GameState sharedGameState] save];
    CCLOG(@"Show the Options screen");
    [[GameManager sharedGameManager] runSceneWithID:kMainMenuScene];
}

-(void) trash{
}

- (void) dealloc{
	[super dealloc];
    [worldObjects release];
}

@end
