//
//  GameManager.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

//  GameManager.h
//
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "SimpleAudioEngine.h"
#import "GAI.h"
#import <AWSSimpleDB/AWSSimpleDB.h>
#import <GameKit/GameKit.h>
@interface GameManager : NSObject {
    BOOL isMusicON;
    BOOL isSoundEffectsON;
    NSInteger total_puzzles;
    NSInteger current_puzzle;
    // This is the total score
    float score;
    float global_score;
    SceneTypes currentScene;
    id<GAITracker> tracker;
    AmazonSimpleDBClient *sdbClient;
    
    BOOL playerAuthenticated;
    NSString *userID;
    GKLocalPlayer* localPlayer;
    BOOL splash;
}
@property (readwrite) BOOL isMusicON;
@property (readwrite) BOOL isSoundEffectsON;
@property (readwrite) BOOL reloading;
@property (readwrite) BOOL playerAuthenticated;
@property (readwrite) BOOL splash;

@property (atomic, readwrite) NSInteger total_puzzles;
@property (atomic, readwrite) NSInteger current_puzzle;
@property (atomic, readwrite) float score;
@property (atomic, readwrite) float global_score;

@property (retain,readwrite)  id<GAITracker> tracker;

+(GameManager*)sharedGameManager;                                  // 1
-(void)runSceneWithID:(SceneTypes)sceneID;                         // 2
-(void)openSiteWithLinkType:(LinkTypes)linkTypeToOpen ;
-(void) reload;
-(void) nextPuzzle;
-(void) finishedPuzzle;
-(void) reset;
-(BOOL) fresh;
-(void)runSplashUpandMain;
-(void) updateScore;
-(void) updateScoreForNewPlayer:(GKLocalPlayer*) newPlayer;
// 3
@end
