//
//  Block.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/29/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "CCNode.h"
#import "cocos2d.h"
#import "WorldObject.h"
@interface Block : WorldObject{
    CCSprite* blockSprite;
    CCLabelTTF *idLabel;
    NSInteger id_n;
    BOOL b_sign;
}
@property (nonatomic,assign) CCSprite* blockSprite;
@property (nonatomic,assign) CCLabelTTF* idLabel;
@property (atomic,assign) NSInteger id_n;
@property (atomic,assign) BOOL b_sign;
+(Block *) blockWithSign:(BOOL)sign andID:(NSInteger)label;
-(void) initWithSign:(BOOL) sign andID:(NSInteger)label;
-(BOOL) cancels:(Block*) other;
-(BOOL) equals:(Block*) other;

+ (Block*) clone:(Block*) other;

@end
