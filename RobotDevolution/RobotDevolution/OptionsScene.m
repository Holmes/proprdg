//
//  OptionsScene.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 1/6/13.
//  Copyright 2013 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "OptionsScene.h"


@implementation OptionsScene
-(id)init {
    self = [super init];
    if (self != nil) {
        optionsLayer = [OptionsLayer node];
        [self addChild:optionsLayer];
    }
    return self;
}

@end
