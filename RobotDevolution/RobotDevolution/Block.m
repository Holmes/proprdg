//
//  Block.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/29/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "Block.h"
#import "GameConfig.h"
#define DELTA_SPACE 20
@implementation Block
@synthesize blockSprite, idLabel;
@synthesize id_n;
@synthesize b_sign;
+(Block *) blockWithSign:(BOOL)sign andID:(NSInteger)label
{
    Block* block = [Block node];
	[block  initWithSign:sign andID:label];
	return block;
}

// on "init" you need to initialize your instance
-(void) initWithSign:(BOOL) sign andID:(NSInteger)label
{
    b_sign=sign;
    ccColor3B color;
    id_n=label;
    //Setting up the block here.
    if( (self=[super init])) {
        if (sign) {
            blockSprite=[CCSprite spriteWithFile:@"blackblock.png"];
            color=ccBLACK;
        }
        else {
            blockSprite=[CCSprite spriteWithFile:@"blackblock.png"];
            color=ccWHITE;
        }
        
        idLabel=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", label] fontName:@"Gill Sans" fontSize:30];
        idLabel.color=color;
        
        [self addChild:blockSprite];
      //  [self addChild:idLabel];
        blockSprite.position=ccp(0,0);
       
        CGFloat width=[blockSprite textureRect].size.width;
        CGFloat height =[blockSprite textureRect].size.height;
        CGFloat Delta= width/(id_n+1);
        CGFloat available_width=width-70;
        CGFloat left=-width*.5;
        CGFloat shoulder_delta=1;
        for (int i= 0; i< id_n; i++)
        {
            CGFloat x = left+(i+1)*Delta;

            int dir=1;
            CCSprite* handsprite_1,*handsprite_2;
            if (sign){
                handsprite_1 = [CCSprite spriteWithFile:@"hand_pos_1.png"];
                handsprite_2 = [CCSprite spriteWithFile:@"hand_pos_2.png"];
                CGFloat hand_height=[handsprite_1 textureRect].size.height;
                [self addChild:handsprite_1];
                [self addChild:handsprite_2];
                handsprite_1.position=ccp(x,-1*(height/2+hand_height*0.4));
                handsprite_2.position=ccp(x,+1*(height/2+hand_height*0.4));

            }
            else{
                handsprite_1 = [CCSprite spriteWithFile:@"hand_neg_1.png"];
                handsprite_2 = [CCSprite spriteWithFile:@"hand_neg_2.png"];
                CGFloat hand_height=[handsprite_1 textureRect].size.height;
                [self addChild:handsprite_1];
                [self addChild:handsprite_2];
                handsprite_1.position=ccp(x,-1*(height/2+hand_height*0.4));
                handsprite_2.position=ccp(x,+1*(height/2+hand_height*0.4));
            }
        }
    }
}

- (BOOL) colorSame:(ccColor3B) c1 as:(ccColor3B) c2{
    
    return (c1.b==c2.b && c1.g == c2.g && c1.r == c2.r);
}
-(BOOL) touchInside:(CGPoint)location{
    
    CGPoint loc=[self convertToNodeSpace:location];
    
    CGRect box =[blockSprite boundingBox];
  //  box.origin.x=box.origin.x-box.size.width*.5;
    box.origin.y=box.origin.y-box.size.height*.5;

    box.size.height=box.size.height*2;
  //  box.size.width=box.size.width*2;

    if(CGRectContainsPoint(box, loc))
    {
        return true;
    }
    
    return false;
    
}
-(BOOL) cancels:(Block*) other{
    BOOL sign_inverted=(b_sign?1:-1)*id_n== (other.b_sign?-1:1)*other.id_n;
    if (!sign_inverted)
        return false;
    return true;
    
}

-(BOOL) equals:(Block*) other{
    BOOL same_id=(b_sign?1:-1)*id_n== (other.b_sign?1:-1)*other.id_n;
    if (same_id)
        return true;
    return false;
    
}
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeBool:b_sign forKey:@"b_sign"];
    [encoder encodeInteger:id_n forKey:@"id_n"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    
    NSInteger _id_n=[decoder decodeIntegerForKey:@"id_n"];
    NSInteger _b_sign=[decoder decodeBoolForKey:@"b_sign"];
    [self initWithSign:_b_sign andID:_id_n];
    return  self;
}


+ (Block*) clone:(Block*) other{
    
    Block* block = [Block node];
    //CCArray* args = [[CCArray alloc] init];
    [block  initWithSign:other.b_sign andID:ABS(other.id_n) ];
    
    return block;
    
}




@end
