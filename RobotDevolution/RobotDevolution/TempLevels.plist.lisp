
(((:PUZZLE (OR (NOT P1) (NOT P3)) (OR P4 P3) (OR P2 (NOT P4))
   (OR (NOT P3) (NOT P2)) (OR P2 P4) (OR P3 (NOT P4)) (OR P3 (NOT P1) (NOT P2))
   (OR (NOT P4) P1))
  (:MIN-PROOF-LEVEL . 4) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 87248 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:57 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 4.
% Maximum clause weight is 2.000.
% Given clauses 9.


2 -P1 | -P3.  [assumption].
3 P4 | P3.  [assumption].
5 -P3 | -P2.  [assumption].
6 P2 | P4.  [assumption].
7 P3 | -P4.  [assumption].
9 -P4 | P1.  [assumption].
10 -P2 | P4.  [resolve(5,a,3,b)].
11A P4 | P4.  [resolve(10,a,6,a)].
11 P4.  [copy(11A),merge(b)].
12 P1.  [resolve(11,a,9,a)].
13 P3.  [resolve(11,a,7,b)].
15A -P3.  [resolve(12,a,2,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 2 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 4.
% Maximum clause weight is 2.000.
% Given clauses 9.


3 P4 | P3.  [assumption].
4 P2 | -P4.  [assumption].
5 -P3 | -P2.  [assumption].
6 P2 | P4.  [assumption].
7 P3 | -P4.  [assumption].
10 -P2 | P4.  [resolve(5,a,3,b)].
11A P4 | P4.  [resolve(10,a,6,a)].
11 P4.  [copy(11A),merge(b)].
13 P3.  [resolve(11,a,7,b)].
14 P2.  [resolve(11,a,4,b)].
16A -P2.  [resolve(13,a,5,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 P4 P3) (OR (NOT P4) P3) (OR P1 (NOT P4))
   (OR (NOT P1) (NOT P2) P4 P3) (OR P2 P4) (OR (NOT P3) P4)
   (OR (NOT P1) (NOT P4) P3 P2) (OR (NOT P4) (NOT P1)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 85020 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:44 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 9.


2 P1 | P4 | P3.  [assumption].
4 P1 | -P4.  [assumption].
5 -P1 | -P2 | P4 | P3.  [assumption].
6 P2 | P4.  [assumption].
7 -P3 | P4.  [assumption].
8 -P4 | -P1.  [assumption].
9A P4 | -P1 | P4 | P3.  [resolve(6,a,5,b)].
9 P4 | -P1 | P3.  [copy(9A),merge(c)].
10A P4 | P3 | P4 | P3.  [resolve(9,b,2,a)].
10B P4 | P3 | P3.  [copy(10A),merge(c)].
10 P4 | P3.  [copy(10B),merge(c)].
11A P4 | P4.  [resolve(10,b,7,a)].
11 P4.  [copy(11A),merge(b)].
12 -P1.  [resolve(11,a,8,a)].
13A -P4.  [resolve(12,a,4,a)].
13 $F.  [resolve(11,a,13A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 P2) (OR P3 P4) (OR (NOT P3) (NOT P2)) (OR (NOT P3) P1)
   (OR P2 (NOT P1)) (OR (NOT P2) (NOT P4)) (OR (NOT P3) P1 (NOT P4) P2)
   (OR P3 P1))
  (:MIN-PROOF-LEVEL . 4) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 85004 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:43 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 4.
% Maximum clause weight is 2.000.
% Given clauses 7.


3 P3 | P4.  [assumption].
4 -P3 | -P2.  [assumption].
5 -P3 | P1.  [assumption].
6 P2 | -P1.  [assumption].
7 -P2 | -P4.  [assumption].
8 P3 | P1.  [assumption].
9 -P2 | P3.  [resolve(7,b,3,b)].
10A P1 | P1.  [resolve(8,a,5,a)].
10 P1.  [copy(10A),merge(b)].
11 P2.  [resolve(10,a,6,b)].
12 P3.  [resolve(11,a,9,a)].
14A -P2.  [resolve(12,a,4,a)].
14 $F.  [resolve(11,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 P1) (OR (NOT P4) (NOT P2)) (OR (NOT P1) P3 P4 (NOT P2))
   (OR (NOT P3) (NOT P1)) (OR (NOT P1) (NOT P3) (NOT P2))
   (OR P3 (NOT P4) (NOT P2) (NOT P1)) (OR P4 (NOT P3) (NOT P2) P1)
   (OR (NOT P4) P2) (OR P4 (NOT P1) P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5616 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:50 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 7.


2 P4 | P1.  [assumption].
3 -P4 | -P2.  [assumption].
5 -P3 | -P1.  [assumption].
6 -P4 | P2.  [assumption].
7 P4 | -P1 | P3.  [assumption].
8A P4 | P3 | P4.  [resolve(7,b,2,b)].
8 P4 | P3.  [copy(8A),merge(c)].
9 P4 | -P1.  [resolve(8,b,5,a)].
10A P4 | P4.  [resolve(9,b,2,b)].
10 P4.  [copy(10A),merge(b)].
11 P2.  [resolve(10,a,6,a)].
12A -P2.  [resolve(10,a,3,a)].
12 $F.  [resolve(11,a,12A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 P4) (OR (NOT P4) P1 P2) (OR P2 (NOT P4)) (OR (NOT P3) P1)
   (OR (NOT P3) (NOT P1)) (OR (NOT P4) P3) (OR P1 P2 (NOT P3))
   (OR P3 P4 P1 (NOT P2)) (OR P1 P4))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 140 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:15 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 5.
% Maximum clause weight is 2.000.
% Given clauses 7.


2 P3 | P4.  [assumption].
5 -P3 | P1.  [assumption].
6 -P3 | -P1.  [assumption].
7 -P4 | P3.  [assumption].
8 P1 | P4.  [assumption].
9 -P1 | P4.  [resolve(6,a,2,a)].
10A P4 | P4.  [resolve(9,a,8,a)].
10 P4.  [copy(10A),merge(b)].
11 P3.  [resolve(10,a,7,a)].
13 -P1.  [resolve(11,a,6,a)].
14A P1.  [resolve(11,a,5,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P3) (OR (NOT P2) P3 (NOT P4) (NOT P1))
   (OR P1 (NOT P3) P4 P2) (OR P2 (NOT P1)) (OR (NOT P3) (NOT P2))
   (OR (NOT P4) (NOT P1) (NOT P3)) (OR (NOT P2) P3) (OR (NOT P4) P2)
   (OR P2 P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 98804 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:08 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 10.


3 P1 | -P3 | P4 | P2.  [assumption].
4 P2 | -P1.  [assumption].
5 -P3 | -P2.  [assumption].
7 -P2 | P3.  [assumption].
8 -P4 | P2.  [assumption].
9 P2 | P3.  [assumption].
10A P2 | P1 | P4 | P2.  [resolve(9,b,3,b)].
10 P2 | P1 | P4.  [copy(10A),merge(d)].
11A P2 | P1 | P2.  [resolve(10,c,8,a)].
11 P2 | P1.  [copy(11A),merge(c)].
12A P2 | P2.  [resolve(11,b,4,b)].
12 P2.  [copy(12A),merge(b)].
13 P3.  [resolve(12,a,7,a)].
14A -P2.  [resolve(13,a,5,a)].
14 $F.  [resolve(12,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P2) (OR (NOT P2) (NOT P3)) (OR P4 P1) (OR P1 (NOT P3))
   (OR P2 P1) (OR (NOT P3) (NOT P2) P4 P1) (OR P1 P3 (NOT P2))
   (OR (NOT P2) P1 (NOT P4)) (OR (NOT P1) P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 96392 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:53 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 -P3 | P2.  [assumption].
3 -P2 | -P3.  [assumption].
5 P1 | -P3.  [assumption].
6 P2 | P1.  [assumption].
7 P1 | P3 | -P2.  [assumption].
9 -P1 | P3.  [assumption].
10A P1 | P3 | P1.  [resolve(7,c,6,a)].
10 P1 | P3.  [copy(10A),merge(c)].
12A P1 | P1.  [resolve(10,b,5,b)].
12 P1.  [copy(12A),merge(b)].
13 P3.  [resolve(12,a,9,a)].
14 -P2.  [resolve(13,a,3,b)].
15A P2.  [resolve(13,a,2,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P3) P4 P2) (OR (NOT P3) P4) (OR P3 P1)
   (OR (NOT P3) (NOT P4)) (OR P3 (NOT P1) (NOT P2) P4)
   (OR P1 (NOT P2) (NOT P4)) (OR P3 (NOT P2)) (OR P3 P4 P2 (NOT P1))
   (OR (NOT P4) P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 96112 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:52 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 9.


3 -P3 | P4.  [assumption].
4 P3 | P1.  [assumption].
5 -P3 | -P4.  [assumption].
8 P3 | -P2.  [assumption].
9 P3 | P4 | P2 | -P1.  [assumption].
10 -P4 | P3.  [assumption].
11A P3 | P4 | P2 | P3.  [resolve(9,d,4,b)].
11 P3 | P4 | P2.  [copy(11A),merge(d)].
12A P3 | P4 | P3.  [resolve(11,c,8,b)].
12 P3 | P4.  [copy(12A),merge(c)].
13A P3 | P3.  [resolve(12,b,10,a)].
13 P3.  [copy(13A),merge(b)].
14 -P4.  [resolve(13,a,5,a)].
15A P4.  [resolve(13,a,3,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P4) (OR (NOT P2) (NOT P3) P1) (OR P3 (NOT P4) P1)
   (OR (NOT P2) (NOT P3) P1) (OR P2 P3 (NOT P4)) (OR P4 P3)
   (OR (NOT P1) (NOT P4) P3) (OR (NOT P4) (NOT P3)) (OR (NOT P3) P4))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 95800 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:49 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 11.


4 P3 | -P4 | P1.  [assumption].
6 P4 | P3.  [assumption].
7 -P1 | -P4 | P3.  [assumption].
8 -P4 | -P3.  [assumption].
9 -P3 | P4.  [assumption].
11A P3 | P3 | P1.  [resolve(6,a,4,b)].
11 P3 | P1.  [copy(11A),merge(b)].
12A P3 | -P4 | P3.  [resolve(11,b,7,a)].
12 P3 | -P4.  [copy(12A),merge(c)].
13A P3 | P3.  [resolve(12,b,6,a)].
13 P3.  [copy(13A),merge(b)].
14 P4.  [resolve(13,a,9,a)].
15A -P3.  [resolve(14,a,8,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P2) (NOT P3) (NOT P4)) (OR P1 P3) (OR P4 P3)
   (OR (NOT P1) (NOT P4)) (OR (NOT P4) (NOT P3) (NOT P2) P1)
   (OR P4 P1 (NOT P3)) (OR (NOT P4) P1) (OR (NOT P3) P4)
   (OR (NOT P3) (NOT P2) P1))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 94948 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:44 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 5.
% Maximum clause weight is 2.000.
% Given clauses 7.


3 P1 | P3.  [assumption].
4 P4 | P3.  [assumption].
5 -P1 | -P4.  [assumption].
8 -P4 | P1.  [assumption].
9 -P3 | P4.  [assumption].
11 -P1 | P3.  [resolve(5,b,4,a)].
12A P3 | P3.  [resolve(11,a,3,a)].
12 P3.  [copy(12A),merge(b)].
14 P4.  [resolve(12,a,9,a)].
15 P1.  [resolve(14,a,8,a)].
16A -P4.  [resolve(15,a,5,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P4) (NOT P2) P1) (OR (NOT P1) P4 P2) (OR P3 P1)
   (OR (NOT P4) P3) (OR (NOT P2) (NOT P3)) (OR P2 (NOT P3))
   (OR (NOT P2) (NOT P1) (NOT P3) P4) (OR P4 (NOT P2))
   (OR (NOT P1) (NOT P4) P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 94136 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:39 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 8.


3 -P1 | P4 | P2.  [assumption].
4 P3 | P1.  [assumption].
5 -P4 | P3.  [assumption].
6 -P2 | -P3.  [assumption].
7 P2 | -P3.  [assumption].
8 P4 | -P2.  [assumption].
9 P3 | P4 | P2.  [resolve(4,b,3,a)].
10A P3 | P4 | P4.  [resolve(9,c,8,b)].
10 P3 | P4.  [copy(10A),merge(c)].
11A P3 | P3.  [resolve(10,b,5,a)].
11 P3.  [copy(11A),merge(b)].
12 P2.  [resolve(11,a,7,b)].
13A -P3.  [resolve(12,a,6,a)].
13 $F.  [resolve(11,a,13A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P4)) (OR (NOT P1) (NOT P2)) (OR P2 (NOT P1) (NOT P4))
   (OR P2 (NOT P4)) (OR P1 (NOT P3) (NOT P2) (NOT P4)) (OR P4 P2)
   (OR (NOT P2) P1) (OR (NOT P4) (NOT P2) (NOT P1) (NOT P3)) (OR P4 P1))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 6)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 92720 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:31 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 5.
% Maximum clause weight is 2.000.
% Given clauses 7.


3 -P1 | -P2.  [assumption].
5 P2 | -P4.  [assumption].
7 P4 | P2.  [assumption].
8 -P2 | P1.  [assumption].
9 P4 | P1.  [assumption].
10 P4 | -P2.  [resolve(9,b,3,a)].
11A P4 | P4.  [resolve(10,b,7,b)].
11 P4.  [copy(11A),merge(b)].
12 P2.  [resolve(11,a,5,b)].
14 P1.  [resolve(12,a,8,a)].
15A -P2.  [resolve(14,a,3,a)].
15 $F.  [resolve(12,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P2) (OR (NOT P2) P1 (NOT P4)) (OR (NOT P2) P3)
   (OR P3 P1) (OR (NOT P3) (NOT P2)) (OR (NOT P3) P2 P1) (OR P1 P4))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 77704 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:00:59 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 -P1 | P2.  [assumption].
4 -P2 | P3.  [assumption].
5 P3 | P1.  [assumption].
6 -P3 | -P2.  [assumption].
7 -P3 | P2 | P1.  [assumption].
9 -P2 | P1.  [resolve(6,a,5,a)].
10A P2 | P1 | P1.  [resolve(7,a,5,a)].
10 P2 | P1.  [copy(10A),merge(c)].
11A P1 | P1.  [resolve(10,a,9,a)].
11 P1.  [copy(11A),merge(b)].
12 P2.  [resolve(11,a,2,a)].
13 -P3.  [resolve(12,a,6,b)].
14A P3.  [resolve(12,a,4,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P3 P1) (OR (NOT P3) (NOT P4)) (OR (NOT P1) P4)
   (OR P1 P2 P3) (OR P3 (NOT P1) (NOT P4)) (OR P1 (NOT P3)) (OR P4 P1 P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 75598 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:00:46 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 8.


2 -P4 | P3 | P1.  [assumption].
3 -P3 | -P4.  [assumption].
4 -P1 | P4.  [assumption].
6 P3 | -P1 | -P4.  [assumption].
7 P1 | -P3.  [assumption].
8 P4 | P1 | P3.  [assumption].
9A P1 | P3 | P3 | P1.  [resolve(8,a,2,a)].
9B P1 | P3 | P1.  [copy(9A),merge(c)].
9 P1 | P3.  [copy(9B),merge(c)].
10A P1 | P1.  [resolve(9,b,7,b)].
10 P1.  [copy(10A),merge(b)].
11 P3 | -P4.  [resolve(10,a,6,b)].
12 P4.  [resolve(10,a,4,a)].
13 P3.  [resolve(12,a,11,b)].
14A -P4.  [resolve(13,a,3,a)].
14 $F.  [resolve(12,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P2 (NOT P3)) (OR (NOT P2) (NOT P1)) (OR P3 P2 (NOT P1))
   (OR P4 P1 (NOT P2) (NOT P3)) (OR P2 P3 P4) (OR P1 (NOT P4) P3) (OR P2 P4)
   (OR (NOT P2) P1))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 85136 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:44 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 -P4 | P2 | -P3.  [assumption].
3 -P2 | -P1.  [assumption].
4 P3 | P2 | -P1.  [assumption].
7 P1 | -P4 | P3.  [assumption].
8 P2 | P4.  [assumption].
9 -P2 | P1.  [assumption].
10 P2 | P1 | P3.  [resolve(8,b,7,b)].
11A P2 | P2 | -P3.  [resolve(8,b,2,a)].
11 P2 | -P3.  [copy(11A),merge(b)].
12A P2 | P3 | P3 | P2.  [resolve(10,b,4,c)].
12B P2 | P3 | P2.  [copy(12A),merge(c)].
12 P2 | P3.  [copy(12B),merge(c)].
13A P2 | P2.  [resolve(12,b,11,b)].
13 P2.  [copy(13A),merge(b)].
14 P1.  [resolve(13,a,9,a)].
15A -P1.  [resolve(13,a,3,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P1) (OR (NOT P3) P2 (NOT P4)) (OR P2 P3 P1 (NOT P4))
   (OR (NOT P4) (NOT P2)) (OR P3 (NOT P1) P2) (OR P3 P2 P4 (NOT P1))
   (OR (NOT P3) P4) (OR (NOT P2) P3) (OR P1 P2 P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 6464 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:55 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 8.


3 -P3 | P2 | -P4.  [assumption].
5 -P4 | -P2.  [assumption].
6 P3 | -P1 | P2.  [assumption].
7 -P3 | P4.  [assumption].
8 -P2 | P3.  [assumption].
9 P1 | P2 | P3.  [assumption].
10A P2 | P3 | P3 | P2.  [resolve(9,a,6,b)].
10B P2 | P3 | P2.  [copy(10A),merge(c)].
10 P2 | P3.  [copy(10B),merge(c)].
11A P3 | P3.  [resolve(10,a,8,a)].
11 P3.  [copy(11A),merge(b)].
12 P4.  [resolve(11,a,7,a)].
13A P2 | -P4.  [resolve(11,a,3,a)].
13 P2.  [resolve(12,a,13A,b)].
15A -P2.  [resolve(12,a,5,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P3 (NOT P4)) (OR (NOT P1) P2 (NOT P4)) (OR (NOT P1) P4)
   (OR P2 P3 P1 P4) (OR (NOT P2) P3) (OR (NOT P1) (NOT P4))
   (OR (NOT P2) (NOT P4) P1 (NOT P3)) (OR (NOT P3) P2 P4 (NOT P1))
   (OR P1 (NOT P3)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 4702 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:44 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 9.


2 P2 | P3 | -P4.  [assumption].
4 -P1 | P4.  [assumption].
5 P2 | P3 | P1 | P4.  [assumption].
6 -P2 | P3.  [assumption].
7 -P1 | -P4.  [assumption].
9 P1 | -P3.  [assumption].
10A P2 | P3 | P1 | P2 | P3.  [resolve(5,d,2,c)].
10B P2 | P3 | P1 | P3.  [copy(10A),merge(d)].
10 P2 | P3 | P1.  [copy(10B),merge(d)].
11A P2 | P1 | P1.  [resolve(10,b,9,b)].
11 P2 | P1.  [copy(11A),merge(c)].
12 P1 | P3.  [resolve(11,a,6,a)].
13A P1 | P1.  [resolve(12,b,9,b)].
13 P1.  [copy(13A),merge(b)].
14 -P4.  [resolve(13,a,7,a)].
15A P4.  [resolve(13,a,4,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P2) P3) (OR P2 (NOT P1)) (OR P2 P3 (NOT P4)) (OR P1 P4)
   (OR P4 P1) (OR (NOT P2) P3) (OR (NOT P1) (NOT P2)) (OR P2 (NOT P4) (NOT P1))
   (OR (NOT P3) P1))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 3710 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:38 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 9.


3 P2 | -P1.  [assumption].
4 P2 | P3 | -P4.  [assumption].
5 P1 | P4.  [assumption].
6 -P2 | P3.  [assumption].
7 -P1 | -P2.  [assumption].
8 -P3 | P1.  [assumption].
9 P1 | P2 | P3.  [resolve(5,b,4,c)].
10A P1 | P2 | P1.  [resolve(9,c,8,a)].
10 P1 | P2.  [copy(10A),merge(c)].
11 P1 | P3.  [resolve(10,b,6,a)].
12A P1 | P1.  [resolve(11,b,8,a)].
12 P1.  [copy(12A),merge(b)].
13 -P2.  [resolve(12,a,7,a)].
14A -P1.  [resolve(13,a,3,a)].
14 $F.  [resolve(12,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P1) (OR P4 (NOT P2) P3) (OR P1 P3 (NOT P4)) (OR P3 P2)
   (OR (NOT P3) (NOT P1)) (OR P1 P2) (OR P4 (NOT P3) P2)
   (OR P3 (NOT P2) (NOT P1)) (OR (NOT P2) (NOT P1) (NOT P3)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 2822 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:32 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 11.


2 -P3 | P1.  [assumption].
3 P4 | -P2 | P3.  [assumption].
4 P1 | P3 | -P4.  [assumption].
5 P3 | P2.  [assumption].
6 -P3 | -P1.  [assumption].
9 P3 | -P2 | -P1.  [assumption].
10A P3 | P4 | P3.  [resolve(5,b,3,b)].
10 P3 | P4.  [copy(10A),merge(c)].
12A P3 | P1 | P3.  [resolve(10,b,4,c)].
12 P3 | P1.  [copy(12A),merge(c)].
13A P3 | P3 | -P2.  [resolve(12,b,9,c)].
13 P3 | -P2.  [copy(13A),merge(b)].
14A P3 | P3.  [resolve(13,b,5,b)].
14 P3.  [copy(14A),merge(b)].
16 -P1.  [resolve(14,a,6,a)].
17A P1.  [resolve(14,a,2,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) (NOT P2)) (OR P3 P2) (OR (NOT P3) P4)
   (OR P3 (NOT P1) (NOT P4) (NOT P2)) (OR P1 P3 (NOT P4) P2)
   (OR (NOT P3) P2 (NOT P4)) (OR P3 P4) (OR P3 P4 (NOT P2) P1)
   (OR P4 (NOT P2) (NOT P1)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 2558 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:30 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 7.


2 -P4 | -P2.  [assumption].
3 P3 | P2.  [assumption].
4 -P3 | P4.  [assumption].
5 -P3 | P2 | -P4.  [assumption].
6 P3 | P4.  [assumption].
8 P3 | -P4.  [resolve(3,b,2,b)].
9A P3 | P3.  [resolve(8,b,6,b)].
9 P3.  [copy(9A),merge(b)].
10 P2 | -P4.  [resolve(9,a,5,a)].
11 P4.  [resolve(9,a,4,a)].
12 P2.  [resolve(11,a,10,b)].
13A -P2.  [resolve(11,a,2,a)].
13 $F.  [resolve(12,a,13A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P3) (OR (NOT P1) P2 P3) (OR (NOT P1) (NOT P3))
   (OR P4 (NOT P2) P3) (OR P1 (NOT P2)) (OR P2 P3 (NOT P1) P4) (OR P1 P2)
   (OR (NOT P2) (NOT P4) (NOT P1)) (OR (NOT P2) P4))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 1428 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:23 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 7.


2 P2 | P3.  [assumption].
3 -P1 | -P3.  [assumption].
5 P1 | -P2.  [assumption].
6 P1 | P2.  [assumption].
7 -P2 | -P4 | -P1.  [assumption].
8 -P2 | P4.  [assumption].
9 -P1 | P2.  [resolve(3,b,2,b)].
10A P2 | P2.  [resolve(9,a,6,a)].
10 P2.  [copy(10A),merge(b)].
11 P4.  [resolve(10,a,8,a)].
12A -P4 | -P1.  [resolve(10,a,7,a)].
12 -P1.  [resolve(11,a,12A,a)].
13A -P2.  [resolve(12,a,5,a)].
13 $F.  [resolve(10,a,13A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 P2 (NOT P4) P1) (OR (NOT P1) (NOT P4)) (OR P3 P4)
   (OR P1 (NOT P2)) (OR (NOT P3) (NOT P1)) (OR P4 (NOT P2) (NOT P1))
   (OR P2 (NOT P3) P4 P1) (OR (NOT P1) P2) (OR P1 P2 (NOT P4)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 938 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:20 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 10.


3 -P1 | -P4.  [assumption].
4 P3 | P4.  [assumption].
5 P1 | -P2.  [assumption].
6 -P3 | -P1.  [assumption].
8 P2 | -P3 | P4 | P1.  [assumption].
10 P1 | P2 | -P4.  [assumption].
11 -P1 | P4.  [resolve(6,a,4,a)].
12A P2 | P4 | P1 | P4.  [resolve(8,b,4,a)].
12 P2 | P4 | P1.  [copy(12A),merge(d)].
13A P2 | P1 | P1 | P2.  [resolve(12,b,10,c)].
13B P2 | P1 | P2.  [copy(13A),merge(c)].
13 P2 | P1.  [copy(13B),merge(c)].
14A P1 | P1.  [resolve(13,a,5,b)].
14 P1.  [copy(14A),merge(b)].
15 P4.  [resolve(14,a,11,a)].
18A -P4.  [resolve(14,a,3,a)].
18 $F.  [resolve(15,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P2) (NOT P3)) (OR (NOT P3) (NOT P1))
   (OR (NOT P1) P4 P3) (OR (NOT P1) P2) (OR P1 (NOT P2)) (OR P3 (NOT P2))
   (OR (NOT P4) P1) (OR P1 P4 P3) (OR (NOT P3) P4))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 906 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:20 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 10.


3 -P3 | -P1.  [assumption].
5 -P1 | P2.  [assumption].
7 P3 | -P2.  [assumption].
8 -P4 | P1.  [assumption].
9 P1 | P4 | P3.  [assumption].
10 -P3 | P4.  [assumption].
11A P1 | P3 | P1.  [resolve(9,b,8,a)].
11 P1 | P3.  [copy(11A),merge(c)].
12 P1 | P4.  [resolve(11,b,10,a)].
13A P1 | P1.  [resolve(12,b,8,a)].
13 P1.  [copy(13A),merge(b)].
14 P2.  [resolve(13,a,5,a)].
16 -P3.  [resolve(13,a,3,b)].
17A -P2.  [resolve(16,a,7,a)].
17 $F.  [resolve(14,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P2) (OR (NOT P1) (NOT P3)) (OR (NOT P3) (NOT P2))
   (OR (NOT P3) (NOT P4) (NOT P1)) (OR P3 P1) (OR (NOT P4) P1)
   (OR (NOT P1) (NOT P2)) (OR P4 (NOT P3) P1 P2) (OR (NOT P2) (NOT P1)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 559 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:18 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 -P1 | P2.  [assumption].
4 -P3 | -P2.  [assumption].
5 P3 | P1.  [assumption].
6 -P4 | P1.  [assumption].
7 -P1 | -P2.  [assumption].
8 P4 | -P3 | P1 | P2.  [assumption].
9 P1 | -P2.  [resolve(5,a,4,a)].
10A P4 | P1 | P2 | P1.  [resolve(8,b,5,a)].
10 P4 | P1 | P2.  [copy(10A),merge(d)].
11A P1 | P2 | P1.  [resolve(10,a,6,a)].
11 P1 | P2.  [copy(11A),merge(c)].
12A P1 | P1.  [resolve(11,b,9,b)].
12 P1.  [copy(12A),merge(b)].
13 -P2.  [resolve(12,a,7,a)].
15A P2.  [resolve(12,a,2,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P4) P2 (NOT P3)) (OR (NOT P2) (NOT P3)) (OR (NOT P3) P4)
   (OR P2 P4 P3) (OR P3 (NOT P2)) (OR P3 (NOT P4))
   (OR (NOT P3) (NOT P1) (NOT P2)) (OR P4 P2) (OR P2 (NOT P1)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 99356 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:11 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 P1 | -P4 | P2 | -P3.  [assumption].
3 -P2 | -P3.  [assumption].
6 P3 | -P2.  [assumption].
7 P3 | -P4.  [assumption].
8 P4 | P2.  [assumption].
9 P2 | -P1.  [assumption].
10 P2 | P3.  [resolve(8,a,7,b)].
11A P2 | P1 | P2 | -P3.  [resolve(8,a,2,b)].
11 P2 | P1 | -P3.  [copy(11A),merge(c)].
12A P2 | P1 | P2.  [resolve(11,c,10,b)].
12 P2 | P1.  [copy(12A),merge(c)].
13A P2 | P2.  [resolve(12,b,9,b)].
13 P2.  [copy(13A),merge(b)].
14 P3.  [resolve(13,a,6,b)].
15A -P3.  [resolve(13,a,3,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P1) (OR P1 P3 P4) (OR P1 P3) (OR P1 (NOT P4) (NOT P2))
   (OR (NOT P3) (NOT P4)) (OR (NOT P4) (NOT P1)) (OR P2 P4 P3) (OR (NOT P3) P4)
   (OR (NOT P1) P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 98516 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:06 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 2.000.
% Given clauses 9.


4 P1 | P3.  [assumption].
5 -P3 | -P4.  [assumption].
6 -P4 | -P1.  [assumption].
8 -P3 | P4.  [assumption].
9 -P1 | P3.  [assumption].
10 P4 | P1.  [resolve(8,a,4,b)].
11 P1 | -P3.  [resolve(10,a,5,b)].
12A P1 | P1.  [resolve(11,b,4,b)].
12 P1.  [copy(12A),merge(b)].
13 P3.  [resolve(12,a,9,a)].
14 -P4.  [resolve(12,a,6,b)].
15A P4.  [resolve(13,a,8,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P4 (NOT P3)) (OR P1 (NOT P2)) (OR (NOT P1) P4)
   (OR (NOT P3) (NOT P1)) (OR P4 P2 P3) (OR P1 (NOT P3) P2) (OR P4 P3 P2)
   (OR P3 (NOT P1)) (OR P2 P3))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 97792 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:02 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 10.


3 P1 | -P2.  [assumption].
5 -P3 | -P1.  [assumption].
7 P1 | -P3 | P2.  [assumption].
8 P3 | -P1.  [assumption].
9 P2 | P3.  [assumption].
10A P2 | P1 | P2.  [resolve(9,b,7,b)].
10 P2 | P1.  [copy(10A),merge(c)].
12 P2 | -P3.  [resolve(10,b,5,b)].
13A P2 | P2.  [resolve(12,b,9,b)].
13 P2.  [copy(13A),merge(b)].
14 P1.  [resolve(13,a,3,b)].
15 P3.  [resolve(14,a,8,b)].
16A -P1.  [resolve(15,a,5,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 (NOT P1)) (OR (NOT P1) P2 (NOT P3) (NOT P4)) (OR P1 P4)
   (OR P3 (NOT P1) (NOT P4)) (OR (NOT P2) (NOT P4) P3 P1)
   (OR (NOT P4) P2 P1 (NOT P3)) (OR P3 P2) (OR (NOT P4) P1 (NOT P3))
   (OR (NOT P4) (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 97528 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:00 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 9.


2 P4 | -P1.  [assumption].
4 P1 | P4.  [assumption].
6 -P2 | -P4 | P3 | P1.  [assumption].
8 P3 | P2.  [assumption].
9 -P4 | P1 | -P3.  [assumption].
10 -P4 | -P1.  [assumption].
11A P3 | -P4 | P3 | P1.  [resolve(8,b,6,a)].
11 P3 | -P4 | P1.  [copy(11A),merge(c)].
12A P3 | P1 | P1.  [resolve(11,b,4,b)].
12 P3 | P1.  [copy(12A),merge(c)].
13A P1 | -P4 | P1.  [resolve(12,a,9,c)].
13 P1 | -P4.  [copy(13A),merge(c)].
14A P1 | P1.  [resolve(13,b,4,b)].
14 P1.  [copy(14A),merge(b)].
15 -P4.  [resolve(14,a,10,b)].
16A -P1.  [resolve(15,a,2,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 (NOT P4) P1) (OR P1 (NOT P3)) (OR (NOT P3) P4) (OR P1 P4)
   (OR (NOT P4) (NOT P3) (NOT P1)) (OR P4 P2) (OR P1 (NOT P2)) (OR P3 (NOT P1))
   (OR P4 P3 P1 (NOT P2)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 96292 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:53 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 11.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 P2 | -P4 | P1.  [assumption].
4 -P3 | P4.  [assumption].
5 P1 | P4.  [assumption].
6 -P4 | -P3 | -P1.  [assumption].
8 P1 | -P2.  [assumption].
9 P3 | -P1.  [assumption].
10A P1 | P2 | P1.  [resolve(5,b,2,b)].
10 P1 | P2.  [copy(10A),merge(c)].
11A P1 | P1.  [resolve(10,b,8,b)].
11 P1.  [copy(11A),merge(b)].
12 P3.  [resolve(11,a,9,b)].
13A -P4 | -P1.  [resolve(12,a,6,b)].
13 -P4.  [resolve(11,a,13A,b)].
14A P4.  [resolve(12,a,4,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P3 P4) (OR (NOT P1) P4) (OR (NOT P4) (NOT P1))
   (OR (NOT P1) (NOT P2) (NOT P3) (NOT P4)) (OR P2 (NOT P3) P4)
   (OR P2 P4 (NOT P1)) (OR P1 P2 (NOT P3) P4) (OR P2 P1) (OR (NOT P2) P1))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 7)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 93920 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:38 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 10.
% Level of proof is 6.
% Maximum clause weight is 2.000.
% Given clauses 8.


3 -P1 | P4.  [assumption].
4 -P4 | -P1.  [assumption].
6 P2 | P1.  [assumption].
7 -P2 | P1.  [assumption].
8 P2 | P4.  [resolve(6,b,3,a)].
9 P2 | -P1.  [resolve(8,b,4,a)].
10A P2 | P2.  [resolve(9,b,6,b)].
10 P2.  [copy(10A),merge(b)].
11 P1.  [resolve(10,a,7,a)].
13 -P4.  [resolve(11,a,4,b)].
14A P4.  [resolve(11,a,3,a)].
14 $F.  [resolve(13,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P1)) (OR (NOT P1) P4 (NOT P3))
   (OR P1 P3 (NOT P2) (NOT P4)) (OR P3 P2) (OR P1 (NOT P3)) (OR P1 P3 P4)
   (OR (NOT P3) (NOT P4)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 79676 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:11 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 9.


2 P3 | -P1.  [assumption].
3 -P1 | P4 | -P3.  [assumption].
4 P1 | P3 | -P2 | -P4.  [assumption].
5 P3 | P2.  [assumption].
6 P1 | -P3.  [assumption].
7 P1 | P3 | P4.  [assumption].
8 -P3 | -P4.  [assumption].
9A P3 | P1 | P3 | -P4.  [resolve(5,b,4,c)].
9 P3 | P1 | -P4.  [copy(9A),merge(c)].
10A P3 | P1 | P1 | P3.  [resolve(9,c,7,c)].
10B P3 | P1 | P3.  [copy(10A),merge(c)].
10 P3 | P1.  [copy(10B),merge(c)].
11A P3 | P3.  [resolve(10,b,2,b)].
11 P3.  [copy(11A),merge(b)].
12 -P4.  [resolve(11,a,8,a)].
13 P1.  [resolve(11,a,6,b)].
14A P4 | -P3.  [resolve(13,a,3,a)].
14B -P3.  [resolve(12,a,14A,a)].
14 $F.  [resolve(11,a,14B,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P2) (OR (NOT P1) (NOT P2)) (OR P3 P4 P1) (OR P3 P4)
   (OR (NOT P4) P2) (OR (NOT P4) P1 P3 (NOT P2)) (OR (NOT P2) (NOT P3)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 77864 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:00 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 -P3 | P2.  [assumption].
3 -P1 | -P2.  [assumption].
5 P3 | P4.  [assumption].
6 -P4 | P2.  [assumption].
7 -P4 | P1 | P3 | -P2.  [assumption].
8 -P2 | -P3.  [assumption].
9 P2 | P3.  [resolve(6,a,5,b)].
10A P3 | -P4 | P1 | P3.  [resolve(9,a,7,d)].
10 P3 | -P4 | P1.  [copy(10A),merge(d)].
11A P3 | P1 | P3.  [resolve(10,b,5,b)].
11 P3 | P1.  [copy(11A),merge(c)].
12 P3 | -P2.  [resolve(11,b,3,a)].
13A P3 | P3.  [resolve(12,b,9,a)].
13 P3.  [copy(13A),merge(b)].
14 -P2.  [resolve(13,a,8,b)].
15A P2.  [resolve(13,a,2,a)].
15 $F.  [resolve(14,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P2)) (OR P1 P3) (OR P3 (NOT P4) (NOT P1))
   (OR P1 (NOT P4)) (OR P2 (NOT P4)) (OR (NOT P3) P4) (OR (NOT P1) P4 P2 P3)
   (OR (NOT P1) (NOT P3)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 89480 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:11 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 P3 | -P2.  [assumption].
3 P1 | P3.  [assumption].
4 P3 | -P4 | -P1.  [assumption].
5 P1 | -P4.  [assumption].
7 -P3 | P4.  [assumption].
8 -P1 | P4 | P2 | P3.  [assumption].
9 -P1 | -P3.  [assumption].
10A P3 | -P4 | P3.  [resolve(4,c,3,a)].
10 P3 | -P4.  [copy(10A),merge(c)].
11A P4 | P2 | P3 | P3.  [resolve(8,a,3,a)].
11 P4 | P2 | P3.  [copy(11A),merge(d)].
12A P4 | P3 | P3.  [resolve(11,b,2,b)].
12 P4 | P3.  [copy(12A),merge(c)].
13A P3 | P3.  [resolve(12,a,10,b)].
13 P3.  [copy(13A),merge(b)].
14 -P1.  [resolve(13,a,9,b)].
15 P4.  [resolve(13,a,7,a)].
16A -P4.  [resolve(14,a,5,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) (NOT P2) (NOT P3)) (OR (NOT P3) P4 P1) (OR (NOT P1) P3)
   (OR (NOT P4) P2) (OR (NOT P1) (NOT P3)) (OR P1 P3) (OR (NOT P1) P2)
   (OR (NOT P2) P3 (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 88480 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:05 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 11.


2 -P4 | -P2 | -P3.  [assumption].
3 -P3 | P4 | P1.  [assumption].
4 -P1 | P3.  [assumption].
5 -P4 | P2.  [assumption].
6 -P1 | -P3.  [assumption].
7 P1 | P3.  [assumption].
9A P1 | P4 | P1.  [resolve(7,b,3,a)].
9 P1 | P4.  [copy(9A),merge(c)].
10 P1 | P2.  [resolve(9,b,5,a)].
11 P1 | -P2 | -P3.  [resolve(9,b,2,a)].
12A P1 | -P3 | P1.  [resolve(11,b,10,b)].
12 P1 | -P3.  [copy(12A),merge(c)].
13A P1 | P1.  [resolve(12,b,7,b)].
13 P1.  [copy(13A),merge(b)].
15 -P3.  [resolve(13,a,6,a)].
16A P3.  [resolve(13,a,4,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P4) (OR (NOT P2) (NOT P4) P3) (OR P2 P4)
   (OR (NOT P3) P1) (OR P2 (NOT P1) P3) (OR (NOT P4) P1) (OR (NOT P4) (NOT P1))
   (OR P3 (NOT P2)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 85360 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:46 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 2.000.
% Given clauses 9.


2 -P3 | P4.  [assumption].
4 P2 | P4.  [assumption].
5 -P3 | P1.  [assumption].
7 -P4 | P1.  [assumption].
8 -P4 | -P1.  [assumption].
9 P3 | -P2.  [assumption].
10 P1 | P2.  [resolve(7,a,4,b)].
11 P2 | -P4.  [resolve(10,a,8,b)].
13A P2 | P2.  [resolve(11,b,4,b)].
13 P2.  [copy(13A),merge(b)].
14 P3.  [resolve(13,a,9,b)].
15 P1.  [resolve(14,a,5,a)].
16 P4.  [resolve(14,a,2,a)].
17A -P1.  [resolve(16,a,8,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P3 P2) (OR P1 (NOT P4))
   (OR (NOT P1) (NOT P2) (NOT P3) P4) (OR P4 P2) (OR P4 (NOT P2))
   (OR P3 (NOT P2)) (OR P2 (NOT P1) (NOT P4) (NOT P3)) (OR (NOT P3) (NOT P2)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 84648 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:41 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 -P1 | P3 | P2.  [assumption].
3 P1 | -P4.  [assumption].
5 P4 | P2.  [assumption].
7 P3 | -P2.  [assumption].
8 P2 | -P1 | -P4 | -P3.  [assumption].
9 -P3 | -P2.  [assumption].
10 P2 | P1.  [resolve(5,a,3,b)].
11A P2 | P3 | P2.  [resolve(10,b,2,a)].
11 P2 | P3.  [copy(11A),merge(c)].
12A P2 | P2 | -P1 | -P4.  [resolve(11,b,8,d)].
12 P2 | -P1 | -P4.  [copy(12A),merge(b)].
13A P2 | -P4 | P2.  [resolve(12,b,10,b)].
13 P2 | -P4.  [copy(13A),merge(c)].
14A P2 | P2.  [resolve(13,b,5,a)].
14 P2.  [copy(14A),merge(b)].
15 -P3.  [resolve(14,a,9,b)].
16A -P2.  [resolve(15,a,7,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P1)) (OR P3 P1 P4) (OR (NOT P2) P3 P1) (OR P2 (NOT P1))
   (OR P1 (NOT P4)) (OR (NOT P1) P2 P4) (OR (NOT P3) (NOT P2)) (OR P4 P2))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 83036 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:31 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 10.


2 P3 | -P1.  [assumption].
3 P3 | P1 | P4.  [assumption].
5 P2 | -P1.  [assumption].
6 P1 | -P4.  [assumption].
7 -P3 | -P2.  [assumption].
8 P4 | P2.  [assumption].
9 -P2 | P1 | P4.  [resolve(7,a,3,a)].
10 P2 | P1.  [resolve(8,a,6,b)].
11A P1 | P1 | P4.  [resolve(10,a,9,a)].
11 P1 | P4.  [copy(11A),merge(b)].
13A P1 | P1.  [resolve(11,b,6,b)].
13 P1.  [copy(13A),merge(b)].
14 P2.  [resolve(13,a,5,b)].
15 P3.  [resolve(13,a,2,b)].
16A -P2.  [resolve(15,a,7,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P1 (NOT P3)) (OR P1 P3 P4) (OR (NOT P3) P4)
   (OR (NOT P4) P3 P1 P2) (OR (NOT P2) (NOT P3) (NOT P4)) (OR P2 (NOT P3))
   (OR P2 P3) (OR P3 (NOT P1)) (OR P1 (NOT P4) P3 (NOT P2)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 6100 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:53 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 10.


3 P1 | P3 | P4.  [assumption].
4 -P3 | P4.  [assumption].
6 -P2 | -P3 | -P4.  [assumption].
7 P2 | -P3.  [assumption].
8 P2 | P3.  [assumption].
9 P3 | -P1.  [assumption].
10 P1 | -P4 | P3 | -P2.  [assumption].
11A P1 | P3 | -P2 | P1 | P3.  [resolve(10,b,3,c)].
11B P1 | P3 | -P2 | P3.  [copy(11A),merge(d)].
11 P1 | P3 | -P2.  [copy(11B),merge(d)].
12A P1 | P3 | P3.  [resolve(11,c,8,a)].
12 P1 | P3.  [copy(12A),merge(c)].
13A P3 | P3.  [resolve(12,a,9,b)].
13 P3.  [copy(13A),merge(b)].
14 P2.  [resolve(13,a,7,b)].
15A -P3 | -P4.  [resolve(14,a,6,a)].
15 -P4.  [resolve(13,a,15A,a)].
16A P4.  [resolve(13,a,4,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 P3 (NOT P4) P2) (OR (NOT P1) (NOT P2) (NOT P3))
   (OR P4 (NOT P1)) (OR (NOT P3) P4 P1) (OR (NOT P2) P4 (NOT P3) (NOT P1))
   (OR (NOT P2) (NOT P4)) (OR (NOT P4) P2 P1) (OR P3 P4) (OR P2 (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5652 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:50 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 10.


4 P4 | -P1.  [assumption].
5 -P3 | P4 | P1.  [assumption].
6 -P2 | -P4.  [assumption].
7 -P4 | P2 | P1.  [assumption].
8 P3 | P4.  [assumption].
9 P2 | -P1.  [assumption].
10A P4 | P4 | P1.  [resolve(8,a,5,a)].
10 P4 | P1.  [copy(10A),merge(b)].
11A P1 | P2 | P1.  [resolve(10,a,7,a)].
11 P1 | P2.  [copy(11A),merge(c)].
12 P1 | -P4.  [resolve(11,b,6,a)].
13A P1 | P1.  [resolve(12,b,10,a)].
13 P1.  [copy(13A),merge(b)].
14 P2.  [resolve(13,a,9,b)].
15 P4.  [resolve(13,a,4,b)].
17A -P4.  [resolve(14,a,6,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P3) (NOT P4)) (OR (NOT P2) P3) (OR P4 (NOT P2))
   (OR P3 (NOT P4) (NOT P1) P2) (OR (NOT P1) (NOT P3)) (OR P2 P3) (OR P2 P1)
   (OR (NOT P4) (NOT P1)) (OR P4 (NOT P3)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5272 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:47 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 P1 | -P3 | -P4.  [assumption].
3 -P2 | P3.  [assumption].
4 P4 | -P2.  [assumption].
6 -P1 | -P3.  [assumption].
7 P2 | P3.  [assumption].
8 P2 | P1.  [assumption].
9 -P4 | -P1.  [assumption].
11 P2 | -P3.  [resolve(8,b,6,a)].
14A P2 | P2.  [resolve(11,b,7,b)].
14 P2.  [copy(14A),merge(b)].
15 P4.  [resolve(14,a,4,b)].
16 P3.  [resolve(14,a,3,a)].
17 -P1.  [resolve(15,a,9,a)].
18A -P3 | -P4.  [resolve(17,a,2,a)].
18B -P4.  [resolve(16,a,18A,a)].
18 $F.  [resolve(15,a,18B,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) (NOT P2)) (OR (NOT P2) P4) (OR P4 P3 P2)
   (OR (NOT P3) P4 P1) (OR P1 (NOT P3) P2 (NOT P4)) (OR P1 P2)
   (OR (NOT P3) (NOT P1)) (OR P3 (NOT P4)) (OR P4 (NOT P2) (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 4243 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:41 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 -P4 | -P2.  [assumption].
3 -P2 | P4.  [assumption].
4 P4 | P3 | P2.  [assumption].
7 P1 | P2.  [assumption].
8 -P3 | -P1.  [assumption].
9 P3 | -P4.  [assumption].
10 -P1 | P4 | P2.  [resolve(8,a,4,b)].
11A P4 | P2 | P2.  [resolve(10,a,7,a)].
11 P4 | P2.  [copy(11A),merge(c)].
12A P4 | P4.  [resolve(11,b,3,a)].
12 P4.  [copy(12A),merge(b)].
13 P3.  [resolve(12,a,9,b)].
14 -P2.  [resolve(12,a,2,a)].
15 -P1.  [resolve(13,a,8,a)].
16A P2.  [resolve(15,a,7,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P2)) (OR (NOT P3) P4) (OR P4 P3) (OR P1 (NOT P4))
   (OR (NOT P3) P2) (OR P3 P4 (NOT P2)) (OR P4 (NOT P3) P2 (NOT P1)) (OR P2 P3)
   (OR (NOT P1) (NOT P3) (NOT P4) P2))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 95312 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:47 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 2.000.
% Given clauses 8.


2 -P1 | -P2.  [assumption].
3 -P3 | P4.  [assumption].
4 P4 | P3.  [assumption].
5 P1 | -P4.  [assumption].
6 -P3 | P2.  [assumption].
7 P2 | P3.  [assumption].
8 P1 | P3.  [resolve(5,b,4,a)].
9 P3 | -P2.  [resolve(8,a,2,a)].
10A P3 | P3.  [resolve(9,b,7,a)].
10 P3.  [copy(10A),merge(b)].
11 P2.  [resolve(10,a,6,a)].
12 P4.  [resolve(10,a,3,a)].
13 -P1.  [resolve(11,a,2,b)].
14A -P4.  [resolve(13,a,5,a)].
14 $F.  [resolve(12,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 P3) (OR P3 (NOT P4)) (OR (NOT P1) (NOT P4) P2 P3)
   (OR (NOT P3) P2 (NOT P4)) (OR P1 P4 P2) (OR (NOT P4) (NOT P2))
   (OR (NOT P4) (NOT P1) P3 (NOT P2)) (OR (NOT P3) P1 (NOT P2))
   (OR P4 (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 93388 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:35 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 P4 | P3.  [assumption].
3 P3 | -P4.  [assumption].
4 -P3 | P2 | -P4.  [assumption].
5 P1 | P4 | P2.  [assumption].
6 -P4 | -P2.  [assumption].
7 -P3 | P1 | -P2.  [assumption].
8 P4 | -P1.  [assumption].
9A -P3 | P1 | P1 | P4.  [resolve(7,c,5,c)].
9 -P3 | P1 | P4.  [copy(9A),merge(c)].
10A P1 | P4 | P4.  [resolve(9,a,2,b)].
10 P1 | P4.  [copy(10A),merge(c)].
11A P4 | P4.  [resolve(10,a,8,b)].
11 P4.  [copy(11A),merge(b)].
12 -P2.  [resolve(11,a,6,a)].
13A -P3 | -P4.  [resolve(12,a,4,b)].
13 -P3.  [resolve(11,a,13A,b)].
14A -P4.  [resolve(13,a,3,a)].
14 $F.  [resolve(11,a,14A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 P2) (OR (NOT P3) P4) (OR (NOT P2) P3)
   (OR (NOT P4) (NOT P2) P1) (OR (NOT P2) P1 P4) (OR (NOT P1) P2 (NOT P3))
   (OR P3 P4 (NOT P1)) (OR (NOT P4) (NOT P1)) (OR (NOT P2) P4 (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 8)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 92796 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:31 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 3.000.
% Given clauses 12.


2 P1 | P2.  [assumption].
5 -P4 | -P2 | P1.  [assumption].
6 -P2 | P1 | P4.  [assumption].
7 -P1 | P2 | -P3.  [assumption].
8 P3 | P4 | -P1.  [assumption].
9 -P4 | -P1.  [assumption].
10 -P2 | P4 | -P1.  [assumption].
12A P1 | P4 | P1.  [resolve(6,a,2,b)].
12 P1 | P4.  [copy(12A),merge(c)].
13A P1 | -P2 | P1.  [resolve(12,b,5,a)].
13 P1 | -P2.  [copy(13A),merge(c)].
14A P1 | P1.  [resolve(13,b,2,b)].
14 P1.  [copy(14A),merge(b)].
15 -P2 | P4.  [resolve(14,a,10,c)].
16 -P4.  [resolve(14,a,9,b)].
17A P3 | -P1.  [resolve(16,a,8,b)].
17 P3.  [resolve(14,a,17A,b)].
18A P2 | -P3.  [resolve(14,a,7,a)].
18 P2.  [resolve(17,a,18A,b)].
19A P4.  [resolve(18,a,15,a)].
19 $F.  [resolve(16,a,19A,a)].

============================== end of proof ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 2 at 0.00 (+ 0.00) seconds.
% Length of proof is 12.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 12.


2 P1 | P2.  [assumption].
3 -P3 | P4.  [assumption].
5 -P4 | -P2 | P1.  [assumption].
6 -P2 | P1 | P4.  [assumption].
8 P3 | P4 | -P1.  [assumption].
9 -P4 | -P1.  [assumption].
12A P1 | P4 | P1.  [resolve(6,a,2,b)].
12 P1 | P4.  [copy(12A),merge(c)].
13A P1 | -P2 | P1.  [resolve(12,b,5,a)].
13 P1 | -P2.  [copy(13A),merge(c)].
14A P1 | P1.  [resolve(13,b,2,b)].
14 P1.  [copy(14A),merge(b)].
16 -P4.  [resolve(14,a,9,b)].
17A P3 | -P1.  [resolve(16,a,8,b)].
17 P3.  [resolve(14,a,17A,b)].
20A P4.  [resolve(17,a,3,a)].
20 $F.  [resolve(16,a,20A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) (NOT P1) P3) (OR P1 P2) (OR P4 P3 P1) (OR P2 (NOT P1))
   (OR (NOT P4) (NOT P1) (NOT P3)) (OR P1 (NOT P4)) (OR (NOT P2) P4))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 70685 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:00:16 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 8.


2 -P2 | -P1 | P3.  [assumption].
3 P1 | P2.  [assumption].
5 P2 | -P1.  [assumption].
6 -P4 | -P1 | -P3.  [assumption].
7 P1 | -P4.  [assumption].
8 -P2 | P4.  [assumption].
9 P4 | P1.  [resolve(8,a,3,b)].
10A P1 | P1.  [resolve(9,a,7,b)].
10 P1.  [copy(10A),merge(b)].
11 -P4 | -P3.  [resolve(10,a,6,b)].
12 P2.  [resolve(10,a,5,b)].
13A -P1 | P3.  [resolve(12,a,2,a)].
13 P3.  [resolve(10,a,13A,a)].
14 P4.  [resolve(12,a,8,a)].
15A -P3.  [resolve(14,a,11,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P4)) (OR (NOT P3) P1 P4 (NOT P2))
   (OR (NOT P1) (NOT P3)) (OR P2 P4) (OR P1 (NOT P4) (NOT P3) P2) (OR P3 P1)
   (OR P1 (NOT P4) (NOT P3)) (OR P3 (NOT P1) P4))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 90140 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:15 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 -P1 | -P4.  [assumption].
3 -P3 | P1 | P4 | -P2.  [assumption].
4 -P1 | -P3.  [assumption].
5 P2 | P4.  [assumption].
7 P3 | P1.  [assumption].
8 P1 | -P4 | -P3.  [assumption].
9 P3 | -P1 | P4.  [assumption].
10A P4 | -P3 | P1 | P4.  [resolve(5,a,3,d)].
10 P4 | -P3 | P1.  [copy(10A),merge(d)].
11A P4 | P1 | P1.  [resolve(10,b,7,a)].
11 P4 | P1.  [copy(11A),merge(c)].
12A P1 | P1 | -P3.  [resolve(11,a,8,b)].
12 P1 | -P3.  [copy(12A),merge(b)].
13A P1 | P1.  [resolve(12,b,7,a)].
13 P1.  [copy(13A),merge(b)].
14 P3 | P4.  [resolve(13,a,9,b)].
15 -P3.  [resolve(13,a,4,a)].
16 -P4.  [resolve(13,a,2,a)].
17A P4.  [resolve(15,a,14,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) (NOT P4) P1) (OR P3 P4 P1) (OR (NOT P3) (NOT P2) P4)
   (OR P4 (NOT P1)) (OR (NOT P3) P2 (NOT P4)) (OR P3 (NOT P4)) (OR P2 P1)
   (OR (NOT P4) (NOT P1) (NOT P3)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 86616 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:53 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 11.


2 -P2 | -P4 | P1.  [assumption].
3 P3 | P4 | P1.  [assumption].
4 -P3 | -P2 | P4.  [assumption].
5 P4 | -P1.  [assumption].
7 P3 | -P4.  [assumption].
8 P2 | P1.  [assumption].
9 -P4 | -P1 | -P3.  [assumption].
10A -P2 | P4 | P4 | P1.  [resolve(4,a,3,a)].
10 -P2 | P4 | P1.  [copy(10A),merge(c)].
11A P1 | -P4 | P1.  [resolve(8,a,2,a)].
11 P1 | -P4.  [copy(11A),merge(c)].
12A P4 | P1 | P1.  [resolve(10,a,8,a)].
12 P4 | P1.  [copy(12A),merge(c)].
13A P4 | P4.  [resolve(12,b,5,b)].
13 P4.  [copy(13A),merge(b)].
14 P1.  [resolve(13,a,11,b)].
15A -P1 | -P3.  [resolve(13,a,9,a)].
15 -P3.  [resolve(14,a,15A,a)].
16A -P4.  [resolve(15,a,7,a)].
16 $F.  [resolve(13,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) (NOT P1)) (OR P4 P1 P2 P3) (OR P1 (NOT P4) (NOT P3))
   (OR P4 P3) (OR P3 (NOT P4)) (OR (NOT P4) (NOT P2) P1) (OR P2 P4 P1)
   (OR (NOT P2) P1) (OR (NOT P1) P2 P3))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5448 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:49 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 11.


2 -P3 | -P1.  [assumption].
4 P1 | -P4 | -P3.  [assumption].
5 P4 | P3.  [assumption].
6 P3 | -P4.  [assumption].
8 P2 | P4 | P1.  [assumption].
9 -P2 | P1.  [assumption].
11 P4 | -P1.  [resolve(5,b,2,a)].
12A P1 | P4 | P1.  [resolve(9,a,8,a)].
12 P1 | P4.  [copy(12A),merge(c)].
13 P1 | P3.  [resolve(12,b,6,b)].
14A P1 | P1 | -P4.  [resolve(13,b,4,c)].
14 P1 | -P4.  [copy(14A),merge(b)].
15A P1 | P1.  [resolve(14,b,12,b)].
15 P1.  [copy(15A),merge(b)].
16 P4.  [resolve(15,a,11,b)].
18 -P3.  [resolve(15,a,2,b)].
19A -P4.  [resolve(18,a,6,a)].
19 $F.  [resolve(16,a,19A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 (NOT P1) P2 P3) (OR (NOT P3) P2) (OR P2 (NOT P1) P4 P3)
   (OR (NOT P3) P4) (OR (NOT P2) P3) (OR (NOT P1) P2 P3 (NOT P4))
   (OR (NOT P4) (NOT P2)) (OR P3 P2 P4 P1) (OR P1 P2 P3 (NOT P4)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 2975 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:33 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 12.


2 P4 | -P1 | P2 | P3.  [assumption].
3 -P3 | P2.  [assumption].
4 -P3 | P4.  [assumption].
5 -P2 | P3.  [assumption].
6 -P1 | P2 | P3 | -P4.  [assumption].
7 -P4 | -P2.  [assumption].
8 P3 | P2 | P4 | P1.  [assumption].
9 P1 | P2 | P3 | -P4.  [assumption].
10A P3 | P2 | P4 | P4 | P2 | P3.  [resolve(8,d,2,b)].
10B P3 | P2 | P4 | P2 | P3.  [copy(10A),merge(d)].
10C P3 | P2 | P4 | P3.  [copy(10B),merge(d)].
10 P3 | P2 | P4.  [copy(10C),merge(d)].
11A P3 | P2 | P1 | P2 | P3.  [resolve(10,c,9,d)].
11B P3 | P2 | P1 | P3.  [copy(11A),merge(d)].
11 P3 | P2 | P1.  [copy(11B),merge(d)].
12A P3 | P2 | P2 | P3 | -P4.  [resolve(11,c,6,a)].
12B P3 | P2 | P3 | -P4.  [copy(12A),merge(c)].
12 P3 | P2 | -P4.  [copy(12B),merge(c)].
13A P3 | P2 | P3 | P2.  [resolve(12,c,10,c)].
13B P3 | P2 | P2.  [copy(13A),merge(c)].
13 P3 | P2.  [copy(13B),merge(c)].
14A P3 | P3.  [resolve(13,b,5,a)].
14 P3.  [copy(14A),merge(b)].
15 P4.  [resolve(14,a,4,a)].
16 P2.  [resolve(14,a,3,a)].
17A -P2.  [resolve(15,a,7,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P4) (NOT P3) P2) (OR (NOT P4) (NOT P1))
   (OR (NOT P4) (NOT P3) (NOT P1) P2) (OR P2 (NOT P1)) (OR P4 P1)
   (OR (NOT P3) (NOT P2)) (OR (NOT P1) P3 (NOT P2))
   (OR (NOT P2) P1 P3 (NOT P4)) (OR P3 P1))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 2530 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:30 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 P1 | -P4 | -P3 | P2.  [assumption].
4 P2 | -P1.  [assumption].
5 P4 | P1.  [assumption].
6 -P3 | -P2.  [assumption].
7 -P1 | P3 | -P2.  [assumption].
9 P3 | P1.  [assumption].
10A P1 | P1 | -P3 | P2.  [resolve(5,a,2,b)].
10 P1 | -P3 | P2.  [copy(10A),merge(b)].
11 P1 | -P2.  [resolve(9,a,6,a)].
12A P1 | P2 | P1.  [resolve(10,b,9,a)].
12 P1 | P2.  [copy(12A),merge(c)].
13A P1 | P1.  [resolve(12,b,11,b)].
13 P1.  [copy(13A),merge(b)].
14 P3 | -P2.  [resolve(13,a,7,a)].
15 P2.  [resolve(13,a,4,b)].
17 P3.  [resolve(15,a,14,b)].
18A -P2.  [resolve(17,a,6,a)].
18 $F.  [resolve(15,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P4) (OR P4 P2) (OR (NOT P1) (NOT P2)) (OR (NOT P2) P4)
   (OR P1 P3 (NOT P4)) (OR (NOT P3) (NOT P4) P1 (NOT P2)) (OR P4 (NOT P3))
   (OR (NOT P2) (NOT P4) (NOT P1)) (OR (NOT P4) P2))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 2314 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:28 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 2.


2 -P2 | P4.  [assumption].
3 P4 | P2.  [assumption].
4 -P1 | -P2.  [assumption].
5 P1 | P3 | -P4.  [assumption].
6 -P3 | -P4 | P1 | -P2.  [assumption].
8 -P4 | P2.  [assumption].
9A P4 | P4.  [resolve(3,b,2,a)].
9 P4.  [copy(9A),merge(b)].
10 P2.  [resolve(9,a,8,a)].
11A -P3 | P1 | -P2.  [resolve(9,a,6,b)].
11 -P3 | P1.  [resolve(10,a,11A,c)].
12 P1 | P3.  [resolve(9,a,5,c)].
13 -P1.  [resolve(10,a,4,b)].
14 P3.  [resolve(13,a,12,a)].
15A P1.  [resolve(14,a,11,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) P4 P1) (OR (NOT P3) P2) (OR P3 (NOT P4) (NOT P2) P1)
   (OR P4 P3) (OR P2 P4 P3 P1) (OR (NOT P1) (NOT P3) (NOT P2))
   (OR (NOT P4) (NOT P2) P1) (OR P2 P3) (OR (NOT P2) P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 1147 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:21 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 7.


2 -P3 | P4 | P1.  [assumption].
3 -P3 | P2.  [assumption].
6 -P1 | -P3 | -P2.  [assumption].
7 -P4 | -P2 | P1.  [assumption].
8 P2 | P3.  [assumption].
9 -P2 | P3.  [assumption].
10A P3 | P3.  [resolve(9,a,8,a)].
10 P3.  [copy(10A),merge(b)].
11 -P1 | -P2.  [resolve(10,a,6,b)].
12 P2.  [resolve(10,a,3,a)].
13 P4 | P1.  [resolve(10,a,2,a)].
14 -P1.  [resolve(12,a,11,b)].
15A -P4 | P1.  [resolve(12,a,7,b)].
15 -P4.  [resolve(14,a,15A,b)].
16A P1.  [resolve(15,a,13,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P4) P3) (OR P3 P1) (OR (NOT P2) (NOT P3))
   (OR (NOT P1) P2 P3 (NOT P4)) (OR P2 P4 P3)
   (OR P3 (NOT P4) (NOT P2) (NOT P1)) (OR P2 P4 P1) (OR P4 (NOT P2))
   (OR (NOT P3) P2))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 95528 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:48 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 10.


3 P3 | P1.  [assumption].
4 -P2 | -P3.  [assumption].
5 -P1 | P2 | P3 | -P4.  [assumption].
6 P2 | P4 | P3.  [assumption].
7 P3 | -P4 | -P2 | -P1.  [assumption].
9 P4 | -P2.  [assumption].
10 -P3 | P2.  [assumption].
11A P2 | P3 | -P4 | P3.  [resolve(5,a,3,b)].
11 P2 | P3 | -P4.  [copy(11A),merge(d)].
12A P3 | -P4 | -P2 | P3.  [resolve(7,d,3,b)].
12 P3 | -P4 | -P2.  [copy(12A),merge(d)].
13A P2 | P3 | P2 | P3.  [resolve(11,c,6,b)].
13B P2 | P3 | P3.  [copy(13A),merge(c)].
13 P2 | P3.  [copy(13B),merge(c)].
14A P2 | P2.  [resolve(13,b,10,a)].
14 P2.  [copy(14A),merge(b)].
15 P3 | -P4.  [resolve(14,a,12,c)].
16 P4.  [resolve(14,a,9,b)].
17 -P3.  [resolve(14,a,4,a)].
18A -P4.  [resolve(17,a,15,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) (NOT P1) P4) (OR P3 (NOT P1))
   (OR (NOT P1) (NOT P4) (NOT P3) P2) (OR P2 P1) (OR (NOT P4) (NOT P2))
   (OR (NOT P4) (NOT P1) (NOT P3)) (OR (NOT P2) (NOT P3)) (OR P3 (NOT P2))
   (OR (NOT P4) P2 (NOT P1) P3))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 94400 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:41 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 -P3 | -P1 | P4.  [assumption].
3 P3 | -P1.  [assumption].
5 P2 | P1.  [assumption].
7 -P4 | -P1 | -P3.  [assumption].
8 -P2 | -P3.  [assumption].
9 P3 | -P2.  [assumption].
10 -P3 | P1.  [resolve(8,a,5,a)].
11 P3 | P1.  [resolve(9,b,5,a)].
12A P1 | P1.  [resolve(11,a,10,a)].
12 P1.  [copy(12A),merge(b)].
13 -P4 | -P3.  [resolve(12,a,7,b)].
14 P3.  [resolve(12,a,3,b)].
15A -P1 | P4.  [resolve(14,a,2,a)].
15 P4.  [resolve(12,a,15A,a)].
16A -P3.  [resolve(15,a,13,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P3 (NOT P1) P4) (OR P3 P2 P4) (OR (NOT P3) P4 P2)
   (OR (NOT P2) P1) (OR P2 (NOT P4)) (OR P1 (NOT P3) (NOT P4) P2)
   (OR (NOT P2) (NOT P1) (NOT P3)) (OR (NOT P1) (NOT P4))
   (OR (NOT P4) (NOT P2) (NOT P3)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 93396 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:35 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 P3 | -P1 | P4.  [assumption].
3 P3 | P2 | P4.  [assumption].
4 -P3 | P4 | P2.  [assumption].
5 -P2 | P1.  [assumption].
6 P2 | -P4.  [assumption].
7 -P2 | -P1 | -P3.  [assumption].
8 -P1 | -P4.  [assumption].
10A P4 | P2 | P2 | P4.  [resolve(4,a,3,a)].
10B P4 | P2 | P4.  [copy(10A),merge(c)].
10 P4 | P2.  [copy(10B),merge(c)].
11A P2 | P2.  [resolve(10,a,6,b)].
11 P2.  [copy(11A),merge(b)].
13 -P1 | -P3.  [resolve(11,a,7,a)].
14 P1.  [resolve(11,a,5,a)].
15 -P3.  [resolve(14,a,13,a)].
16 -P4.  [resolve(14,a,8,a)].
17A -P1 | P4.  [resolve(15,a,2,a)].
17B P4.  [resolve(14,a,17A,a)].
17 $F.  [resolve(16,a,17B,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P3)) (OR P3 (NOT P4)) (OR (NOT P4) P2)
   (OR P1 (NOT P4) (NOT P2) P3) (OR (NOT P1) P3) (OR P2 P3)
   (OR (NOT P1) (NOT P3) (NOT P2)) (OR (NOT P3) (NOT P1) P4) (OR (NOT P2) P4))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 9)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 90940 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:20 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 5.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 P1 | -P3.  [assumption].
3 P3 | -P4.  [assumption].
4 -P4 | P2.  [assumption].
6 P2 | P3.  [assumption].
7 -P1 | -P3 | -P2.  [assumption].
8 -P3 | -P1 | P4.  [assumption].
9 -P2 | P4.  [assumption].
10 P4 | P3.  [resolve(9,a,6,a)].
11A P3 | P3.  [resolve(10,a,3,b)].
11 P3.  [copy(11A),merge(b)].
12 -P1 | P4.  [resolve(11,a,8,a)].
13 -P1 | -P2.  [resolve(11,a,7,b)].
14 P1.  [resolve(11,a,2,b)].
15 -P2.  [resolve(14,a,13,a)].
16 P4.  [resolve(14,a,12,a)].
17A P2.  [resolve(16,a,4,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P3) (NOT P4)) (OR P2 (NOT P1) (NOT P3)) (OR P2 P3)
   (OR (NOT P2) P4 (NOT P1)) (OR P1 P4) (OR (NOT P2) (NOT P4))
   (OR P3 (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 75954 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:00:48 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 11.


2 -P3 | -P4.  [assumption].
3 P2 | -P1 | -P3.  [assumption].
4 P2 | P3.  [assumption].
5 -P2 | P4 | -P1.  [assumption].
6 P1 | P4.  [assumption].
7 -P2 | -P4.  [assumption].
9A P2 | P2 | -P1.  [resolve(4,b,3,c)].
9 P2 | -P1.  [copy(9A),merge(b)].
10 P1 | -P3.  [resolve(6,b,2,b)].
11 -P2 | P1.  [resolve(7,b,6,b)].
12 P1 | P2.  [resolve(10,b,4,b)].
13A P1 | P1.  [resolve(12,b,11,a)].
13 P1.  [copy(13A),merge(b)].
14 P2.  [resolve(13,a,9,b)].
16A P4 | -P1.  [resolve(14,a,5,a)].
16 P4.  [resolve(13,a,16A,b)].
17A -P4.  [resolve(14,a,7,a)].
17 $F.  [resolve(16,a,17A,a)].

============================== end of proof ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 2 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 11.


2 -P3 | -P4.  [assumption].
3 P2 | -P1 | -P3.  [assumption].
4 P2 | P3.  [assumption].
5 -P2 | P4 | -P1.  [assumption].
6 P1 | P4.  [assumption].
7 -P2 | -P4.  [assumption].
8 P3 | -P1.  [assumption].
9A P2 | P2 | -P1.  [resolve(4,b,3,c)].
9 P2 | -P1.  [copy(9A),merge(b)].
10 P1 | -P3.  [resolve(6,b,2,b)].
11 -P2 | P1.  [resolve(7,b,6,b)].
12 P1 | P2.  [resolve(10,b,4,b)].
13A P1 | P1.  [resolve(12,b,11,a)].
13 P1.  [copy(13A),merge(b)].
14 P2.  [resolve(13,a,9,b)].
15 P3.  [resolve(13,a,8,b)].
16A P4 | -P1.  [resolve(14,a,5,a)].
16 P4.  [resolve(13,a,16A,b)].
18A -P4.  [resolve(15,a,2,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 (NOT P1)) (OR P2 P3) (OR (NOT P2) (NOT P4) (NOT P3))
   (OR (NOT P1) P4 (NOT P2) (NOT P3)) (OR (NOT P2) P3) (OR (NOT P3) P1)
   (OR P3 (NOT P2)) (OR (NOT P2) (NOT P1) P3) (OR P4 (NOT P2) P1))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5492 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:49 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 8.


2 P2 | -P1.  [assumption].
3 P2 | P3.  [assumption].
4 -P2 | -P4 | -P3.  [assumption].
5 -P1 | P4 | -P2 | -P3.  [assumption].
6 -P2 | P3.  [assumption].
7 -P3 | P1.  [assumption].
9 P1 | P2.  [resolve(7,a,3,b)].
10A P2 | P2.  [resolve(9,a,2,b)].
10 P2.  [copy(10A),merge(b)].
12 P3.  [resolve(10,a,6,a)].
13A -P1 | P4 | -P3.  [resolve(10,a,5,c)].
13 -P1 | P4.  [resolve(12,a,13A,c)].
14A -P4 | -P3.  [resolve(10,a,4,a)].
14 -P4.  [resolve(12,a,14A,b)].
15 P1.  [resolve(12,a,7,a)].
16A P4.  [resolve(15,a,13,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P2) (OR P1 (NOT P2) P3) (OR P4 P3 (NOT P2) (NOT P1))
   (OR (NOT P4) P1 (NOT P2) P3) (OR P2 (NOT P1)) (OR (NOT P2) (NOT P3))
   (OR P2 P1) (OR (NOT P1) (NOT P4) P3) (OR (NOT P1) (NOT P4)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 3335 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:35 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 13.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 9.


3 P1 | -P2 | P3.  [assumption].
4 P4 | P3 | -P2 | -P1.  [assumption].
5 P2 | -P1.  [assumption].
6 -P2 | -P3.  [assumption].
7 P2 | P1.  [assumption].
9 -P1 | -P4.  [assumption].
10A P1 | P1 | P3.  [resolve(7,a,3,b)].
10 P1 | P3.  [copy(10A),merge(b)].
11 P1 | -P2.  [resolve(10,b,6,b)].
12A P1 | P1.  [resolve(11,b,7,a)].
12 P1.  [copy(12A),merge(b)].
13 -P4.  [resolve(12,a,9,a)].
14 P2.  [resolve(12,a,5,b)].
15A P3 | -P2 | -P1.  [resolve(13,a,4,a)].
15B P3 | -P1.  [resolve(14,a,15A,b)].
15 P3.  [resolve(12,a,15B,b)].
16A -P3.  [resolve(14,a,6,a)].
16 $F.  [resolve(15,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 (NOT P2) P3 (NOT P1)) (OR (NOT P3) P1) (OR P2 P3)
   (OR (NOT P2) P1) (OR (NOT P3) (NOT P1) P4) (OR P2 (NOT P3))
   (OR P3 (NOT P1) (NOT P4) P2) (OR (NOT P1) (NOT P4))
   (OR (NOT P3) (NOT P4) (NOT P1)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 2010 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:27 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 P4 | -P2 | P3 | -P1.  [assumption].
3 -P3 | P1.  [assumption].
4 P2 | P3.  [assumption].
5 -P2 | P1.  [assumption].
6 -P3 | -P1 | P4.  [assumption].
8 -P1 | -P4.  [assumption].
9A P3 | P4 | P3 | -P1.  [resolve(4,a,2,b)].
9 P3 | P4 | -P1.  [copy(9A),merge(c)].
10 P1 | P3.  [resolve(5,a,4,a)].
11A P3 | P3 | P4.  [resolve(10,a,9,c)].
11 P3 | P4.  [copy(11A),merge(b)].
12 P3 | -P1.  [resolve(11,b,8,b)].
13A P3 | P3.  [resolve(12,b,10,a)].
13 P3.  [copy(13A),merge(b)].
15 -P1 | P4.  [resolve(13,a,6,a)].
16 P1.  [resolve(13,a,3,a)].
17 P4.  [resolve(16,a,15,a)].
18A -P4.  [resolve(16,a,8,a)].
18 $F.  [resolve(17,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P1) (OR P1 (NOT P4) P3 P2) (OR (NOT P1) P3) (OR P4 P3)
   (OR (NOT P2) (NOT P3) P4) (OR (NOT P2) (NOT P4) (NOT P3) (NOT P1))
   (OR (NOT P1) P4) (OR (NOT P2) P4) (OR P2 (NOT P3)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 97780 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:02 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 -P2 | P1.  [assumption].
3 P1 | -P4 | P3 | P2.  [assumption].
4 -P1 | P3.  [assumption].
5 P4 | P3.  [assumption].
7 -P2 | -P4 | -P3 | -P1.  [assumption].
9 -P2 | P4.  [assumption].
10 P2 | -P3.  [assumption].
11A P3 | P1 | P3 | P2.  [resolve(5,a,3,b)].
11 P3 | P1 | P2.  [copy(11A),merge(c)].
12A P3 | P2 | P3.  [resolve(11,b,4,a)].
12 P3 | P2.  [copy(12A),merge(c)].
13A P2 | P2.  [resolve(12,a,10,b)].
13 P2.  [copy(13A),merge(b)].
14 P4.  [resolve(13,a,9,a)].
15A -P4 | -P3 | -P1.  [resolve(13,a,7,a)].
15 -P3 | -P1.  [resolve(14,a,15A,a)].
16 P1.  [resolve(13,a,2,a)].
17 -P3.  [resolve(16,a,15,b)].
18A P3.  [resolve(16,a,4,a)].
18 $F.  [resolve(17,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P2) P3 (NOT P4)) (OR P2 (NOT P3)) (OR P2 P1)
   (OR (NOT P3) P2 (NOT P1) (NOT P4)) (OR P3 (NOT P2)) (OR (NOT P1) P4)
   (OR (NOT P3) P1 (NOT P4)) (OR (NOT P3) (NOT P2))
   (OR P3 (NOT P1) P2 (NOT P4)))
  (:MIN-PROOF-LEVEL . 8) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 97320 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:59 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 8.
% Maximum clause weight is 4.000.
% Given clauses 12.


3 P2 | -P3.  [assumption].
4 P2 | P1.  [assumption].
5 P3 | -P2.  [assumption].
6 -P1 | P4.  [assumption].
8 -P3 | -P2.  [assumption].
9 P3 | -P1 | P2 | -P4.  [assumption].
10 P3 | P1.  [resolve(5,b,4,a)].
11 P1 | -P2.  [resolve(10,a,8,a)].
12A P1 | P1.  [resolve(11,b,4,a)].
12 P1.  [copy(12A),merge(b)].
13 P3 | P2 | -P4.  [resolve(12,a,9,b)].
14 P4.  [resolve(12,a,6,a)].
15 P3 | P2.  [resolve(14,a,13,c)].
16A P2 | P2.  [resolve(15,a,3,b)].
16 P2.  [copy(16A),merge(b)].
17 -P3.  [resolve(16,a,8,b)].
18A -P2.  [resolve(17,a,5,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P2) P3) (OR P2 P3) (OR (NOT P3) (NOT P2) P1 (NOT P4))
   (OR P2 (NOT P4)) (OR P1 P2) (OR (NOT P3) (NOT P2) P4) (OR P2 (NOT P3))
   (OR (NOT P4) (NOT P1) (NOT P2)) (OR P1 P2 (NOT P3) (NOT P4)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 97132 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:58 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 7.


2 -P2 | P3.  [assumption].
3 P2 | P3.  [assumption].
4 -P3 | -P2 | P1 | -P4.  [assumption].
7 -P3 | -P2 | P4.  [assumption].
8 P2 | -P3.  [assumption].
9 -P4 | -P1 | -P2.  [assumption].
10A P2 | P2.  [resolve(8,b,3,b)].
10 P2.  [copy(10A),merge(b)].
11 -P4 | -P1.  [resolve(10,a,9,c)].
12 -P3 | P4.  [resolve(10,a,7,b)].
13 -P3 | P1 | -P4.  [resolve(10,a,4,b)].
14 P3.  [resolve(10,a,2,a)].
15 P1 | -P4.  [resolve(14,a,13,a)].
16 P4.  [resolve(14,a,12,a)].
17 P1.  [resolve(16,a,15,b)].
18A -P1.  [resolve(16,a,11,a)].
18 $F.  [resolve(17,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 (NOT P1)) (OR P2 (NOT P4)) (OR (NOT P4) (NOT P2) (NOT P3))
   (OR P4 (NOT P2) (NOT P1)) (OR P3 P1) (OR P3 (NOT P1))
   (OR P1 (NOT P4) (NOT P2) P3) (OR (NOT P4) (NOT P1) P2) (OR P4 P1))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 97040 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:57 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 9.


2 P4 | -P1.  [assumption].
3 P2 | -P4.  [assumption].
4 -P4 | -P2 | -P3.  [assumption].
5 P3 | P1.  [assumption].
6 P3 | -P1.  [assumption].
7 P4 | P1.  [assumption].
8 P1 | -P4 | -P2.  [resolve(5,a,4,c)].
9 P1 | P2.  [resolve(7,a,3,b)].
10A P1 | P1 | -P4.  [resolve(9,b,8,c)].
10 P1 | -P4.  [copy(10A),merge(b)].
11A P1 | P1.  [resolve(10,b,7,a)].
11 P1.  [copy(11A),merge(b)].
12 P3.  [resolve(11,a,6,b)].
13 P4.  [resolve(11,a,2,b)].
14A -P2 | -P3.  [resolve(13,a,4,a)].
14 -P2.  [resolve(12,a,14A,b)].
15A -P4.  [resolve(14,a,3,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) (NOT P3)) (OR (NOT P3) (NOT P2))
   (OR P3 (NOT P2) (NOT P4)) (OR (NOT P3) P2 P4 P1) (OR P4 (NOT P1))
   (OR P3 P4 (NOT P2)) (OR (NOT P4) P2) (OR P3 P1) (OR P2 (NOT P1)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 96004 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:51 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 12.


3 -P3 | -P2.  [assumption].
4 P3 | -P2 | -P4.  [assumption].
5 -P3 | P2 | P4 | P1.  [assumption].
6 P4 | -P1.  [assumption].
8 -P4 | P2.  [assumption].
9 P3 | P1.  [assumption].
10 P2 | -P1.  [assumption].
11A P1 | P2 | P4 | P1.  [resolve(9,a,5,a)].
11 P1 | P2 | P4.  [copy(11A),merge(d)].
12 P1 | -P2.  [resolve(9,a,3,a)].
13A P2 | P4 | P2.  [resolve(11,a,10,b)].
13 P2 | P4.  [copy(13A),merge(c)].
14A P2 | P2.  [resolve(13,b,8,a)].
14 P2.  [copy(14A),merge(b)].
15 P1.  [resolve(14,a,12,b)].
17 P3 | -P4.  [resolve(14,a,4,b)].
18 -P3.  [resolve(14,a,3,b)].
19 P4.  [resolve(15,a,6,b)].
20A -P4.  [resolve(18,a,17,a)].
20 $F.  [resolve(19,a,20A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P4) (OR P2 P1 (NOT P4) (NOT P3)) (OR (NOT P4) P3)
   (OR (NOT P4) (NOT P2)) (OR P4 (NOT P1) (NOT P3) (NOT P2))
   (OR P4 (NOT P1) (NOT P2)) (OR (NOT P1) P2 (NOT P3)) (OR (NOT P2) P4 P1)
   (OR (NOT P1) (NOT P2)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 94912 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:44 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 P2 | P4.  [assumption].
3 P2 | P1 | -P4 | -P3.  [assumption].
4 -P4 | P3.  [assumption].
5 -P4 | -P2.  [assumption].
8 -P1 | P2 | -P3.  [assumption].
9 -P2 | P4 | P1.  [assumption].
10 -P1 | -P2.  [assumption].
11 P3 | P2.  [resolve(4,a,2,b)].
12A P2 | -P1 | P2.  [resolve(11,a,8,c)].
12 P2 | -P1.  [copy(12A),merge(c)].
13A P2 | P2 | P1 | -P4.  [resolve(11,a,3,d)].
13 P2 | P1 | -P4.  [copy(13A),merge(b)].
14A P2 | P1 | P2.  [resolve(13,c,2,b)].
14 P2 | P1.  [copy(14A),merge(c)].
15A P2 | P2.  [resolve(14,b,12,b)].
15 P2.  [copy(15A),merge(b)].
16 -P1.  [resolve(15,a,10,b)].
17A P4 | P1.  [resolve(15,a,9,a)].
17 P4.  [resolve(16,a,17A,b)].
18A -P2.  [resolve(17,a,5,a)].
18 $F.  [resolve(15,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 P3) (OR (NOT P1) P4) (OR (NOT P4) P2 P1 P3)
   (OR (NOT P3) P4 (NOT P1)) (OR (NOT P1) (NOT P2) P3)
   (OR P3 (NOT P1) P2 (NOT P4)) (OR (NOT P3) P1) (OR P4 P3 (NOT P1))
   (OR (NOT P1) (NOT P3) (NOT P4)))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 10)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 91816 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:25 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 5.


2 P1 | P3.  [assumption].
3 -P1 | P4.  [assumption].
4 -P1 | -P2 | P3.  [assumption].
5 P3 | -P1 | P2 | -P4.  [assumption].
6 -P3 | P1.  [assumption].
7 -P1 | -P3 | -P4.  [assumption].
8A P1 | P1.  [resolve(6,a,2,b)].
8 P1.  [copy(8A),merge(b)].
9 -P3 | -P4.  [resolve(8,a,7,a)].
10 P3 | P2 | -P4.  [resolve(8,a,5,b)].
11 -P2 | P3.  [resolve(8,a,4,a)].
12 P4.  [resolve(8,a,3,a)].
13 P3 | P2.  [resolve(12,a,10,c)].
14 -P3.  [resolve(12,a,9,b)].
15 P2.  [resolve(14,a,13,a)].
16A P3.  [resolve(15,a,11,a)].
16 $F.  [resolve(14,a,16A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P2 P1) (OR (NOT P3) P4 (NOT P1)) (OR (NOT P2) (NOT P3))
   (OR P2 (NOT P1) (NOT P4)) (OR P3 P4) (OR P3 (NOT P4) (NOT P1))
   (OR (NOT P1) P3) (OR (NOT P4) (NOT P2)))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 84520 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:01:40 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 6.
% Maximum clause weight is 3.000.
% Given clauses 10.


2 P2 | P1.  [assumption].
3 -P3 | P4 | -P1.  [assumption].
4 -P2 | -P3.  [assumption].
5 P2 | -P1 | -P4.  [assumption].
6 P3 | P4.  [assumption].
8 -P1 | P3.  [assumption].
9 -P4 | -P2.  [assumption].
10 -P3 | P1.  [resolve(4,a,2,a)].
11 -P4 | P1.  [resolve(9,b,2,a)].
12 P1 | P3.  [resolve(11,a,6,b)].
13A P1 | P1.  [resolve(12,b,10,a)].
13 P1.  [copy(13A),merge(b)].
14 P3.  [resolve(13,a,8,a)].
15 P2 | -P4.  [resolve(13,a,5,b)].
16A P4 | -P1.  [resolve(14,a,3,a)].
16 P4.  [resolve(13,a,16A,b)].
17 -P2.  [resolve(14,a,4,b)].
18A -P4.  [resolve(17,a,15,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P2 P1) (OR P1 P4) (OR (NOT P1) P3) (OR P1 (NOT P3))
   (OR (NOT P2) (NOT P4)) (OR (NOT P2) P4 (NOT P3) (NOT P1))
   (OR (NOT P3) (NOT P4) (NOT P1) P2) (OR P4 (NOT P3) (NOT P1))
   (OR P4 P3 (NOT P2) (NOT P1)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 5704 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:50 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 9.


2 -P4 | P2 | P1.  [assumption].
3 P1 | P4.  [assumption].
4 -P1 | P3.  [assumption].
6 -P2 | -P4.  [assumption].
8 -P3 | -P4 | -P1 | P2.  [assumption].
9 P4 | -P3 | -P1.  [assumption].
10A P1 | P2 | P1.  [resolve(3,b,2,a)].
10 P1 | P2.  [copy(10A),merge(c)].
11 P1 | -P4.  [resolve(10,b,6,a)].
12A P1 | P1.  [resolve(11,b,3,b)].
12 P1.  [copy(12A),merge(b)].
13 P4 | -P3.  [resolve(12,a,9,c)].
14 -P3 | -P4 | P2.  [resolve(12,a,8,c)].
15 P3.  [resolve(12,a,4,a)].
16 -P4 | P2.  [resolve(15,a,14,a)].
17 P4.  [resolve(15,a,13,b)].
18 P2.  [resolve(17,a,16,a)].
19A -P4.  [resolve(18,a,6,a)].
19 $F.  [resolve(17,a,19A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P2) (OR (NOT P2) (NOT P4)) (OR P4 P1)
   (OR (NOT P1) (NOT P3) (NOT P4) (NOT P2)) (OR (NOT P2) (NOT P3) P1 (NOT P4))
   (OR P3 (NOT P2) (NOT P1)) (OR (NOT P2) P4 (NOT P1) (NOT P3))
   (OR P2 (NOT P1)) (OR P4 P1 P3 P2))
  (:MIN-PROOF-LEVEL . 5) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 4526 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:43 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 14.
% Level of proof is 5.
% Maximum clause weight is 4.000.
% Given clauses 8.


2 -P4 | P2.  [assumption].
3 -P2 | -P4.  [assumption].
4 P4 | P1.  [assumption].
5 P3 | -P2 | -P1.  [assumption].
6 -P2 | P4 | -P1 | -P3.  [assumption].
7 P2 | -P1.  [assumption].
8 P1 | -P2.  [resolve(4,a,3,b)].
9 P1 | P2.  [resolve(4,a,2,a)].
10A P1 | P1.  [resolve(9,b,8,b)].
10 P1.  [copy(10A),merge(b)].
11 P2.  [resolve(10,a,7,b)].
12A P4 | -P1 | -P3.  [resolve(11,a,6,a)].
12 P4 | -P3.  [resolve(10,a,12A,b)].
13A P3 | -P1.  [resolve(11,a,5,b)].
13 P3.  [resolve(10,a,13A,b)].
14 -P4.  [resolve(11,a,3,a)].
15A -P3.  [resolve(14,a,12,a)].
15 $F.  [resolve(13,a,15A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P1 (NOT P3)) (OR P1 P2) (OR (NOT P1) P4 P3)
   (OR (NOT P1) (NOT P2)) (OR (NOT P2) P1) (OR (NOT P3) P2 (NOT P4)) (OR P4 P3)
   (OR (NOT P1) (NOT P4) P3 P2) (OR (NOT P1) (NOT P3) P4 P2))
  (:MIN-PROOF-LEVEL . 6) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 764 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:19 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 17.
% Level of proof is 6.
% Maximum clause weight is 4.000.
% Given clauses 9.


3 P1 | P2.  [assumption].
5 -P1 | -P2.  [assumption].
6 -P2 | P1.  [assumption].
7 -P3 | P2 | -P4.  [assumption].
8 P4 | P3.  [assumption].
9 -P1 | -P4 | P3 | P2.  [assumption].
10 -P1 | -P3 | P4 | P2.  [assumption].
11A P1 | P1.  [resolve(6,a,3,b)].
11 P1.  [copy(11A),merge(b)].
12 -P3 | P4 | P2.  [resolve(11,a,10,a)].
13 -P4 | P3 | P2.  [resolve(11,a,9,a)].
14 -P2.  [resolve(11,a,5,a)].
15 -P4 | P3.  [resolve(14,a,13,c)].
16 -P3 | P4.  [resolve(14,a,12,c)].
17 -P3 | -P4.  [resolve(14,a,7,b)].
18A P3 | P3.  [resolve(15,a,8,a)].
18 P3.  [copy(18A),merge(b)].
19 -P4.  [resolve(18,a,17,a)].
20A P4.  [resolve(18,a,16,a)].
20 $F.  [resolve(19,a,20A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P4 P1 P3 P2) (OR P3 (NOT P1) (NOT P2))
   (OR P1 (NOT P2) (NOT P3) (NOT P4)) (OR (NOT P2) P3 P4) (OR P1 (NOT P3))
   (OR (NOT P3) (NOT P1) (NOT P2)) (OR (NOT P1) P2)
   (OR P3 (NOT P1) (NOT P2) (NOT P4)) (OR (NOT P4) P1))
  (:MIN-PROOF-LEVEL . 8) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 98036 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:03 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 15.
% Level of proof is 8.
% Maximum clause weight is 4.000.
% Given clauses 11.


2 P4 | P1 | P3 | P2.  [assumption].
3 P3 | -P1 | -P2.  [assumption].
5 -P2 | P3 | P4.  [assumption].
6 P1 | -P3.  [assumption].
7 -P3 | -P1 | -P2.  [assumption].
8 -P1 | P2.  [assumption].
9 -P4 | P1.  [assumption].
10A P1 | P1 | P3 | P2.  [resolve(9,a,2,a)].
10 P1 | P3 | P2.  [copy(10A),merge(b)].
11A P1 | P2 | P1.  [resolve(10,b,6,b)].
11 P1 | P2.  [copy(11A),merge(c)].
12 P1 | P3 | P4.  [resolve(11,b,5,a)].
13A P1 | P3 | P1.  [resolve(12,c,9,a)].
13 P1 | P3.  [copy(13A),merge(c)].
14A P1 | P1.  [resolve(13,b,6,b)].
14 P1.  [copy(14A),merge(b)].
15 P2.  [resolve(14,a,8,a)].
16A -P3 | -P2.  [resolve(14,a,7,b)].
16 -P3.  [resolve(15,a,16A,b)].
17A -P1 | -P2.  [resolve(16,a,3,a)].
17B -P2.  [resolve(14,a,17A,a)].
17 $F.  [resolve(15,a,17B,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 (NOT P2)) (OR (NOT P4) (NOT P1) (NOT P3) (NOT P2))
   (OR P4 (NOT P3)) (OR P3 (NOT P2) P4 P1) (OR P1 P3 P4) (OR (NOT P1) P2)
   (OR (NOT P4) P2) (OR (NOT P4) (NOT P2) P1) (OR (NOT P1) (NOT P2) P3))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 96164 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:52 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 9.


2 P1 | -P2.  [assumption].
3 -P4 | -P1 | -P3 | -P2.  [assumption].
4 P4 | -P3.  [assumption].
5 P1 | P3 | P4.  [assumption].
6 -P1 | P2.  [assumption].
7 -P4 | P2.  [assumption].
8 -P1 | -P2 | P3.  [assumption].
9A P1 | P4 | P4.  [resolve(5,b,4,b)].
9 P1 | P4.  [copy(9A),merge(c)].
10 P1 | P2.  [resolve(9,b,7,a)].
11A P1 | P1.  [resolve(10,b,2,b)].
11 P1.  [copy(11A),merge(b)].
12 -P2 | P3.  [resolve(11,a,8,a)].
13 P2.  [resolve(11,a,6,a)].
14A -P4 | -P3 | -P2.  [resolve(11,a,3,b)].
14 -P4 | -P3.  [resolve(13,a,14A,c)].
15 P3.  [resolve(13,a,12,a)].
16 -P4.  [resolve(15,a,14,b)].
17A -P3.  [resolve(16,a,4,a)].
17 $F.  [resolve(15,a,17A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P1) P3) (OR (NOT P3) P4 P2)
   (OR (NOT P4) (NOT P2) (NOT P1) (NOT P3)) (OR (NOT P4) (NOT P2)) (OR P1 P3)
   (OR (NOT P1) P2) (OR (NOT P4) P1 (NOT P3)) (OR P2 P1 (NOT P3) P4)
   (OR (NOT P3) P4 (NOT P2)))
  (:MIN-PROOF-LEVEL . 8) (:MIN-PROOF-LENGTH . 11)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 94620 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:42 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 17.
% Level of proof is 8.
% Maximum clause weight is 3.000.
% Given clauses 11.


2 -P1 | P3.  [assumption].
3 -P3 | P4 | P2.  [assumption].
5 -P4 | -P2.  [assumption].
6 P1 | P3.  [assumption].
7 -P1 | P2.  [assumption].
8 -P4 | P1 | -P3.  [assumption].
9 -P3 | P4 | -P2.  [assumption].
10A P3 | P3.  [resolve(6,a,2,a)].
10 P3.  [copy(10A),merge(b)].
11 P4 | -P2.  [resolve(10,a,9,a)].
12 -P4 | P1.  [resolve(10,a,8,c)].
13 P4 | P2.  [resolve(10,a,3,a)].
14 P2 | P1.  [resolve(13,a,12,a)].
15 P1 | P4.  [resolve(14,a,11,b)].
16A P1 | P1.  [resolve(15,b,12,a)].
16 P1.  [copy(16A),merge(b)].
17 P2.  [resolve(16,a,7,a)].
18 P4.  [resolve(17,a,11,b)].
19A -P2.  [resolve(18,a,5,a)].
19 $F.  [resolve(17,a,19A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) (NOT P1)) (OR P2 P3 P4 P1) (OR P4 P2)
   (OR P4 P1 (NOT P2)) (OR P1 (NOT P2) (NOT P3) (NOT P4)) (OR P3 (NOT P4) P1)
   (OR P2 (NOT P3)) (OR (NOT P3) P2 P1) (OR (NOT P2) (NOT P1)))
  (:MIN-PROOF-LEVEL . 7) (:MIN-PROOF-LENGTH . 13)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 2034 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:27 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 16.
% Level of proof is 7.
% Maximum clause weight is 4.000.
% Given clauses 10.


2 -P4 | -P1.  [assumption].
4 P4 | P2.  [assumption].
5 P4 | P1 | -P2.  [assumption].
6 P1 | -P2 | -P3 | -P4.  [assumption].
7 P3 | -P4 | P1.  [assumption].
8 P2 | -P3.  [assumption].
9 -P2 | -P1.  [assumption].
10 P2 | -P1.  [resolve(4,a,2,a)].
11 P3 | P1 | P2.  [resolve(7,b,4,a)].
12A P1 | P2 | P2.  [resolve(11,a,8,b)].
12 P1 | P2.  [copy(12A),merge(c)].
13A P2 | P2.  [resolve(12,a,10,b)].
13 P2.  [copy(13A),merge(b)].
14 -P1.  [resolve(13,a,9,a)].
15A -P2 | -P3 | -P4.  [resolve(14,a,6,a)].
15 -P3 | -P4.  [resolve(13,a,15A,a)].
16A P4 | -P2.  [resolve(14,a,5,b)].
16 P4.  [resolve(13,a,16A,b)].
17A P3 | P1.  [resolve(16,a,7,b)].
17 P3.  [resolve(14,a,17A,b)].
18A -P4.  [resolve(17,a,15,a)].
18 $F.  [resolve(16,a,18A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR (NOT P4) P3) (OR P4 P1) (OR (NOT P1) (NOT P2)) (OR P3 P2 P4)
   (OR P1 (NOT P3) (NOT P4)) (OR P2 (NOT P4) (NOT P1))
   (OR (NOT P3) (NOT P4) (NOT P1) (NOT P2)) (OR P2 (NOT P1) P3 (NOT P4))
   (OR (NOT P1) P2 P4 (NOT P3)))
  (:MIN-PROOF-LEVEL . 10) (:MIN-PROOF-LENGTH . 13)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 652 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:03:18 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 19.
% Level of proof is 10.
% Maximum clause weight is 4.000.
% Given clauses 12.


2 -P4 | P3.  [assumption].
3 P4 | P1.  [assumption].
4 -P1 | -P2.  [assumption].
5 P3 | P2 | P4.  [assumption].
6 P1 | -P3 | -P4.  [assumption].
7 P2 | -P4 | -P1.  [assumption].
8 -P1 | P2 | P4 | -P3.  [assumption].
9 P3 | P4 | -P1.  [resolve(5,b,4,b)].
10A P3 | P4 | P4.  [resolve(9,c,3,b)].
10 P3 | P4.  [copy(10A),merge(c)].
11A P4 | -P1 | P2 | P4.  [resolve(10,a,8,d)].
11 P4 | -P1 | P2.  [copy(11A),merge(d)].
12A P4 | P2 | P4.  [resolve(11,b,3,b)].
12 P4 | P2.  [copy(12A),merge(c)].
13 P4 | -P1.  [resolve(12,b,4,b)].
14A P4 | P4.  [resolve(13,b,3,b)].
14 P4.  [copy(14A),merge(b)].
15 P2 | -P1.  [resolve(14,a,7,b)].
16 P1 | -P3.  [resolve(14,a,6,c)].
17 P3.  [resolve(14,a,2,a)].
18 P1.  [resolve(17,a,16,b)].
19 P2.  [resolve(18,a,15,b)].
20A -P2.  [resolve(18,a,4,a)].
20 $F.  [resolve(19,a,20A,a)].

============================== end of proof ==========================
"))
 ((:PUZZLE (OR P1 P3 (NOT P2) P4) (OR P4 (NOT P2) (NOT P1))
   (OR (NOT P4) (NOT P3) (NOT P2) P1) (OR (NOT P2) P4 (NOT P3))
   (OR P2 (NOT P1)) (OR P3 P2) (OR (NOT P4) (NOT P1)) (OR P1 P2)
   (OR P1 (NOT P4) P3 (NOT P2)))
  (:MIN-PROOF-LEVEL . 8) (:MIN-PROOF-LENGTH . 13)
  (:PROOF-FILE-STRING
   . "============================== prooftrans ============================
Prover9 (64) version 2009-11A, November 2009.
Process 91696 was started by Naveen on m5-05.dynamic.rpi.edu,
Wed Jun  5 18:02:24 2013
The command was \"/Applications/LADR-2009-11A/bin/prover9 -f /Volumes/prover9ramdisk/prob.in\".
============================== end of head ===========================

============================== end of input ==========================

============================== PROOF =================================

% -------- Comments from original proof --------
% Proof 1 at 0.00 (+ 0.00) seconds.
% Length of proof is 20.
% Level of proof is 8.
% Maximum clause weight is 4.000.
% Given clauses 17.


2 P1 | P3 | -P2 | P4.  [assumption].
3 P4 | -P2 | -P1.  [assumption].
4 -P4 | -P3 | -P2 | P1.  [assumption].
5 -P2 | P4 | -P3.  [assumption].
6 P2 | -P1.  [assumption].
8 -P4 | -P1.  [assumption].
9 P1 | P2.  [assumption].
10 P1 | -P4 | P3 | -P2.  [assumption].
11A P2 | P2.  [resolve(9,a,6,b)].
11 P2.  [copy(11A),merge(b)].
12 P1 | -P4 | P3.  [resolve(11,a,10,d)].
13 P4 | -P3.  [resolve(11,a,5,a)].
14 -P4 | -P3 | P1.  [resolve(11,a,4,c)].
15 P4 | -P1.  [resolve(11,a,3,b)].
16 P1 | P3 | P4.  [resolve(11,a,2,c)].
17A P1 | P3 | P1 | P3.  [resolve(16,c,12,b)].
17B P1 | P3 | P3.  [copy(17A),merge(c)].
17 P1 | P3.  [copy(17B),merge(c)].
18 P1 | P4.  [resolve(17,b,13,b)].
19A P1 | -P3 | P1.  [resolve(18,b,14,a)].
19 P1 | -P3.  [copy(19A),merge(c)].
20A P1 | P1.  [resolve(19,b,17,b)].
20 P1.  [copy(20A),merge(b)].
21 P4.  [resolve(20,a,15,b)].
22A -P1.  [resolve(21,a,8,a)].
22 $F.  [resolve(20,a,22A,a)].

============================== end of proof ==========================
"))) 