//
//  Signature.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/23/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "Signature.h"

@implementation Signature
@synthesize arity;
@synthesize color;
@synthesize id_n;
@synthesize variable;
+(id)withArity:(NSInteger) arity andColor:(ccColor3B) color andVariable:(BOOL) var{
    Signature* sig=[[[Signature alloc] init] autorelease];
    sig.arity=arity;
    sig.color=color;
    sig.id_n=0;
    sig.variable=var;
    return sig;
}
+(id)withId:(NSInteger) id_n {
    Signature* sig=[[Signature alloc] init] ;
    [sig retain];
    sig.arity=-1;
   // sig.color=ccWHITE;
    sig.id_n=id_n;
    return [sig autorelease];

};

@end
