//
//  MainMenuScene.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

//  MainMenuScene.h
//  SpaceViking
//
#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MainMenuLayer.h"
@interface MainMenuScene : CCScene {
    MainMenuLayer *mainMenuLayer;
}
@end
