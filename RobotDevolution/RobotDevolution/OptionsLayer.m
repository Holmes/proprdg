//
//  OptionsLayer.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 1/6/13.
//  Copyright 2013 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "OptionsLayer.h"
#import "GameManager.h"
#import "GameConfig.h"

@implementation OptionsLayer
-(void)openMotalenSite {
    [[GameManager sharedGameManager]
     openSiteWithLinkType:kLinkTypeMotalen];
}
-(void) resetGame{
    [[GameManager sharedGameManager] reset];
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Puzzles Reset! "];
    [dialog setMessage:@"All the robots have been imprisoned again!"];
    [dialog addButtonWithTitle:@"Ok!"];
    [dialog show];
    [dialog release];
    [resetButton setIsEnabled:false];

}
-(void) displayControls{
    CGSize winSize = [[CCDirector sharedDirector] winSize];

    UISlider *mainVolume = [[UISlider alloc]
                            initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width*.5, [[UIScreen mainScreen] bounds].size.height*.55 - 44, 200, 44)];
    
    [mainVolume addTarget:self action:@selector(volumeControl:)
         forControlEvents:UIControlEventValueChanged];
    
    mainVolume.backgroundColor = [UIColor clearColor];
    mainVolume.value =     [CDAudioManager sharedManager].backgroundMusic.volume;

    CCMenuItemImage *back=[CCMenuItemImage
                            itemFromNormalImage:@"arrow-left.png"
                            selectedImage:@"arrow-left-sel.png"
                            block:^(id sender){
                                [mainVolume removeFromSuperview];
                            [[GameManager sharedGameManager] runSceneWithID:kMainMenuScene];}
                           ];
    back.scale=0.5;
    CCMenu *menu=[CCMenu menuWithItems:back, nil];
    menu.position=ccp(BACK_BUTTON_X,winSize.height-BACK_BUTTON_Y);
    
    [self addChild:menu];
    
    CCLabelTTF* slider_label=[CCLabelTTF labelWithString:@"Volume" fontName:@"Helvetica" fontSize:30];
    [self addChild:slider_label];

    slider_label.position=ccp(winSize.width*.52,winSize.height*.45);
    
    [slider_label setColor:ccc3(0, 0, 0)];

    
    [[[[CCDirector sharedDirector] openGLView] window] addSubview:mainVolume];

    

    resetButton=[CCMenuItemImage itemFromNormalImage:@"resetbuttonnormal.png" selectedImage:@"resetbuttonselected.png" disabledImage:@"resetbuttondisabled.png" target:self selector:@selector(resetGame)];
    if ([[GameManager sharedGameManager] fresh]) {
        [resetButton setIsEnabled:false];
    }
    CCMenu *bottommenu=[CCMenu menuWithItems:resetButton, nil];
    [bottommenu setPosition:
     ccp(winSize.width * 0.8,
         -winSize.height *2)];
    id moveActionBottom =
    [CCMoveTo actionWithDuration:0.5f
                        position:ccp(winSize.width * 0.8,
                                     winSize.height*.2)];
    id moveEffectBottom = [CCEaseIn actionWithAction:moveActionBottom rate:.5];
    [bottommenu runAction:moveEffectBottom];
    
    [self addChild:bottommenu];
    

}
-(void) volumeControl:(UISlider*) slider{
    [CDAudioManager sharedManager].backgroundMusic.volume = slider.value;
    [[SimpleAudioEngine sharedEngine] setEffectsVolume:slider.value];

}
-(id)init {
    self = [super initWithColor:ccc4(255, 255, 255, 255)] ;
    if (self != nil) {
        [self displayControls];
    }
    return self;
}


@end
