//
//  TestScene.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "GameScene.h"
#import "GameLayer.h"
#import "GameManager.h"
@implementation GameScene
@synthesize mainLayer;
+(GameScene *) scene
{
	// 'scene' is an autorelease object.
	 GameScene* scene = [GameScene node];
	// 'layer' is an autorelease object.
		
	// add layer as a child to scene
//	[scene init];
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
        NSInteger puzzle_id=[GameManager sharedGameManager].current_puzzle;
        mainLayer = [GameLayer layerWithPuzzleID:puzzle_id];
        [self addChild: mainLayer];
        

		// create and initialize a Label
        mainLayer.isTouchEnabled=YES;
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
