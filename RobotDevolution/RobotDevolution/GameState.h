//
//  GameState.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 1/7/13.
//  Copyright 2013 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Robot.h"
#import "Constants.h"
#import "GameConfig.h"
@interface GameState : NSObject<NSCoding>{
    int level;
    float currentScore;
}
@property (readwrite,atomic) int level;
@property (readwrite,atomic) float currentScore;
+(GameState*)sharedGameState;

-(void) updateWithState:(CCArray*) worldObjects;
-(CCArray*) undo;
-(CCArray*) redo;
-(float) cost;
-(BOOL) canUndo;
-(BOOL) canRedo;
-(void) clean;
-(void) save;
-(void) readFromSaved;
-(CCArray*) current;
-(void) initializeGameRobots:(CCArray*) gameRobots;

@end
