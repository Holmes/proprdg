//
//  GameManager.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

//  GameManager.m
//  SpaceViking
//
#import "GameManager.h"
#import "cocos2d.h"
#import "MainMenuScene.h"
#import "GameScene.h"
#import "HelpScene.h"
#import "OptionsScene.h"
#import "GameConfig.h"
#import "GameState.h"
#import <AWSRuntime/AWSRuntime.h>

@implementation GameManager
static GameManager* _sharedGameManager = nil;                      // 1
@synthesize isMusicON;
@synthesize isSoundEffectsON;
@synthesize reloading;
@synthesize total_puzzles, current_puzzle;
@synthesize score,global_score;
@synthesize tracker;
@synthesize splash;

@synthesize playerAuthenticated;
+(GameManager*)sharedGameManager {
    @synchronized([GameManager class])                             // 2
    {
        if(!_sharedGameManager)                                    // 3
            [[self alloc] init];
        return _sharedGameManager;                                 // 4
    }
    return nil;
}

+(id)alloc
{
    @synchronized ([GameManager class])                            // 5
    {
        NSAssert(_sharedGameManager == nil,
                 @"Attempted to allocate a second instance of the Game Manager singleton");                                               // 6
        _sharedGameManager = [super alloc];
        return _sharedGameManager;                                 // 7
    }
    return nil;
}

-(id)init {                                                        // 8
    self = [super init];
    if (self != nil) {
        [CDAudioManager sharedManager].backgroundMusic.volume=0.25;
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:0.25];
        
        // Game Manager initialized
        isMusicON = YES;
        isSoundEffectsON = YES;
        currentScene = kNoSceneUninitialized;
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         @"rdg_head_anims.plist"];
        reloading=false;
        [self load_total_puzzles];
        [self load_current_puzzle];
        [self load_score];
        [self initSimpleDBClient];
        [self load_userID];
        localPlayer=nil;
    }
    return self;
}

-(void) initSimpleDBClient{
    sdbClient      = [[AmazonSimpleDBClient alloc]
                      initWithAccessKey:ACCESS_KEY_ID
                      withSecretKey:SECRET_KEY];
    
    sdbClient.endpoint = [AmazonEndpoints sdbEndpoint:US_EAST_1];
    [AmazonLogger verboseLogging];

}


-(void) load_userID{
    NSUserDefaults* ud=[NSUserDefaults standardUserDefaults];
    userID=[ud stringForKey:@"userID"];
    if ( userID == (id)[NSNull null] || userID.length == 0)
    {
        userID=[self newUUID];
        [ud setObject:userID forKey:@"userID"];
        [ud synchronize];
    }
    

}
-(void) load_total_puzzles{
    NSURL *URL = [NSURL URLWithString:TOTAL_PUZZLES_URL];

    // NSData *data = [NSData dataWithContentsOfURL:URL];
    // Assuming data is in UTF8.
    // NSString *string = [NSString stringWithUTF8String:[data bytes]];
    // total_puzzles= [string integerValue];
    
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:LEVEL_DATA_FILE ofType:@"plist"];
    
    NSDictionary *rootDict = [[[NSDictionary alloc]
                               initWithContentsOfFile:path] autorelease];
    
    total_puzzles=[rootDict count];
    current_puzzle=1;
}



-(void) load_current_puzzle{
    
    NSUserDefaults* ud=[NSUserDefaults standardUserDefaults];
    NSInteger puzzle_id=[ud integerForKey:@"current_puzzle"];
    if (puzzle_id==0)
    {
        puzzle_id=1;
    }
    current_puzzle=puzzle_id;
}

-(void) load_score{
    NSUserDefaults* ud=[NSUserDefaults standardUserDefaults];
    score=[ud floatForKey:@"TotalScore"];
    global_score=[ud floatForKey:@"Totalglobal_score"];
}

-(void) finishedPuzzle{
    
    current_puzzle++;
    
    NSUserDefaults* ud=[NSUserDefaults standardUserDefaults];

    [self updateScore];
    
    if(playerAuthenticated){
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
            [self reportScoreToGameCenter];
            [self reportScoreToPropRDGMetrics];

            dispatch_async( dispatch_get_main_queue(), ^{
                // Add code here to update the UI/send notifications based on the
                // results of the background processing
            });
        });

    }else{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self reportScoreToPropRDGMetrics];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
        });
    });}
    [tracker sendEventWithCategory:@"Major"
                        withAction:@"GameOver"
                         withLabel:[NSString stringWithFormat:@"Level Done: %d", current_puzzle]
                         withValue:[NSNumber numberWithInt:score*100]];

    [ud setFloat:score forKey:@"TotalScore"];
    [ud synchronize];

    [ud setFloat:global_score forKey:@"Totalglobal_score"];
    [ud synchronize];

    [ud setInteger:current_puzzle forKey:@"current_puzzle"];
    [ud synchronize];
}


-(void) reportScoreToGameCenter{
    int64_t gcScore = (int64_t)(global_score*100);

    GKScore *scoreReporter =[[GKScore alloc] initWithCategory:@"com.motalen.proprdg.leaderboard"];
    scoreReporter.value = gcScore;
    scoreReporter.context = 0;
    
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
        if (error != nil)
        {
            NSLog(@"Error Descr %@",error.localizedDescription);
            NSLog(@"Error Domain %@",error.domain);
        }
        else {
            NSLog(@"Submission ok");
            NSLog(@"Submitted %lld", gcScore);
        }
    }];

}
- (void) reportScoreToPropRDGMetrics{

    NSDateFormatter *formatter;
    NSString        *timeStamp;
    
    formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog( @"SDK Details: %@", [AmazonSDKUtil userAgentString] );
    timeStamp = [formatter stringFromDate:[NSDate date]];
    
    NSString *stringScore = [NSString stringWithFormat:@"Puzzle: %d Score: %f ", current_puzzle-1,score];
    
    SimpleDBReplaceableAttribute *scoreAttribute = [[[SimpleDBReplaceableAttribute alloc]  initWithName:timeStamp andValue:stringScore andReplace:YES] autorelease];

    
    NSMutableArray *attributes = [[[NSMutableArray alloc] initWithCapacity:1] autorelease];
    [attributes addObject:scoreAttribute];
    SimpleDBPutAttributesRequest *putAttributesRequest = [[[SimpleDBPutAttributesRequest alloc] initWithDomainName:@"proprdgmetrics" andItemName:userID andAttributes:attributes] autorelease];
    
    
    @try{
        [sdbClient putAttributes:putAttributesRequest];
    }
    @catch(NSException* ex)
    {
       //Not connected. Not doing anything now. Just throwing away the score for now. 
    }

}
-(void) reload{
    
    [self runSceneWithID:kLevelScene];
}

-(void) nextPuzzle{
     [[GameState sharedGameState] clean];
    [self finishedPuzzle];

    if(current_puzzle<=total_puzzles)
        [self runSceneWithID:kLevelScene];
    else
        [self runSceneWithID:kMainMenuScene];
    
}

- (NSString *)newUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (NSString *)string ;
}
-(void) reset{
    
    current_puzzle=1;
    score=0.00;
    global_score=0.0;
    NSUserDefaults* ud=[NSUserDefaults standardUserDefaults];
    
    [ud setFloat:score forKey:@"TotalScore"];
    [ud synchronize];

    [ud setFloat:global_score forKey:@"Totalglobal_score"];
    [ud synchronize];

    [ud setInteger:current_puzzle forKey:@"current_puzzle"];
    [ud synchronize];
    
    [[GameState sharedGameState] clean];
     [[GameState sharedGameState] save];
}

-(BOOL) fresh{
    return current_puzzle==1;
}
-(void) updateScore{
    score= [GameState sharedGameState].currentScore;
    global_score=current_puzzle-1+1/(1+score);
}

-(void) updateScoreForNewPlayer:(GKLocalPlayer*) newPlayer{
    
    if([newPlayer.playerID isEqualToString:localPlayer.playerID])
        return;
    else{
        
        localPlayer=newPlayer;
        GKLeaderboard *leaderboardRequest = [[[GKLeaderboard alloc] init] autorelease];
        if (leaderboardRequest != nil) {
            [leaderboardRequest loadScoresWithCompletionHandler:^(NSArray *scores, NSError *error){
                if (error != nil) {
                    //Handle error
                }
                else{
                 //   score=((float)leaderboardRequest.localPlayerScore.value)/100;
                }
            }];
        }
    
    }
    
}
-(void)runSceneWithID:(SceneTypes)sceneID {
    SceneTypes oldScene = currentScene;
    currentScene = sceneID;
    id sceneToRun = nil;
    switch (sceneID) {
        case kMainMenuScene:
            sceneToRun = [MainMenuScene node];
            break;
        case kLevelScene:
            if(current_puzzle<=total_puzzles)
                sceneToRun = [GameScene node];
            else
            {
                UIAlertView* dialog = [[UIAlertView alloc] init];
                [dialog setDelegate:self];
                [dialog setTitle:@"All Robots Freed! "];
                [dialog setMessage:@"All knows prisons have been broken out of! Please wait as we discover more prisons with poor robots in  them!"];
                [dialog addButtonWithTitle:@"Aargh! I can't wait!"];
                
                [dialog addButtonWithTitle:@"Ok!"];
                [dialog show];
                [dialog release];
            }            break;
        case kHelpScene:
            sceneToRun = [HelpScene node];
            break;
        case kOptionsScene:
            sceneToRun = [OptionsScene node];
            break;
            
        default:
            NSLog(@"Unknown ID, cannot switch scenes");
            return;
            break;
    }
    
    if (sceneToRun == nil) {
        // Revert back, since no new scene was found
        currentScene = oldScene;
        return;
    }
    
    
    if ([[CCDirector sharedDirector] runningScene] == nil) {
        [[CCDirector sharedDirector] runWithScene:sceneToRun];
        
    } else {
        [[CCDirector sharedDirector] replaceScene:sceneToRun];
    }
}

- (void) alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex==0) {
        //Aargh! I can't wait
    }
    if(buttonIndex==1) {
        //Ok
    }
}
-(void)runSplashUpandMain{

splash=TRUE;
    [self runSceneWithID:kMainMenuScene];
    splash=FALSE;

}

-(void)openSiteWithLinkType:(LinkTypes)linkTypeToOpen {
    NSURL *urlToOpen = nil;
    if (linkTypeToOpen == kLinkTypeMotalen) {
        CCLOG(@"Opening Musician Site");
        urlToOpen =
        [NSURL URLWithString:@"http://www.catabotrescue.com/"];
    }
    
    if (![[UIApplication sharedApplication] openURL:urlToOpen]) {
        CCLOG(@"%@%@",@"Failed to open url:",[urlToOpen description]);
        [self runSceneWithID:kMainMenuScene];
    }
}



@end
