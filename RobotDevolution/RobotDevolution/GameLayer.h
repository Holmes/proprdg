//
//  HelloWorldLayer.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright Rensselaer Polytechnic Institute 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
@class Robot;
@class Signature;

@interface GameLayer : CCLayerColor
{

    CCArray* worldObjects;

    
}


+(GameLayer *) layerWithPuzzleID:(NSInteger) puzzleID;

-(void) initLayerWithPuzzleID:(NSInteger) _puzzleID;
-(void)zoom:(UIPinchGestureRecognizer*)pinch;

@end
