//
//  HelpLayer.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "CCLayer.h"
#import "cocos2d.h"
#import "Constants.h"
#import "GameManager.h"

@interface HelpLayer : CCLayerColor{
    
    UISwipeGestureRecognizer *swipeForward;
    UISwipeGestureRecognizer *swipeBackward;
}
@end
