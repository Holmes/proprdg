//
//  Constants.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#ifndef RobotDevolution_Constants_h
#define RobotDevolution_Constants_h
#define kMainMenuTagValue 10
#define kSceneMenuTagValue 20


#define ACCESS_KEY_ID          @"AKIAIFYIMW54LVN3OSPQ"
#define SECRET_KEY             @"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"
#define CREDENTIALS_MESSAGE    @"AWS Credentials not configured correctly.  Please review the README file."

typedef enum {
    kNoSceneUninitialized=0,
    kMainMenuScene=1,
    kLevelScene=2,
    kHelpScene=3,
    kOptionsScene=5,
    kGameScene=101,
    kLevelCompleteScene=5,
} SceneTypes;

typedef enum {
    kLinkTypeBookSite,
    kLinkTypeDeveloperSiteRod,
    kLinkTypeDeveloperSiteRay,
    kLinkTypeArtistSite,
    kLinkTypeMusicianSite,
    kLinkTypeMotalen
} LinkTypes;
// Debug Enemy States with Labels
// 0 for OFF, 1 for ON
#define ENEMY_STATE_DEBUG 0



#endif
