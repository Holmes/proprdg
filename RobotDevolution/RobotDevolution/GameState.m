//
//  GameState.m
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 1/7/13.
//  Copyright 2013 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "GameState.h"

@interface GameState(){
    
@private
    CCArray* gameHistory; // Undo stack
    CCArray* gameFuture;  // Redo stack
    CCArray* gameRobots;
}
@end

@implementation GameState
@synthesize level;
@synthesize currentScore;
static GameState* _sharedGameState = nil;                      // 1

+(GameState*)sharedGameState {
    @synchronized([GameState class])                             // 2
    {
        if(!_sharedGameState)                                    // 3
            [[self alloc] init];
        return _sharedGameState;                                 // 4
    }
    return nil;
}

+(id)alloc
{
    @synchronized ([GameState class])                            // 5
    {
        NSAssert(_sharedGameState == nil,
                 @"Attempted to allocate a second instance of the Game Manager singleton");                                               // 6
        _sharedGameState = [super alloc];
        return _sharedGameState;                                 // 7
    }
    return nil;
}

-(id)init {                                                        // 8
    self = [super init];
    if (self != nil) {
        // Game State initialized
        gameHistory=[[CCArray alloc] init];
        gameFuture=[[CCArray alloc] init];
    }
    return self;
}

-(float) cost{
    currentScore = ([gameHistory count]-1)*COST_PER_MOVE;
    return currentScore;
}
-(BOOL) canUndo{
    return [gameHistory count] > 1;
}
-(BOOL) canRedo{
    return [gameFuture count] > 0;
}

-(void) updateWithState:(CCArray*) worldObjects{
    
    if(gameRobots){
        [gameHistory addObject:[CCArray arrayWithArray:worldObjects]];
        [gameFuture removeAllObjects];
        gameRobots=worldObjects;
    }
}
-(void) printHistory{
    for (int i =0 ; i< [gameHistory count]; i++) {
        //       NSLog([NSString stringWithFormat:@"State at %d contains %d number of robots", i,[(CCArray*) [gameHistory objectAtIndex:i] count]]);
    }
}

-(CCArray*) undo{
    if([gameHistory count]>1)
    {
        [gameFuture addObject:[gameHistory lastObject]];
        [gameHistory removeLastObject];
        CCArray* curr= [CCArray arrayWithArray:[gameHistory lastObject]];

        return curr;
    }
    else
        return nil;
    
}
-(CCArray*) redo{
    if([gameFuture count]>0)
    {
        CCArray* curr = (CCArray*) [gameFuture lastObject];
        [gameHistory addObject:curr];
        [gameFuture removeLastObject];
        CCArray* nxt= [CCArray arrayWithArray:curr];
        
        return nxt;
    }
    else
        return nil;
}

-(void) clean{
    [gameHistory removeAllObjects];
    [gameFuture removeAllObjects];
  //  [gameRobots removeAllObjects];
}

-(CCArray*) current{
    CCArray* curr;
    if([gameHistory count]>0)
   curr= [CCArray arrayWithArray:[gameHistory lastObject]];
    else
        curr=[CCArray arrayWithArray:gameRobots];
    return curr;
}
-(void) save{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:level forKey:@"level"];
    [prefs setBool:true forKey:@"Saved?"];
    [prefs setFloat:currentScore forKey:@"currentLevelScore"];
    [NSKeyedArchiver archiveRootObject:gameRobots toFile: [self pathForDataFile:@"gameRobots"]];
    [NSKeyedArchiver archiveRootObject:gameFuture toFile: [self pathForDataFile:@"gameFuture"]];
    [NSKeyedArchiver archiveRootObject:gameHistory toFile: [self pathForDataFile:@"gameHistory"]];
}

-(void) readFromSaved{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    if([prefs boolForKey:@"Saved?"])
    {
        level=[prefs integerForKey:@"level"];
        
        gameRobots =[[NSKeyedUnarchiver unarchiveObjectWithFile:[self pathForDataFile:@"gameRobots"]] retain];
        
        
        gameFuture =[[NSKeyedUnarchiver unarchiveObjectWithFile:[self pathForDataFile:@"gameFuture"]] retain];
        gameHistory =[[NSKeyedUnarchiver unarchiveObjectWithFile:[self pathForDataFile:@"gameHistory"]] retain];
        currentScore=[prefs floatForKey:@"currentLevelScore"];
    }

}
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:gameHistory forKey:@"gameHistory"];
    [encoder encodeObject:gameFuture forKey:@"gameFuture"];
    [encoder encodeObject:gameRobots forKey:@"gameRobots"];
    [encoder encodeInteger:level forKey:@"level"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    gameHistory = [decoder decodeObjectForKey:@"gameHistory"];
    gameFuture = [decoder decodeObjectForKey:@"gameFuture"];
    gameRobots = [decoder decodeObjectForKey:@"gameRobots"];
    level = [decoder decodeIntegerForKey:@"level"];
    return  self;
}

- (NSString *) pathForDataFile:(NSString*) filename {
    NSArray*	documentDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString*	path = nil;
 	
    if (documentDir) {
        path = [documentDir objectAtIndex:0];
    }
 	
    return [NSString stringWithFormat:@"%@/%@", path, filename];
}

-(void) initializeGameRobots:(CCArray*) _gameRobots{
    
    gameRobots= _gameRobots;
    
    /*When the current level is reset we 
      need to clear out the undo and redo
      stacks and start fresh.
     */
    [self clean];
    
}
-(void) dealloc{
    [gameHistory release];
    [gameFuture release];
    [super dealloc];

}

@end
