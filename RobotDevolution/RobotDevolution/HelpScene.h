//
//  HelpScene.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "CCScene.h"
#import "HelpLayer.h"
@interface HelpScene : CCScene{
    HelpLayer* helpLayer;
}
@end
