//
//  MainMenuLayer.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 12/17/12.
//  Copyright (c) 2012 Rensselaer Polytechnic Institute. All rights reserved.
//  MainMenuLayer.h
//  SpaceViking
//
#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Constants.h"
#import "GameManager.h"

@interface MainMenuLayer : CCLayerColor {
    CCMenu *mainMenu;
    CCMenu *sceneSelectMenu;
    UIViewController* viewController;
}
@property (retain,readwrite) UIViewController* viewController;

@end

