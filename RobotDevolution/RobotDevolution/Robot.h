//
//  Robot.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright 2012 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "WorldObject.h"
@class  Head;
@class Block;
@interface Robot : WorldObject {
    Head* head;
    CCArray *blocks;
    GLubyte opacity;
    CGFloat mating_distance_x;
    CGFloat mating_distance_y;
    CGFloat lower_y, upper_y;
    CGFloat blockWidth;
    CGFloat bouncePointY;
    CGFloat effectiveheight;
}
@property (nonatomic,retain) CCArray* blocks;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;
@property (nonatomic) CGFloat effectiveheight;

+ (Robot *) robotWithBlocks:(CCArray*)blocks;
+ (Robot *) robotFromRawSpecs:(CCArray*)specs;
-(void) initRobotWithBlocks:(CCArray*)blocks;
- (BOOL) acceptedable:(PartialTerm*) other;
- (CCArray*) canCombineWith:(Robot* )other;
-(BOOL) bodyLess;
-(BOOL) inMatingPosition:(Robot*) other block:(int)i otherBlock:(int)j;
-(CGFloat) boundary_x_withNextPos:(CGPoint)next;
-(CGFloat) boundary_y_withNextPos:(CGPoint)next;
+ (Robot*) clone:(Robot*) other;
-(NSString*) getSpec;
@end

//+ (Robot *) create:();