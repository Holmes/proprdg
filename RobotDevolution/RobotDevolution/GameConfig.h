//
//  GameConfig.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 6/18/12.
//  Copyright Rensselaer Polytechnic Institute 2012. All rights reserved.
//

#ifndef __GAME_CONFIG_H
#define __GAME_CONFIG_H
#define CC_FIX_ARTIFACTS_BY_STRECHING_TEXEL 1


//
// Supported Autorotations:
//		None,
//		UIViewController,
//		CCDirector
//
#define kGameAutorotationNone 0
#define kGameAutorotationCCDirector 1
#define kGameAutorotationUIViewController 2
#define CC_SPRITE_DEBUG_DRAW 2
//
// Define here the type of autorotation that you want for your game
//

// 3rd generation and newer devices: Rotate using UIViewController. Rotation should be supported on iPad apps.
// TIP:
// To improve the performance, you should set this value to "kGameAutorotationNone" or "kGameAutorotationCCDirector"
#if defined(__ARM_NEON__) || TARGET_IPHONE_SIMULATOR
#define GAME_AUTOROTATION kGameAutorotationNone

// ARMv6 (1st and 2nd generation devices): Don't rotate. It is very expensive
#elif __arm__
#define GAME_AUTOROTATION kGameAutorotationNone


// Ignore this value on Mac
#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)

#else
#error(unknown architecture)
#endif

////////////// Robot Devolution Game Constants //////////////
#define TOTAL_LEVELS 11


#define BLOCK_A_DEFAULT_WIDTH 20
#define BLOCK_A_DEFAULT_HEIGHT 40
#define BLOCK_B_DEFAULT_WIDTH 20
#define BLOCK_B_DEFAULT_HEIGHT 20

#define FOUNTAIN_X_POS [CCDirector sharedDirector].winSize.width*0.9
#define FOUNTAIN_Y_OFFSET 50
#define FOUNTAIN_LINE_X FOUNTAIN_X_POS-40
#define SPAN_POS_X FOUNTAIN_LINE_X-100

#define FOUNTAIN_SPAN_SPEED 200
#define BUBBLE_DURATION 0.2
#define BUBBLE_SCALE 1.1

#define FADE_TO_FINAL_REST 0.5

#define MATING_DISTANCE 200

#define BACK_BUTTON_X 50
#define BACK_BUTTON_Y 50

#define BOTTOM_MENU_X 20
#define BOTTOM_MENU_Y 30



#define PRISON_WALL_BUFFER 80
#define PRISON_WALL_WIDTH 60/2
#define TOTAL_PUZZLES_URL @"https://s3.amazonaws.com/PropRDG/list"

//Block Positioning
#define MATING_DISTANCE_X_THRESHOLD 20/2
#define MATING_DISTANCE_Y 131/2
#define MATING_DISTANCE_Y_THRESHOLD 30/2
#define PERFECT_MATING_DISTANCE_Y 116/2
#define BouncePointY_DELTA 30/2

// Effects
#define CLONE_BIRTH_JUMP_DIST_Y 100
//Maximum Game History
#define GAME_HISTORY_MAX 10

//Trash
#define TRASH_SQUARE_DIM 70
#define TRASH_THRESHOLD 30

//Linear Cost Per Move
#define COST_PER_MOVE 0.10

// Levels data file name

#define LEVEL_DATA_FILE @"LiteLevels"

#define FINAL_VELOCITY 250

#endif // __GAME_CONFIG_H


