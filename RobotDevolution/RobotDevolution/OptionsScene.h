//
//  OptionsScene.h
//  RobotDevolution
//
//  Created by Naveen Sundar Govindarajulu on 1/6/13.
//  Copyright 2013 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "OptionsLayer.h"
@interface OptionsScene : CCScene {
    OptionsLayer* optionsLayer;
}

@end
