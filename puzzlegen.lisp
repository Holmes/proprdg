(ql:quickload "trivial-shell")
(ql:quickload "cl-ppcre")
(defun file-string (path)
  (with-open-file (stream path)
    (let ((data (make-string (file-length stream))))
      (read-sequence data stream)
      data)))
(defun randomly (list)
   (elt list (random (length list))))

(defun repeat (n x) 
  (let ((result ())) (dotimes (i n result) (push x result))))

(defun shuffle (list)
  "Returns a randomly re-ordered copy of list."
  (let ((result nil))
    (do ()
        ((null list) result)
      (let* ((which (random (length list)))
             (it (nth which list)))
        (push it result)
        (setq list (remove it list :count 1))))))

(defun literal-to-integer (l)
  (if (symbolp l)
      (atom-to-natural l)
      (* -1 (atom-to-natural (second l)))))
(defun atom-to-natural (a)
  (cdr (assoc a '((p1 . 1) (p2 . 2) (p3 . 3) (p4 . 4)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;; PARAMETERS  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defparameter *prop-vars* '(p1 p2 p3 p4))
(defparameter *max-proof-depth* 5)
(defparameter *max-final-clause* 1)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;; SYNTAX HANDLING ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun atomic? (sentence) 
  (and (some (lambda (x) (equalp (first x) (first sentence)))
	     *predicates*)))


(defun head (expr) (first expr))
(defun args (expr) (rest expr))

(defun connective (sentence) (first sentence))
(defun prop (x) x)
(defun left (sent) (second sent))
(defun right (sent) (third sent))
(defun sym-equalp (s1 s2) (equalp (string s1) (string s2)))

(defun prop=? (p1 p2) (equalp p1 p2))

(defun make-disjunction (literals) (cons 'or literals))
(defun make-negation (literal) (list 'not literal))
(defun prop (x) x)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;; RANDOMIZATION STUFF ;;;;;;;;;;;;;;;;;;;;;;;;
(defun flip-coin () (if (eq 0 (random 2)) t ()))

(defun assume? () (if (flip-coin) t ()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; CONSISTENCY HANDLING ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun consistent-generate (x) x)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; RANDOM GENERATION ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun generate-answer ()
  (consistent-generate
   `(killed ,(randomly *people*) ,(randomly *people*))))

(defmacro randbranch (&rest body-decls)
  (let* ((total (length body-decls))
	 (code ())
	 (n total))
    (dolist (b (reverse body-decls))
      (push `(,(decf total) ,b) code))
    (push `(random ,n) code)
    (push 'case code)
    code))



(defun random-atomic ()
  (randomly *prop-vars*))

(defun random-clause (length)
  (labels
      ()
    (add-to-clause ())))

(defun add-to-clause (literals)
	 (if (>= (length literals) 3)
	     (make-disjunction literals)
	     (add-to-clause 
	      (cons 
	       (random-literal-different-from literals) 
	       literals))))
;
(defun depth (sentence)
  (if (atomic? sentence)
      0
      (1+ (apply 'max (mapcar 'depth (args sentence))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; GENERATION CODE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defparameter *all-literals*
  '(p1 p2 p3 p4 ))

(defun literal-to-atom (l) 
  (if (symbolp l) l (second l)))

(defun random-atomic-different-from (literals)
  (let ((univ 
	 (set-difference 
	  *all-literals* 
	  (mapcar #'literal-to-atom  literals) :test #'equalp)))
    (if univ
	(randomly univ)
	nil)))

(defun random-literal-different-from (literals)
  (let ((univ 
	 (set-difference 
	  *all-literals* 
	  (mapcar #'literal-to-atom  literals) :test #'equalp)))
    (if univ
	(randbranch (randomly univ)
		    (make-negation (randomly univ)))
	nil)))



(defun random-generate-test (total-clauses &key (min-size 2) (max-size 4))
  (labels ((fill-up-clauses (curr)
	       (if (= (length curr) total-clauses)
		   curr
		   (fill-up-clauses (cons
				     (let ((current-clause nil))
				       (dotimes (x (+ min-size (random  (1+ (- max-size min-size)))))
					       (push (random-literal-different-from current-clause)
						     current-clause))
				       (make-disjunction  current-clause))
				     curr)))))
   (fill-up-clauses nil)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun prop-rdg-clause-format (x) (format nil "<string> ~{~a ~^| ~}</string>" x))

(defun test-to-proprdg-puzzle-format (clauses)
  (let ((robot-nums-list 
	  (mapcar 
	   (lambda (clause)
	     (mapcar 
	      (lambda (literal) 
			       (literal-to-integer literal)) 
			     (rest  clause)))
			  clauses)))
    (format nil "<array> ~%  ~{  ~A~^~%  ~} ~% </array> ~%" (mapcar #'prop-rdg-clause-format robot-nums-list))))
(defparameter *start-level* 1)

(defun write-data-to-file (file-name data start-level)
  (with-open-file (stream file-name :direction :output :if-exists :supersede)
		  (format stream "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
<plist version=\"1.0\">
<dict>")
    (dotimes ( n (length data)) 
	     (format stream "<key>Level_~d</key>~% ~a ~%" (+ n start-level) (nth n data)))
    (format stream "
</dict>
</plist>
")))



(defun <-p (p1 p2)
  (< (measure-toughness p1) (measure-toughness p2)))


(defparameter *configs* 
  (list
    '((:total-problems . 2000) 
     (:total-clauses . 6)
     (:min-size . 2)
     (:max-size . 4)
     (:min-proof-size . 5)
     (:max-proof-size . 100)
     (:max-proof-level . 100)
     (:min-proof-level . 5))
    '((:total-problems . 3000) 
     (:total-clauses . 7)
     (:min-size . 2)
     (:max-size . 4)
     (:min-proof-size . 5)
     (:max-proof-size . 100)
     (:max-proof-level . 100)
     (:min-proof-level . 5))
   '((:total-problems . 2000) 
     (:total-clauses . 8)
     (:min-size . 2)
     (:max-size . 4)
     (:min-proof-size . 5)
     (:max-proof-size . 100)
     (:max-proof-level . 100)
     (:min-proof-level . 4))
   '((:total-problems . 4000) 
     (:total-clauses . 9)
     (:min-size . 2)
     (:max-size . 4)
     (:min-proof-size . 5)
     (:max-proof-size . 100)
     (:max-proof-level . 100)
     (:min-proof-level . 5))
))


(defparameter *configs* 
  (list

   '((:total-problems . 4000) 
     (:total-clauses . 8)
     (:min-size . 2)
     (:max-size . 4)
     (:min-proof-size . 5)
     (:max-proof-size . 100)
     (:max-proof-level . 100)
     (:min-proof-level . 4))
   '((:total-problems . 4000) 
     (:total-clauses . 9)
     (:min-size . 2)
     (:max-size . 4)
     (:min-proof-size . 5)
     (:max-proof-size . 100)
     (:max-proof-level . 100)
     (:min-proof-level . 5))
))


(defun get-config (key config) (cdr (assoc key config)))
(defun get-param (key param) (cdr (assoc key param)))

(defun generate-puzzles (config)
  (let ((temp nil))
    (dotimes (x (get-config :total-problems config) temp)
      ;(format t "Generating puzzle number ~a:~%" x) (force-output t)
      (let ((curr (random-generate-test (get-config :total-clauses config)
					:min-size (get-config :min-size config) 
					:max-size (get-config :max-size config))))
	(let* ((prover9-stats (prover9-stats curr))
	       (min-proof-level (get-param :min-proof-level prover9-stats))
	       (min-proof-length (get-param :min-proof-length prover9-stats))) 
	  (if (and
	       (<= min-proof-length (get-config :max-proof-size config))
	       (>= min-proof-length (get-config :min-proof-size config))
	       (<= min-proof-level (get-config :max-proof-level config))
	       (>= min-proof-level (get-config :min-proof-level config)))
	      (progn (push (cons `(:puzzle . ,curr)
				 prover9-stats) temp)
		    ; (format t " Puzzle usable[level=~a, length=~a]~%===============~%"min-proof-level min-proof-length )
		     )
	      ;(format t "  Puzzle unusable~%===============~%")
	      ))))))

(defun generate-game-data-file (filename &optional (start-index 1))
  (write-data-to-file
   filename
   (let* ((data (apply #'append (mapcar #'generate-puzzles *configs*)))
	 (sorted-data (sort data #'< :key (lambda (d) (get-param :min-proof-length d)))))
     (with-open-file (proof-file (concatenate 'string filename ".lisp") :direction :output :if-exists :supersede)
       (print sorted-data proof-file))
     (mapcar
      #'test-to-proprdg-puzzle-format
      (mapcar (lambda (d) (get-param :puzzle d)) sorted-data)))
   start-index))





(defparameter *prover9-scratch-dir* "/Volumes/ramdisk/")

(defun prover9-stats (clauses)
  (run-prover9-prooftrans clauses)
  (let ((proof-file-string (file-string (concatenate 'string  *prover9-scratch-dir*  "prooftrans.out"))))
    (list `(:min-proof-level . ,(get-prover9-min-proof-level proof-file-string))
	  `(:min-proof-length . ,(get-prover9-min-resolution-length proof-file-string))
	  `(:proof-file-string . ,proof-file-string))))

(defun run-prover9-prooftrans (clauses)
  (let ((prover9-input-filename  (concatenate 'string  *prover9-scratch-dir*  "prob.in"))
	(prover9-output-filename  (concatenate 'string  *prover9-scratch-dir*  "prover9.out"))
	(prooftrans-output-filename  (concatenate 'string  *prover9-scratch-dir*  "prooftrans.out")))
    (with-open-file (out prover9-input-filename :direction :output :if-exists :supersede)
	(format out (clauses-to-prover9-format clauses)))
    (with-open-file (out prover9-output-filename :direction :output :if-exists :supersede)
      (format out (trivial-shell:shell-command 
		   (concatenate 'string "/Applications/LADR-2009-11A/bin/prover9 -f " prover9-input-filename))))
    (with-open-file (out prooftrans-output-filename :direction :output :if-exists :supersede)
      (format out (trivial-shell:shell-command 
		   (concatenate 'string  "/Applications/LADR-2009-11A/bin/prooftrans expand -f " prover9-output-filename))))))

(defun get-prover9-min-proof-level (proof-file-string)
  (let ((levels nil)
	(in (make-string-input-stream proof-file-string)))
    (when in
      (loop for line = (read-line in nil)
	 while line do (if (string-equal "% Level of proof is" (and (>= (length line) 19) (subseq line 0 19)))
			   (push (read-from-string (subseq line 20 (1- (length line)))) levels)))
      (close in))  
    (if (> (length levels) 0)
	(apply #'min levels)
	-1)))

(defun get-prover9-min-resolution-length (proof-file-string)
  (let* ((proofs (cl-ppcre:all-matches-as-strings   "[=]+\\sPROOF\\s[=]+\\s[^=]+" proof-file-string))
	 (lengths
	  (mapcar #'length 
		  (mapcar (lambda (proof) 
			    (cl-ppcre:all-matches-as-strings  "resolve" proof)) 
			  proofs))))
    (if lengths
	(apply #'min lengths)
	-1)))


(defparameter *prover9-template*
  "assign(max_proofs, 50). ~%formulas(sos). ~% ~aend_of_list. ~% ~%formulas(goals).~%  $F. ~% end_of_list." )

(defun clauses-to-prover9-format (clauses)
  (format 
   nil   *prover9-template*
   (reduce #'(lambda (x y) (concatenate 'string x y)) 
	   (mapcar 
	    (lambda (clause)
	      (format nil (cl-ppcre:regex-replace-all  
			   "NOT " 
			   (let ((literals (rest clause)))
			     (if (= (length literals) 1)
				 (format nil "~a. ~%" (first literals))
				 (concatenate 'string "   ("
					      (string-trim '(#\Space)(format nil "~{~a ~^| ~}"  literals))
					      ").~%"))) "-")))
	    clauses))))  



; LocalWords:  defun
